
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2020 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "version_def.h"
#include "fatfs.h"
#include "usb_host.h"

/* USER CODE BEGIN Includes */
#include <math.h>
// Ble includes
#include "mppt.h"
#include "osal.h"
#include "sample_service.h"
#include "role_type.h"
#include "stm32_bluenrg_ble.h"
#include "bluenrg_utils.h"
////
#include "display.h"
/* USER CODE END Includes */

uint16_t SineBuffer[SINEBUFFERLENGTH];

/* Private variables ---------------------------------------------------------*/
RTC_DateTypeDef sdatestructureget;
RTC_TimeTypeDef stimestructureget;
RTC_DateTypeDef sdatestructureset;
RTC_TimeTypeDef stimestructureset;
struct Flags_fault Flags_fault;
struct Flags_F Flags_F;
struct Battery Battery;
struct Solar  Solar;
struct Flag_ChargingState Flag_ChargingState;
struct PWM PWM;
struct ParameterSet ParameterSet;
struct System System;
struct  Load Load;
struct Flag_State Flag_State;
struct LED LED;
struct  TIMERt TIMERt;
struct Energy Energy;
struct Mains Mains;
struct Temperature Temperature;
struct Inverter_OP Inverter_OP;
struct Mains_flags Mains_flags;
struct Inverter_flags Inverter_flags;
struct Inverter_timers Inverter_timers;
struct Inverter_ParameterSet Inverter_ParameterSet;
struct Keypad Keypad;
struct Inverter_Mode Inverter_Mode;
_Bool AvgCaptured = 0;
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

RTC_HandleTypeDef hrtc;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim8;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
///// Power section////
#define BATV_CLB_FACTOR 84.0 //167.0  // battv
#define SLRV_CLB_FACTOR 29.0 // 57.0  //solar voltage
#define SLRI_CLB_FACTOR  109.0//282// solar cur
#define BATI_CLB_FACTOR  100.7//304.0  // batt current
#define INVOP_CLB_FACTOR 10 //12.9  // inv o/p vol
#define MAINV_CLB_FACTOR 7.42  //Mains voltage
#define MAINI_CLB_FACTOR 550.86//2466.0  //Inv o/p current
#define BATTEMP_CLB_FACTOR 123.6  // NTC
#define LOADI_CLB_FACTOR  43.1//55.5  // dis charging current
float Scaling_fctor[10]={1000.0,1000.0,1000.0,1000.0,1000.0,1000.0,1000.0,1000.0,1000.0,1000.0};
float Adc_Multiplier[10];//={1000.0,1000.0,1000.0,1000.0,1000.0,1000.0,1000.0,1000.0,1000.0,1000.0};

float Battery_V;
float Solar_V;
float Solar_I;
float Battery_I;
float Inv_OPV;
float Mains_I;
float Battery_temp;
float Load_I;
////////////////************************  Sine Table for 12.8 Khz Switching Frequency  ****************************************************///////////////////////////////////////////////////
 unsigned int sine_TABLE[128]={0,104,208,312,415,519,622,724,827,928,1030,1130,1230,1329,1428,1525,1622,1717,1812,1905,1998,2089,2179,2267,2354,2440,
                               2524,2607,2688,2768,2846,2922,2997,3069,3140,3209,3276,3341,3404,3465,3524,3580,3635,3687,3737,3785,3831,3874,3915,3954,
							   3990,4024,4055,4084,4111,4135,4156,4175,4192,4206,4217,4226,4233,4237,4238,4237,4233,4226,4217,4206,4192,4175,4156,4135,4111,
							   4084,4055,4024,3990,3954,3915,3874,3831,3785,3737,3687,3635,3580,3524,3465,3404,3341,3276,3209,3140,3069,2997,2922,2846,2768,
							   2688,2607,2524,2440,2354,2267,2179,2089,1998,1905,1812,1717,1622,1525,1428,1329,1230,1130,1030,928,827,724,622,519,415,312,208,104};	 						


uint32_t min=0,hour=0,secnd=0,year=0,month=0,day=0;

/* Private variables ---------------------------------------------------------*/
uint8_t filecounter;
/*start ble*/
struct Flags 						        Flags;
struct MPPT_RX_flags 				                MPPT_RX_flags;
struct mppt_cmd 						mppt_cmd;
struct user_cmd 						user_cmd;
 struct mppt_data 						mppt_data;
/* Private variables ---------------------------------------------------------*/
#define BDADDR_SIZE     6
uint8_t R_DATA[15];
uint8_t MPPT_ID[6];
uint8_t D_BUFF[200];
uint8_t D_BUFF1[200];
uint8_t log_buff[15];
uint8_t prepaid_buff[15];
uint8_t volatile bnrg_expansion_board = IDB05A1; /* at startup, suppose the X-NUCLEO-IDB04A1 is used */   

BLE_RoleTypeDef BLE_Role = SERVER;

uint8_t throughput_test = 0; /* disable the test for the estimation of the throughput */
//variables of service
extern volatile uint8_t set_connectable;
extern volatile int connected;
extern volatile uint8_t notification_enabled;

extern volatile uint8_t start_read_tx_char_handle;
extern volatile uint8_t start_read_rx_char_handle;
extern volatile uint8_t end_read_tx_char_handle;
extern volatile uint8_t end_read_rx_char_handle;
/* ble End */
char PCUX_ID[6];
uint8_t aShowTime[13] = "12:44:00";            //Time Buffer for RTC
uint8_t aShowDate[13] = "19/09/19";
uint16_t ADC_DMA_Buff[11];              //Buffer to get ADC values into DMA
unsigned int ADC_DMA_Buff_mains;
static uint32_t adc_avg_sample=0;
static uint16_t base_avg_time=0;

uint32_t ADC_DMA_Buff_Avg[9];
uint32_t Battery_DMA[1000];
uint8_t EE_DATA[50];
uint8_t EE_READ[50];
uint32_t k=0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_SPI1_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM8_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
void MX_USB_HOST_Process(void);
void SineWave__BufferInit(void);
void Adc_signal_calib(void);
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
 void System_Check (void); 
 void AppsMain__ControlLoop(void);                         

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void Ble_Init(void);
void User_Process(void);
void ADC_Coversion(void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
unsigned int display_time;
uint16_t solartracking_timer;
uint16_t invertertracking_timer;

_Bool Stsrt_apps = 0;
_Bool display2lcd =0;
float dummy_mains;
uint16_t samples_m;
float Scalning_Factor[20]= {1000,1000,1000,1000,1000,1000,1000,1000};
float Calib_Scalning_Factor[20]= {1,1,1,1,1,1,1,1,1,1,1,1,1,1};
uint32_t Flash_data_ram[20];
/* USER CODE END 0 */
uint16_t reset_counter1=0;
/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_USART2_UART_Init();
  MX_SPI1_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_TIM8_Init();
  MX_RTC_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  // MX_USB_HOST_Init();
  //MX_FATFS_Init();
  /* USER CODE BEGIN 2 */
    HAL_Delay(2000);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  SineWave__BufferInit(); 
  HAL_UART_Transmit(&huart2,(uint8_t *)"\33[2J\r",8,50);// clear screen
  HAL_UART_Transmit(&huart2,(uint8_t *)"\r|-----------------------------------|",39,50);
  HAL_UART_Transmit(&huart2,(uint8_t *)"\n\r|       Statcon PCU  V.1.2          |",39,50);
  HAL_UART_Transmit(&huart2,(uint8_t *)"\n\r|         12V- 1000VA               |",39,50);
  HAL_UART_Transmit(&huart2,(uint8_t *)"\n\r|-----------------------------------|\n\r",40,50);
#ifndef USB_BYPASS
  MX_FATFS_Init();
  USB_INIT();
#endif
  Welcome_Show();
#ifndef BLE_BYPASS
  Ble_Init();
#endif
  /* USER CODE END 2 */

  /* Infinite loop */
  HAL_UART_Transmit(&huart2,(uint8_t *)"\r\n---Program ready & started---\r\n",39,50);
  /* USER CODE BEGIN WHILE */
  ////////////
  // Set__Date(29,2,2020);
 // Set__Time(17,45,50);
  PCUX_ID[5]=1;
  Initialization_DataBase();
  // LOAD_ON;
  Stsrt_apps = 1;
  BUZZER_OFF;           
  INVERTER_OFF;            
  MPPT_CHARGING_OFF; 
  GRID_CHARGING_OFF;      
  // PV_OFF;
  FAULT_OFF;
  FAN_ON;
  /*   */
  Inverter_flags.inv_short_ckt_check = RESET;
  Inverter_flags.inv_soft_short_ckt_delay = SET;
    HAL_TIM_Base_Start_IT(&htim2);                          //start timer2 interrupt //30ms
  HAL_TIM_Base_Start_IT(&htim3);
  HAL_TIM_Base_Start_IT(&htim4);
  
  //USB_OFF;
  //////
  for(;;)
  {
    Stsrt_apps = 1;
     AppsMain__ControlLoop();
    reset_counter1 = 0;

  }
  /* USER CODE END 3 */

}
void AppsMain__ControlLoop(void)
{
    if(Flags_fault.save_to_flash == 1 && Flags_F.inv_sw_state==0)
    {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("Data Saving.....");
      
      HAL_Delay(1000);
      
      HAL_UART_Transmit(&huart2,(uint8_t *)"\n\r Saved in Flash",17,50);
      Flags_fault.save_to_flash = 0;
      if(Flags_fault.factory_reset_todo == 1)
      {
        Flags_fault.factory_reset_todo = 0;
        DataBase_Update(INIT_PAGE,INIT_CHAR_ADRS,0xFF);
        Write_data_to_Flash();
        HAL_Delay(100);
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("   RESET DONE   ");
        HAL_Delay(1000);
        HAL_NVIC_SystemReset();
        
        
      }
      if(Flags_fault.save_to_flash_clb ==1)
      {
        Flags_fault.save_to_flash_clb = 0;
        update_to_flash();      
      }
      else if(Flags_fault.save_to_flash_user_fac ==1)
      {
        Flags_fault.save_to_flash_user_fac = 0;
        Write_data_to_Flash();      
      }
      read_data();
      //HAL_NVIC_SystemReset();
    }
    
    
    if((SET_KEY==0) && (ENT_KEY==0) && (INC_KEY==0) && (DEC_KEY==0))
    {
      if(Mains.start_rd_vol==SET)
      {
       // LED_TEST(GPIO_PIN_SET);
        Mains.cal_ok =RESET;
        samples_m++;
        dummy_mains = dummy_mains+(ADC_DMA_Buff[FACTOR_POS_MAIN_VOL]/MAINV_CLB_FACTOR)*(Adc_Multiplier[FACTOR_POS_MAIN_VOL]/Scaling_fctor[FACTOR_POS_MAIN_VOL]);
        
      }
      else if(Mains.cal_ok == RESET && samples_m>100)
      {
       // LED_TEST(GPIO_PIN_RESET);
        Mains.cal_ok =SET;
        Mains.voltage =  dummy_mains/samples_m;
        samples_m =0;
        dummy_mains =0;
      }
    }
     if(display2lcd==1)
      {
        Parameters_Display();
        display2lcd =0;
        //HAL_UART_Transmit(&huart2,(uint8_t *)"\n\r System Alive",15,50);
      }
#ifndef USB_BYPASS
    MX_USB_HOST_Init();
#endif
  /* USER CODE BEGIN 3 */
#ifndef BLE_BYPASS
      HCI_Process();
     User_Process();
#endif
      ADC_Coversion();
      if(Flags_F.grid_chrg_timer == 1)
      {
        Flags_F.grid_chrg_timer = 0;
        GridCharging__Management();
      }

      if(Flags_F.track_start == SET )
      {
        Flags_F.track_start = RESET;
        Fault__Management();
        LEDindication_Management();
        Inverter__FaulsManagement();
        Inverter__ControlManagement();
        GridCharging__Faults();
       // DCLoad__Management();
        NTC__Control();
      }
      if(Flags_F.mppt_track_start == SET)
      {
        Flags_F.mppt_track_start = RESET;
        SoftStartCharging__Management();
        Charging__Management();
       // DCLoad__Management();
      }
      
      //////////////Keypad control/////
      if(Keypad.scan_do == SET)
	  {

		  Keypad.scan_do =0;
		  KeyPressed__check();
      Inverter__SwitchState();
	  }
	  if(Keypad.handling == SET)
	  {
		Keypad.handling =0;
		KeyScan__FastHandler();
		//Buzzer__Control();

	  }
      
    Fault_led();  

    /* ISR optimization */
    if(AvgCaptured == 1)
    {
      Adc_signal_calib(); 
      ADC_DMA_Buff[0] = ADC_DMA_Buff_Avg[0]/adc_avg_sample;
      ADC_DMA_Buff[1] = ADC_DMA_Buff_Avg[1]/adc_avg_sample;
      ADC_DMA_Buff[2] = ADC_DMA_Buff_Avg[2]/adc_avg_sample;
      ADC_DMA_Buff[3] = ADC_DMA_Buff_Avg[3]/adc_avg_sample;
      ADC_DMA_Buff[4] = ADC_DMA_Buff_Avg[4]/adc_avg_sample;
      ADC_DMA_Buff[5] = ADC_DMA_Buff_Avg[5]/adc_avg_sample;
      ADC_DMA_Buff[6] = ADC_DMA_Buff_Avg[6]/adc_avg_sample;
      ADC_DMA_Buff[7] = ADC_DMA_Buff_Avg[7]/adc_avg_sample;
      k=0;
  

      adc_avg_sample =0;
      memset(ADC_DMA_Buff_Avg,0,sizeof(ADC_DMA_Buff_Avg));
      Solar.Current = (ADC_DMA_Buff[0]/SLRI_CLB_FACTOR)*(Adc_Multiplier[0]/Scaling_fctor[0]);
      Solar.Voltage = (ADC_DMA_Buff[1]/SLRV_CLB_FACTOR)*(Adc_Multiplier[1]/Scaling_fctor[1]);
      Battery.Voltage = (ADC_DMA_Buff[2]/BATV_CLB_FACTOR)*(Adc_Multiplier[2]/Scaling_fctor[2]);
      //Inverter_OP.current = (ADC_DMA_Buff[3]/MAINI_CLB_FACTOR)*(Adc_Multiplier[3]/Scaling_fctor[3]);
      Inverter_OP.ct_signal = (ADC_DMA_Buff[3]/MAINI_CLB_FACTOR)*(Adc_Multiplier[3]/Scaling_fctor[3]);
      Battery.Current =(ADC_DMA_Buff[5]/BATI_CLB_FACTOR)*(Adc_Multiplier[5]/Scaling_fctor[5]);
      Battery.Discharging_current = (ADC_DMA_Buff[6]/LOADI_CLB_FACTOR)*(Adc_Multiplier[6]/Scaling_fctor[6]);

      Inverter_OP.voltage = (ADC_DMA_Buff[FACTOR_POS_INVOP_VOL]/INVOP_CLB_FACTOR)*(Adc_Multiplier[FACTOR_POS_INVOP_VOL]/Scaling_fctor[FACTOR_POS_INVOP_VOL]);
      Temperature.compensation = (ADC_DMA_Buff[FACTOR_POS_TEMP_COMP]/BATTEMP_CLB_FACTOR)*(Adc_Multiplier[FACTOR_POS_TEMP_COMP]/Scaling_fctor[FACTOR_POS_TEMP_COMP]);



      if(Inverter_flags.output_on == RESET)
      {
      Mains.curent=Inverter_OP.ct_signal;
      Inverter_OP.current=0;
      Inverter_OP.frq =Mains.frq;
      
      }
      else
      {
      Mains.curent=0; 
      
      Inverter_OP.current=Inverter_OP.ct_signal;
      
      }
      
      if(Inverter_flags.output_on == SET && Inverter_OP.voltage > 100)
      {
        
        Inverter_OP.frq = 49.9;            
      }
      
      else if(Inverter_flags.output_on == SET && Inverter_OP.voltage < 100)
      {
      Inverter_OP.frq = 0;  
      }
    
    
      
    if(Solar.Voltage< 10.0)
      
    {
    Solar.Voltage=0;
    
    }
    
    if( Solar.Current< 0.7)
    {
    Solar.Current=0;
    
    }
    if(Battery.Current< 1.0)
    {
    Battery.Current=0;
    
    }
    if(Inverter_OP.current< 0.2)
    {
    Inverter_OP.current=0;
    
    }
    if(Battery.Discharging_current< 2.0)
    {
    Battery.Discharging_current=0;
    
    }
    AvgCaptured = 0;
  }
}   

void ADC_Coversion(void)
{
  //MX_ADC1_Init();
  //HAL_ADC_Start_IT(&hadc1);
  //HAL_ADC_Start_DMA(&hadc1,(uint32_t*)ADC_DMA_Buff,11);    //Start ADC DMA  //Start ADC
 //Adc_signal_calib();
  //HAL_ADC_DeInit(&hadc1);
}


void Adc_Avg(void)
{
  HAL_ADC_Start_DMA(&hadc1,(uint32_t*)ADC_DMA_Buff,11); 
  if(AvgCaptured == 0)
  {
    if(base_avg_time++ > 7)
    {
      base_avg_time =0;
      if(adc_avg_sample ++ < 16) // 1000 120 msec
      {

              ADC_DMA_Buff_Avg[0] += ADC_DMA_Buff[0];
              ADC_DMA_Buff_Avg[1] += ADC_DMA_Buff[1];
              ADC_DMA_Buff_Avg[2] += ADC_DMA_Buff[2];
              ADC_DMA_Buff_Avg[3] += ADC_DMA_Buff[3];
              ADC_DMA_Buff_Avg[4] += ADC_DMA_Buff[4];
              ADC_DMA_Buff_Avg[5] += ADC_DMA_Buff[5];
              ADC_DMA_Buff_Avg[6] += ADC_DMA_Buff[6];
              ADC_DMA_Buff_Avg[7] += ADC_DMA_Buff[7];
              
              Battery_DMA[k] =ADC_DMA_Buff[2];
              k++;
              
              


      }
      else
      {
        AvgCaptured = 1;

      }
    }
  }
}

void Adc_signal_calib(void)
{
  //Inverter_OP.voltage = (ADC_DMA_Buff[FACTOR_POS_INVOP_VOL]/INVOP_CLB_FACTOR)*(Adc_Multiplier[FACTOR_POS_INVOP_VOL]/Scaling_fctor[FACTOR_POS_INVOP_VOL]);
  Load.Voltage =  Battery.Voltage;
  //Temperature.compensation = (ADC_DMA_Buff[FACTOR_POS_TEMP_COMP]/BATTEMP_CLB_FACTOR)*(Adc_Multiplier[FACTOR_POS_TEMP_COMP]/Scaling_fctor[FACTOR_POS_TEMP_COMP]);
  Temperature.MPPT_NTC = ADC_DMA_Buff[10];
  Temperature.INV_NTC = ADC_DMA_Buff[9];

  ADC_DMA_Buff_mains = ADC_DMA_Buff[7];
  if(Inverter_OP.voltage< 50.0)
  {
    Inverter_OP.voltage=0; 
  }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if((htim->Instance==TIM2) )
    {
      //reset_counter1 =0;
 
      
      if(TIMERt.grid_chrg_counter++ > 400)
      {
        TIMERt.grid_chrg_counter =0;
        Flags_F.grid_chrg_timer = 1;// call grid charging        
      }
      
      if(invertertracking_timer++ >0)
      {
        invertertracking_timer =0;
        Flags_F.track_start = SET;
       // Flags_F.mppt_track_start = SET;        
      }

      if(solartracking_timer++ >1000)
      {
        solartracking_timer =0;
        // Flags_F.track_start = SET;
        Flags_F.mppt_track_start = SET;
      }
      if(!Keypad.password_scan_factory_reset && !Keypad.factory_reset && !Keypad.menu_mode && !Keypad.clb_password_scan_mode&& !Keypad.password_scan_mode&& !Keypad.sub_menu_clb_mode && !Keypad.parameter_setting && !Keypad.user_setting)
      {
   
       if(display_time++> 1600)
        {        
           display_time =0;
           display2lcd = 1;
        }
       }
     
      
    }
 else if(htim->Instance==TIM3)
 {
  if(reset_counter1 >= 100)//200
    {
      //HAL_NVIC_SystemReset();
      reset_counter1=0;
    }
    if(TIMERt.key_scan++ > 20) //40 //200
    {
      reset_counter1++;
      TIMERt.key_scan =0;
      Keypad.scan_do =SET;

    }
    if(TIMERt.handle_scan++ >200)//400//2000
    {
      TIMERt.handle_scan =0;
      Keypad.handling =SET;
    }
     
 }
 else if(htim->Instance==TIM4)
 {
  
   Adc_Avg();
    
  ///////////
  if(Flags_F.inv_sw_state==SET && Inverter_flags.inv_short_ckt_check==SET )
   { 
     if(Inverter_flags.soft_start_Timer++>=75)
     {
      Inverter_flags.inv_soft_short_ckt_delay= 1; //0;
      Inverter_flags.soft_start_Timer=75;
      
      Inverter_flags.inv_short_ckt_check = RESET;
     }
   }
 else if(Flags_F.inv_sw_state==RESET)
 {
   Inverter_flags.soft_start_Timer=0;
   Inverter_flags.inv_short_ckt_check=0;
   Inverter_flags.inv_soft_short_ckt_delay=1;
   Flags_fault.Short_ckt = 0;
 }  
else if(SHORT_CKT==GPIO_PIN_SET && Inverter_flags.inv_soft_short_ckt_delay==0 )
{
  Inverter_flags.output_on=RESET;
  Inverter_flags.output_switched_off=RESET;
  //LED_PV_TOGGLE;
  Flags_fault.Short_ckt = 1;
  
}
  
  
//  else Inverter_flags.inv_short_ckt_check=0;
  
  
  
  
  /////////
   
 }
  
}

void System_Check(void)
{
  ;
}


// initialize sine wave buffer
void SineWave__BufferInit(void)
{
  memset(SineBuffer,0,sizeof(SineBuffer));
  for (int n = 0; n < SINEBUFFERLENGTH; n++)
  {
    const float   amplitude = MAX_AMPLITUDE;
    int32_t val = ( sin( PI * n / SINEBUFFERLENGTH  )) * amplitude*1;
    SineBuffer[n] = val;
  }
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  RCC_OscInitStruct.PLL.PLLR = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48CLKSOURCE_PLLQ;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV8;//ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 11;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_10;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;//ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_11;
  sConfig.Rank = 2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_12;
  sConfig.Rank = 3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_13;
  sConfig.Rank = 4;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = 5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = 6;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_14;
  sConfig.Rank = 7;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = 8;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_9;
  sConfig.Rank = 9;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  
  /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = 10;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  
  /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = 11;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  

}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* RTC init function */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */

    /**Initialize RTC Only 
    */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/* SPI1 init function */
static void MX_SPI1_Init(void)
{

  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM1 init function */
static void MX_TIM1_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 8;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 900;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 120;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim1);

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 1680;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 48;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM3 init function */
static void MX_TIM3_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 8400;//7000;//4200;//840;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM4 init function */
static void MX_TIM4_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 1;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 1680;//840
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM8 init function */
static void MX_TIM8_Init(void)
{

  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

  htim8.Instance = TIM8;
  htim8.Init.Prescaler = 3; ///3------>23.3 khz  2------18.6 khz
  htim8.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim8.Init.Period = 900;
  htim8.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim8.Init.RepetitionCounter = 0;
  if (HAL_TIM_PWM_Init(&htim8) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim8, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim8, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 120;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim8, &sBreakDeadTimeConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim8);

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, BLE_CSN_Pin|BLE_RESET_Pin|MAINS_RELAY_Pin|LED_MPPT_CHRG_Pin 
                          |LED_MAINS_GRID_Pin|LED_FAULTS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, FAN_CONTROL_Pin|FAN_CONTROL_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, D4_Pin|D5_Pin|D6_Pin|D7_Pin 
                          |RS_Pin|EN_Pin|BUZZER_CONTROL_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LCD_BACKLIGHT_Pin|LED_INV_STATUS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : BLE_CSN_Pin BLE_RESET_Pin MAINS_RELAY_Pin LED_MPPT_CHRG_Pin 
                           LED_MAINS_GRID_Pin LED_FAULTS_Pin */
  GPIO_InitStruct.Pin = BLE_CSN_Pin|BLE_RESET_Pin|MAINS_RELAY_Pin|LED_MPPT_CHRG_Pin 
                          |LED_MAINS_GRID_Pin|LED_FAULTS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : BLE_IRQ_Pin */
  GPIO_InitStruct.Pin = BLE_IRQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BLE_IRQ_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : FAN_CONTROL_Pin */
  GPIO_InitStruct.Pin = FAN_CONTROL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  
  /*Configure GPIO pins : SDN_DRIVER_INV_Pin */
  
  
  GPIO_InitStruct.Pin = SDN_DRIVER_INV_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


  /*Configure GPIO pins : ENT_Pin DEC_Pin INC_Pin SET_Pin 
                           INV_ON_OFF_SW_Pin */
  GPIO_InitStruct.Pin = ENT_Pin|DEC_Pin|INC_Pin|SET_Pin 
                          |INV_ON_OFF_SW_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : D4_Pin D5_Pin D6_Pin D7_Pin 
                           RS_Pin EN_Pin BUZZER_CONTROL_Pin */
  GPIO_InitStruct.Pin = D4_Pin|D5_Pin|D6_Pin|D7_Pin 
                          |RS_Pin|EN_Pin|BUZZER_CONTROL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : ZC_Pin */
  GPIO_InitStruct.Pin = ZC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(ZC_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LCD_BACKLIGHT_Pin LED_INV_STATUS_Pin */
  GPIO_InitStruct.Pin = LCD_BACKLIGHT_Pin|LED_INV_STATUS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 4, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */


void Ble_Init(void)
{
  HAL_UART_Transmit(&huart2,(uint8_t *)"\r-----------------BLE-------------",36,50);
  uint8_t CLIENT_BDADDR[6];                                                     // = {0x90, 0x00, 0x00, 0xE1, 0x25, 0x25};
  uint8_t SERVER_BDADDR[6];                                                     //= {0x90, 0x00, 0x00, 0xE1, 0x25, 0x25};
  uint8_t bdaddr[BDADDR_SIZE];
  uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
  uint8_t  hwVersion;
  uint16_t fwVersion;
  int ret;
  HAL_UART_Transmit(&huart2,(uint8_t *) "\n\rBluetooth Initialization \n\r ",31,50);//debug
  BLENRG_SPI1_Init();
  HCI_Init();
  BlueNRG_RST();
  getBlueNRGVersion(&hwVersion, &fwVersion);
  mppt_data.mppt_id_set =0;
 MPPT_ID[5]=1;// for testing
  CLIENT_BDADDR[0]=SERVER_BDADDR[0]= MPPT_ID[5];
  CLIENT_BDADDR[1]=SERVER_BDADDR[1]= MPPT_ID[4];
  CLIENT_BDADDR[2]=SERVER_BDADDR[2]= MPPT_ID[3];
  CLIENT_BDADDR[3]=SERVER_BDADDR[3]= MPPT_ID[2];
  CLIENT_BDADDR[4]=SERVER_BDADDR[4]= MPPT_ID[1];
  CLIENT_BDADDR[5]=SERVER_BDADDR[5]= MPPT_ID[0];
  

  BlueNRG_RST();
  
  if (hwVersion > 0x30) 
  { /* X-NUCLEO-IDB05A1 expansion board is used */
    bnrg_expansion_board = IDB05A1; 
    HAL_UART_Transmit(&huart2,(uint8_t *) "\n\rBLE HW Version: IDB05A1\n\r ",32,50);//debug
  }
  
  if(BLE_Role == CLIENT)
  {
  Osal_MemCpy(bdaddr, CLIENT_BDADDR, sizeof(CLIENT_BDADDR));
  } 
  else 
  {
  Osal_MemCpy(bdaddr, SERVER_BDADDR, sizeof(SERVER_BDADDR));
  }

  ret = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET,CONFIG_DATA_PUBADDR_LEN,bdaddr);
  
  if(ret)
  {
  PRINTF("\nSetting BD_ADDR failed 0x%02x.\n", ret);
  }
  else 
  {
    HAL_UART_Transmit(&huart2,(uint8_t *) "\n\rSetting BD_ADDR:OK \n\r ",28,50);//debug 
  }
  ret = aci_gatt_init();    
  if(ret)
  {
  PRINTF("\n\rGATT_Init failed.\n");
  Flags.a =1;
  }
  else
  {
    HAL_UART_Transmit(&huart2,(uint8_t *) "\n\rGATT Init:OK \n\r ",22,50);//debug
  }
  if(bnrg_expansion_board == IDB05A1)
  {
  ret = aci_gap_init_IDB05A1(GAP_PERIPHERAL_ROLE_IDB05A1, 0, 0x07, &service_handle,&dev_name_char_handle, &appearance_char_handle);
  }
  if(ret != BLE_STATUS_SUCCESS)
  {
  PRINTF("\nGAP_Initr Failed.\n");
  Flags.b =1;
  }
  else 
  {
    HAL_UART_Transmit(&huart2,(uint8_t *) "\n\rGAP Init:OK \n\r ",21,50);//debug
  }     
  ret = aci_gap_set_auth_requirement(MITM_PROTECTION_REQUIRED,OOB_AUTH_DATA_ABSENT,NULL,7,16,USE_FIXED_PIN_FOR_PAIRING,123456,BONDING);
  if(ret == BLE_STATUS_SUCCESS)
  {
  PRINTF("\n SERVER: BLE Statck Initialized.\n");
  }
  else 
  {
    Flags.c =1;
  }
  ret = Add_Sample_Service();
  if(ret == BLE_STATUS_SUCCESS)
  {
  //PRINTF("\nService added successfully.\n");
  HAL_UART_Transmit(&huart2,(uint8_t *) "\n\rService added successfully\n\r ",31,50);//debug
  }
  else 
  {
  PRINTF("\nError while adding services\n");
  Flags.d =1;
  }
  /*end BLE functions*/
  HAL_UART_Transmit(&huart2,(uint8_t *) "\n\rBLE Init: OK\n\r ",20,50);//debug
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void User_Process(void)
{
  if(set_connectable){
    /* Establish connection with remote device */
    Make_Connection();
    set_connectable = FALSE;
  }
  
  if(BLE_Role == CLIENT) {
    /* Start TX handle Characteristic dynamic discovery if not yet done */
    if (connected && !end_read_tx_char_handle){
      startReadTXCharHandle();
    }
    /* Start RX handle Characteristic dynamic discovery if not yet done */
    else if (connected && !end_read_rx_char_handle){      
      startReadRXCharHandle();
    }
    
    if(connected && end_read_tx_char_handle && end_read_rx_char_handle && !notification_enabled) {
      enableNotification();
    }
  }     
	if(user_cmd.device_status  && connected && notification_enabled && mppt_data.status_rdy_to_sent_apps)
	{
                Ble__TransmitUpdateBuff();
		user_cmd.device_status =0;
		mppt_cmd.device_status =0;
		user_cmd.device_log =0;
		mppt_cmd.device_log =0;
		mppt_data.prepaid_to_sent_apps =0;
		mppt_data.status_rdy_to_sent_apps =0;
		uint_least8_t header_packet[3] = {'/','S',(char)D_BUFF1[21]};
                sendData(header_packet, sizeof(header_packet));
		sendData(D_BUFF, 20);
		sendData(D_BUFF1, 20);
		sendData(&D_BUFF1[20], 1);
		sendData(&D_BUFF1[21], 1);
		
		
	}
	else if(user_cmd.device_log && connected && notification_enabled && mppt_data.log_redy_to_sent_apps)
	{
		//mppt_data.log_redy_to_sent_apps =0;
		mppt_cmd.device_status =0;
		user_cmd.device_status =0;
		mppt_data.prepaid_to_sent_apps =0;
		//user_cmd.device_log =0;
		
		//if(mppt_data.log_end_to_sent_apps)
		{
                  //uint_least8_t header_packet[2] = {0xFF,0x55};
                  //sendData(header_packet, sizeof(header_packet));
                 // mppt_data.log_end_to_sent_apps = 0;
                 // mppt_data.log_redy_to_sent_apps =0;
			
		}
		//else 
		{
                  
                  // read log
                 
                  
		}
				
		
	}
	else if(user_cmd.pre_metering && connected && notification_enabled && mppt_data.prepaid_to_sent_apps)
	{
		mppt_data.prepaid_to_sent_apps =0;
		mppt_data.log_redy_to_sent_apps =0;
		mppt_cmd.device_status =0;
		user_cmd.device_status =0;
		uint_least8_t header_packet[3] = {'/','*','R'};
                sendData(header_packet, sizeof(header_packet));
		sendData(prepaid_buff, 12);
		
	}
  }

void Set__Date(uint8_t date,uint8_t month , uint16_t year )
{
  sdatestructureset.Date=date;
    sdatestructureset.Month=month;
    sdatestructureset.Year=year-2000;
    HAL_RTC_SetDate(&hrtc, &sdatestructureset, RTC_FORMAT_BIN);
  
}

void Set__Time(uint8_t hr,uint8_t mm , uint16_t sec )
{
    
    stimestructureset.Hours=hr;
    stimestructureset.Minutes=mm;
    stimestructureset.Seconds=sec;
    stimestructureset.TimeFormat=RTC_HOURFORMAT_24;
    HAL_RTC_SetTime(&hrtc, &stimestructureset, FORMAT_BIN);
  
}

void RTC_Get_Time_Date(void)
{
  
  /* Get the RTC current Time */
  HAL_RTC_GetTime(&hrtc, &stimestructureget, FORMAT_BIN);
  /* Get the RTC current Date */
  HAL_RTC_GetDate(&hrtc, &sdatestructureget, FORMAT_BIN);
  
  /* Display time Format: hh:mm:ss */
  sprintf((char*)aShowTime,"%02d:%02d:%02d",stimestructureget.Hours, stimestructureget.Minutes, stimestructureget.Seconds);
  /* Display date Format: mm-dd-yy */
  sprintf((char*)aShowDate,"%02d/%02d/%04d",sdatestructureget.Date,sdatestructureget.Month, 2000 + sdatestructureget.Year);
  
  min=stimestructureget.Minutes;
  hour=stimestructureget.Hours;
  secnd=stimestructureget.Seconds;
  year=sdatestructureget.Year;
  month=sdatestructureget.Month;
  day=sdatestructureget.Date;
  
  
  //sprintf((char*)RTCLOG,"%s %s \n",aShowTime,aShowDate);  
  
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  HAL_NVIC_SystemReset();
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
  HAL_NVIC_SystemReset();
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
