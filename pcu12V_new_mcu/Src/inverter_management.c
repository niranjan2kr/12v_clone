
#include "version_def.h"


extern HCD_HandleTypeDef hhcd_USB_OTG_FS;
extern DMA_HandleTypeDef hdma_adc1;
extern ADC_HandleTypeDef hadc1;
extern I2C_HandleTypeDef hi2c1;
extern SPI_HandleTypeDef hspi1;
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern UART_HandleTypeDef huart2;
extern uint16_t SineBuffer[SINEBUFFERLENGTH];
uint16_t sampling_time=0;
uint16_t sin_step;
_Bool Inv_on_flag=0;

uint16_t Sample_step = 0;
_Bool pZCROSS;
_Bool Changeover_timer=0;
_Bool ZCD;
float inv_op_err = 250;


void Inverter__ControlManagement(void)
{

  if(!Mains_flags.available &&  (Flags_fault.batt_under_voltage || Flags_fault.batt_over_voltage || Inverter_flags.health_check || Flags_fault.NTC_INV_Fail))// stop inverter only
  {
    Inverter_flags.output_on = RESET;
    Inverter_flags.output_switched_off = RESET;
   // FAULT_ON;
  }
  else if(Flags_fault.NTC_INV_Fail == 0)
	//  FAULT_OFF;

  ///////////////////////////
    if(Mains_flags.available) 
      {
      Inverter_timers.output_on=0;
     // FAULT_OFF;
      if(Inverter_flags.output_under_vol || Inverter_flags.output_over_vol)
      {
        Inverter_flags.health_check = SET;        
      }
      if(Inverter_timers.output_off++ > 1)//3
      {
        Inverter_timers.output_off = 0;// reset timer
        
        MAINS_ON;
      pZCROSS = 1;//
      Changeover_timer =1;
       sin_step=25;
        Inverter_timers.manins_on =0;
        Inverter_timers.output_off =0;
        inv_op_err=900;
     

      }
    }

    else if(!Inverter_flags.health_check && !Mains_flags.available && !Flags_fault.batt_under_voltage && !Flags_fault.batt_over_voltage && !Mains_flags.charging_start && !Flags_fault.NTC_INV_Fail && !Flags_fault.Short_ckt)// inverter on
      {
        Inverter_timers.output_off =0;

        if(Inverter_timers.output_on++ > 1)//3
        {
           Inverter_timers.output_on = 0;// reset timer
           
          Inverter_flags.output_on = 1;
          Inverter_timers.manins_off =0;
         Inverter_flags.output_switched_off = RESET;
         
        }

      }


  
/////////////////////////////////////////////

    if(Inverter_flags.output_on && Inverter_flags.health_check==RESET)
    {
        if(Inverter_OP.voltage > 230)
        {
          if(inv_op_err < 1145 && inv_op_err > 100)
          { 
            if((Inverter_OP.voltage - 230) < 15)
               inv_op_err= inv_op_err-1; 
            else inv_op_err= inv_op_err-3;
          }
        }
        else if(Inverter_OP.voltage < 229)// && Inverter_OP.voltage > 180)
        {
          if(inv_op_err < 1137)
          {
            if((230-Inverter_OP.voltage) < 15)
              inv_op_err= inv_op_err+1;
            else inv_op_err= inv_op_err+1;
          }
        }
      }

  /////////////////////////////

}


void Inverter__FaulsManagement (void)
{ 
  /* check manins avalibality */
  /*if(Flags_F.pv_available == SET && Battery.Voltage>40.0)//23.3V// Mains Disconnect Batt_V
  {
     if ( Inverter_timers.manins_off ++ > 6)//10
      {
        Inverter_timers.manins_off =0;
        if(Mains_flags.charging_start == SET || Mains.charging_disabled == RESET)
        {
           Mains.charging_disabled =SET;
          Mains_flags.charging_start = 0;
          htim1.Instance->CCR3 =0;
          htim1.Instance->CCR2=0;
       
        }
        MAINS_OFF;
        Mains_flags.available = RESET;
        inv_op_err = 1000;
      }
  }
  */
   if(Mains_flags.MAINS_OK == GPIO_PIN_SET)// manis aa gyi hi
  {
    if( Mains_flags.available == SET || Mains_flags.MAINS_OK == SET ) /* && Inverter_flags.output_on == RESET && Mains_flags.under_voltage == RESET && Mains_flags.over_voltage == RESET*/ 
        {	
           if(LED.charging_on == RESET || Mains_flags.grid_charging_indication == RESET)
            {
            GRID_CHARGING_ON;
            }
         }
    
     
    if(Inverter_Mode.S_B_G == SET )
	{

    	if((Flags_F.pv_available && Battery.Voltage < ParameterSet.grid_reconnect_battvol) || Flags_fault.batt_under_voltage==SET/* || (Flags_F.pv_available &&  Mains_flags.mains_bypass ==1 )*/ )
    	{

                  TIMERt.GridDisconnect_delay = 0;
                    if(Flags_fault.batt_under_voltage==SET || Inverter_flags.output_switched_off==SET)
                  {
                  TIMERt.GridConnect_delay=5000;
                  }
                  
                  if(TIMERt.GridConnect_delay ++ > 4000)
                  {
                        TIMERt.GridConnect_delay = 0;
			if ( Inverter_timers.manins_on ++ > 1)//4
			  {
				Inverter_flags.output_on = 0; // inv ON/OFF
				Mains_flags.available = SET;// Apps signal
				inv_op_err = 1000;
                                Mains_flags.mains_bypass =1;
				if(Battery.Voltage < Inverter_ParameterSet.gridcharging_start_Battvoltage ||  Mains_flags.charging_on ==1 )
				{
					// Allow charging
				 Mains_flags.charging_stop = RESET;
				 Mains_flags.chargigng_boost_start = RESET;
				 Mains_flags.chargigng_float_start = RESET;
                                 Mains_flags.charging_start=SET;
                                 Mains_flags.charging_on =1;
				}
			  }
                  }

    	}
    	else if((Flags_F.pv_available == SET && Battery.Voltage > ParameterSet.grid_discon_battvol && Battery.Current > ParameterSet.grid_discon_battCur) /*|| Battery.Voltage > ParameterSet.Batt_Boost_voltage-1.0*/ )
		{
                   TIMERt.GridConnect_delay = 0;
                   
                   if(TIMERt.GridDisconnect_delay++ > 20000)
                  {
                      TIMERt.GridDisconnect_delay = 0;
			if((Mains_flags.charging_start == SET || Mains.charging_disabled == RESET) &&  Inverter_timers.manins_off >0 )//9
				{
					Mains.charging_disabled = SET;
					Mains_flags.charging_start = 0;
					HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
					HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
					HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);
					HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);
					htim1.Instance->CCR3 =0;//Inverter_ParameterSet.PWM_gridChrg;
					htim1.Instance->CCR2=0;// = Inverter_ParameterSet.PWM_gridChrg;
                                        Mains_flags.charging_on =0;

				}
			   if( Inverter_timers.manins_off ++ > 1 )//10
				{

				 Inverter_timers.manins_off = 0;
				  Mains_flags.charging_start = RESET;
				  MAINS_OFF;
				  Mains_flags.available = RESET;
                                  Mains_flags.mains_bypass =0; 


				}
                  }
		}
        
        
        else if((Flags_F.pv_available == SET && Battery.Voltage > ParameterSet.Batt_Boost_voltage-1.0  && Battery.Current< ( ParameterSet.Batt_max_current/2) ) /*|| Battery.Voltage > ParameterSet.Batt_Boost_voltage-1.0*/ )
		{
                   TIMERt.GridConnect_delay = 0;
                   
                   if(TIMERt.GridDisconnect_delay_2++ > 20000)
                  {
                      TIMERt.GridDisconnect_delay_2 = 0;
			if((Mains_flags.charging_start == SET || Mains.charging_disabled == RESET) &&  Inverter_timers.manins_off >0 )//9
				{
					Mains.charging_disabled = SET;
					Mains_flags.charging_start = 0;
					HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
					HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
					HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);
					HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);
					htim1.Instance->CCR3 =0;//Inverter_ParameterSet.PWM_gridChrg;
					htim1.Instance->CCR2=0;// = Inverter_ParameterSet.PWM_gridChrg;
                                        Mains_flags.charging_on =0;

				}
			   if( Inverter_timers.manins_off ++ > 1 )//10
				{

				 Inverter_timers.manins_off = 0;
				  Mains_flags.charging_start = RESET;
				  MAINS_OFF;
				  Mains_flags.available = RESET;
                                  Mains_flags.mains_bypass =0; 


				}
                  }
		}
        
		else if(Battery.Voltage < ParameterSet.grid_reconnect_battvol ||   Mains_flags.mains_bypass ==1 || Flags_fault.batt_under_voltage==SET )// || Flags_F.pv_available == RESET)// mains reconnect batt_vol
		{
                  TIMERt.GridDisconnect_delay =0;
                   if(Flags_fault.batt_under_voltage==SET || Inverter_flags.output_switched_off==SET )
                  {
                  TIMERt.GridConnect_delay=5000;
                  }
                  
                  if(TIMERt.GridConnect_delay++ > 4000)
                  {
                    TIMERt.GridConnect_delay = 0;
                    
                    if ( Inverter_timers.manins_on ++ > 1)//4
                    {
                      Inverter_flags.output_on = 0; // inv ON/OFF
                      Mains_flags.available = SET;// Apps signal
                      inv_op_err = 1000;
                      Mains_flags.mains_bypass =1;
                      if(Battery.Voltage < Inverter_ParameterSet.gridcharging_start_Battvoltage || Mains_flags.charging_on ==1 )
                      {
                              // Allow charging
                         Mains_flags.charging_stop = RESET;
                         Mains_flags.chargigng_boost_start = RESET;
                         Mains_flags.chargigng_float_start = RESET;
                         Mains_flags.charging_start=SET;
                         Mains_flags.charging_on =1;
                      }
                    }
                  }
		}
                else
                {
                  TIMERt.GridDisconnect_delay = 0;
                  TIMERt.GridConnect_delay = 0;   
                  TIMERt.GridDisconnect_delay_2=0;
                  
                }
    }
    /*  Mode S-G-B */
    else if(Inverter_Mode.S_G_B == SET)
    {
    	if((Flags_F.pv_available && Battery.Voltage < ParameterSet.grid_reconnect_battvol) || Flags_fault.batt_under_voltage==SET /*|| (Flags_F.pv_available &&  Mains_flags.mains_bypass ==1 )*/)
		{
                  TIMERt.GridDisconnect_delay = 0;
                  if(Flags_fault.batt_under_voltage==SET)
                  {
                  TIMERt.GridConnect_delay=5000;
                  }
                  if(TIMERt.GridConnect_delay ++ > 4000)
                  { 
                       TIMERt.GridConnect_delay = 0;
			if ( Inverter_timers.manins_on ++ > 1)//4
			  {
				Inverter_flags.output_on = 0; // inv ON/OFF
				Mains_flags.available = SET;// Apps signal
				inv_op_err = 1000;
                                Mains_flags.mains_bypass =1;
				if(Battery.Voltage < Inverter_ParameterSet.gridcharging_start_Battvoltage ||  Mains_flags.charging_on ==1 )
				{
					// Allow charging
				 Mains_flags.charging_stop = RESET;
				 Mains_flags.chargigng_boost_start = RESET;
				 Mains_flags.chargigng_float_start = RESET;
                                 Mains_flags.charging_start=SET;
                                 Mains_flags.charging_on =1;
				}
			  }

		}
                }
                
        

        
            	else if((Flags_F.pv_available == SET && Battery.Voltage > ParameterSet.grid_discon_battvol && Battery.Current > ParameterSet.grid_discon_battCur) /*|| Battery.Voltage > ParameterSet.Batt_Boost_voltage-1.0*/ )
		{
                   TIMERt.GridConnect_delay = 0;
                   
                   if(TIMERt.GridDisconnect_delay++ > 20000)
                  {
                      //TIMERt.GridDisconnect_delay = 0;
			if((Mains_flags.charging_start == SET || Mains.charging_disabled == RESET) &&  Inverter_timers.manins_off >0 )//9
				{
                                       // TIMERt.GridDisconnect_delay = 0;
					Mains.charging_disabled = SET;
					Mains_flags.charging_start = 0;
					HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
					HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
					HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);
					HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);
					htim1.Instance->CCR3 =0;//Inverter_ParameterSet.PWM_gridChrg;
					htim1.Instance->CCR2=0;// = Inverter_ParameterSet.PWM_gridChrg;
                                        Mains_flags.charging_on =0;

				}
			   if( Inverter_timers.manins_off ++ > 1 )//10
				{
                                  TIMERt.GridDisconnect_delay = 0;
				 Inverter_timers.manins_off = 0;
				  Mains_flags.charging_start = RESET;
				  MAINS_OFF;
				  Mains_flags.available = RESET;
                                  Mains_flags.mains_bypass =0; 


				}
                  }
		}
        
        
        else if(Flags_F.pv_available == SET && Battery.Voltage > ParameterSet.Batt_Boost_voltage-1.0 && Battery.Current< ( ParameterSet.Batt_max_current/2))
		{
                   TIMERt.GridConnect_delay = 0;
                   
                   if(TIMERt.GridDisconnect_delay_2++ > 20000)
                  {
                     // TIMERt.GridDisconnect_delay_2 = 0;
			if((Mains_flags.charging_start == SET || Mains.charging_disabled == RESET) &&  Inverter_timers.manins_off >0 )//9
				{
                                        //TIMERt.GridDisconnect_delay_2 = 0;
					Mains.charging_disabled = SET;
					Mains_flags.charging_start = 0;
					HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
					HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
					HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);
					HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);
					htim1.Instance->CCR3 =0;//Inverter_ParameterSet.PWM_gridChrg;
					htim1.Instance->CCR2=0;// = Inverter_ParameterSet.PWM_gridChrg;
                                        Mains_flags.charging_on =0;

				}
			   if( Inverter_timers.manins_off ++ > 1 )//10
				{
                                 TIMERt.GridDisconnect_delay_2 = 0;
				 Inverter_timers.manins_off = 0;
				  Mains_flags.charging_start = RESET;
				  MAINS_OFF;
				  Mains_flags.available = RESET;
                                  Mains_flags.mains_bypass =0; 


				}
                  }
		}
        
        
        
        
		else if(Flags_F.pv_available == RESET)// mains reconnect batt_vol
		{
		  if ( Inverter_timers.manins_on ++ > 1)//4
		  {
			Inverter_flags.output_on = 0; // inv ON/OFF
			Mains_flags.available = SET;// Apps signal
			inv_op_err = 1000;
                        Mains_flags.mains_bypass =1;
			if(Battery.Voltage < Inverter_ParameterSet.gridcharging_start_Battvoltage ||  Mains_flags.charging_on ==1 )
			{
                                // Allow charging
                         Mains_flags.charging_stop = RESET;
                         Mains_flags.chargigng_boost_start = RESET;
                         Mains_flags.chargigng_float_start = RESET;
                         Mains_flags.charging_start=SET;
                         Mains_flags.charging_on =1;
                          
			}
		  }
		}
         else
                {
                  TIMERt.GridDisconnect_delay = 0;
                  TIMERt.GridConnect_delay = 0;   
                  TIMERt.GridDisconnect_delay_2=0;
                  
                }
        
    }
    /*  Mode Hybrid Mode G-S-B* */

    else if(Inverter_Mode.G_S_B == SET)
      {
    	///////////////
		  if ( Inverter_timers.manins_on ++ > 1)//4
		  {
			Inverter_flags.output_on = 0; // inv ON/OFF
			Mains_flags.available = SET;// Apps signal
			inv_op_err = 1000;
                        
				// Allow charging
			 Mains_flags.charging_stop = RESET;
			 Mains_flags.chargigng_boost_start = RESET;
			Mains_flags.chargigng_float_start = RESET;
                        Mains_flags.charging_start=SET;
		  }


      }

    
  }
  
  else if(Mains_flags.MAINS_OK == GPIO_PIN_RESET)// && Battery.Voltage > 25.8)
  {
   if((Mains_flags.charging_start == SET || Mains.charging_disabled == RESET) && Inverter_timers.manins_off > 0 )//9
    {
      Mains.charging_disabled = SET;
      Mains_flags.charging_start = 0;
              HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
        HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
        HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);  
        HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);
      htim1.Instance->CCR3 =0;//Inverter_ParameterSet.PWM_gridChrg;
         htim1.Instance->CCR2=0;// = Inverter_ParameterSet.PWM_gridChrg;  
   
    }   
   if( Inverter_timers.manins_off ++ > 1 )//10
    {
      
      Inverter_timers.manins_off = 0;
      Mains_flags.charging_start = RESET;
      MAINS_OFF;
      Mains_flags.available = RESET;
      
    }
  }



 
  //Inverter_flags.health_check = 1;// test
  if(!Inverter_flags.health_check)//Inverter_flags.output_on )
  {
    
    if(inv_op_err < 0)//700
    {
      if(Inverter_OP.voltage > 245)
      {
        Inverter_timers.op_under =0;
        if(Inverter_timers.op_over++ > 100)
        {
          Inverter_timers.op_over=0;
          
          Inverter_flags.output_over_vol = SET;
          Inverter_flags.output_under_vol = RESET;
        }
      }
    }
    else if(inv_op_err > 9135)//1135
    {
      Inverter_timers.op_over=0;
      if(Inverter_OP.voltage < 180)
      {
        if(Inverter_timers.op_under++ > 2000)
        {
          Inverter_timers.op_under = 0;
          
          Inverter_flags.output_under_vol = SET;
          Flags_F.inv_sw_state = 0;
          Inverter_flags.output_over_vol = RESET;
        }
      }
      
      
    }
    else if(Inverter_OP.voltage > 221 && Inverter_OP.voltage < 234)
    {
      Inverter_flags.output_under_vol = RESET;
      Inverter_flags.output_over_vol = RESET;
      
    }
    
  }
  /// inv overlad load check and control
 
  if(Inverter_OP.current > (Inverter_ParameterSet.op_current_max*3.2) && Inverter_flags.overload == RESET ) //300%
  {
    if(Inverter_timers.Inverter_overload_330_timer++ >1) // Immediate off
    {
      Inverter_timers.Inverter_overload_330_timer =0;
      Inverter_flags.overload = SET;
    } 
  }
  else if (Inverter_OP.current < (Inverter_ParameterSet.op_current_max*3))
    {
     Inverter_timers.Inverter_overload_330_timer =0; 
    
    }
  
  if(Inverter_OP.current > Inverter_ParameterSet.op_current_max*3 && Inverter_flags.overload == RESET ) //300%
  {
    if(Inverter_timers.Inverter_overload_300_timer++ > 20) // calling in  2ms    20----->39.5 ms
    {
      Inverter_timers.Inverter_overload_300_timer =0;
      Inverter_flags.overload = SET;
    } 
  }
  else if (Inverter_OP.current < (Inverter_ParameterSet.op_current_max*2.7))
    {
     Inverter_timers.Inverter_overload_300_timer =0; 
    
    }
  
    if(Inverter_OP.current > Inverter_ParameterSet.op_current_max*2.5 && Inverter_flags.overload == RESET ) //250%
  {
    if(Inverter_timers.Inverter_overload_250_timer++ > 50) // 50--------->100 ms
    {
      Inverter_timers.Inverter_overload_250_timer =0;
      Inverter_flags.overload = SET;
    } 
  }
  else if(Inverter_OP.current < (Inverter_ParameterSet.op_current_max*2.25))
  {
    Inverter_timers.Inverter_overload_250_timer =0; 
  }
  
     if(Inverter_OP.current > Inverter_ParameterSet.op_current_max*2 && Inverter_flags.overload == RESET ) //200%
  {
    if(Inverter_timers.Inverter_overload_200_timer++ > 1012) // 1012------>2 sec
    {
      Inverter_timers.Inverter_overload_200_timer =0;
      Inverter_flags.overload = 1;
  
    } 
  }
  else if (Inverter_OP.current < (Inverter_ParameterSet.op_current_max*1.8))
  {
    Inverter_timers.Inverter_overload_200_timer =0; 
  }
  
     if(Inverter_OP.current > Inverter_ParameterSet.op_current_max*1.5 && Inverter_flags.overload == RESET ) //150%
  {
    if(Inverter_timers.Inverter_overload_150_timer++ > 5060)  //5060------->10 sec
    {
      Inverter_timers.Inverter_overload_150_timer =0;
      Inverter_flags.overload = SET;
    } 
  }
  else if (Inverter_OP.current < (Inverter_ParameterSet.op_current_max*1.35))
  {
    Inverter_timers.Inverter_overload_150_timer =0; 
  }
  
     if(Inverter_OP.current > Inverter_ParameterSet.op_current_max*1.25 && Inverter_flags.overload == RESET ) //125%
  {
    if(Inverter_timers.Inverter_overload_125_timer++ > 15180) //15180---------->30 sec
    {
      Inverter_timers.Inverter_overload_125_timer =0;
      Inverter_flags.overload = SET;
    } 
  }
  else if(Inverter_OP.current < (Inverter_ParameterSet.op_current_max*1.15))
  {
    Inverter_timers.Inverter_overload_125_timer =0; 
  }
  
     if(Inverter_OP.current > Inverter_ParameterSet.op_current_max*1.1 && Inverter_flags.overload == RESET ) //110%
  {
    if(Inverter_timers.Inverter_overload_110_timer++ > 30360) //30360------>60 sec
    {
      Inverter_timers.Inverter_overload_110_timer =0;
      Inverter_flags.overload = SET;
    } 
  }
  else if (Inverter_OP.current < (Inverter_ParameterSet.op_current_max))
  {
    Inverter_timers.Inverter_overload_110_timer =0; 
  }
  
     if(Inverter_OP.current > Inverter_ParameterSet.op_current_max && Inverter_flags.overload == RESET ) //100%
  {
    if(Inverter_timers.Inverter_overload_timer++ > 60720) //60720---------120sec
    {
      Inverter_timers.Inverter_overload_timer =0;
      Inverter_flags.overload = SET;
    } 
  }
  else if (Inverter_OP.current < (Inverter_ParameterSet.op_current_max*0.95))
  {
    Inverter_timers.Inverter_overload_timer =0; 
  }
}

void ZCD__Sensing(void)
{
  if(++Inverter_timers.Ms_counter > 1)//>5
  {
    Inverter_timers.Ms_counter =0;
    if(Mains.Check_ZCD == RESET)
    {
      if(ZCD_Read == RESET )
      {
        if(Mains.Fail_Timer_1 > 20 && Mains.Fail_Timer_1 < 60)
      {
 
    	  if(Flags_F.mains_fails_detected_start_1 == 0)
    	  {
			if(ADC_DMA_Buff[7] <50)
			{
				Flags_F.mains_fails_detected_start_1 = 1;
				ParameterSet.Mains_Fails_Detect_time_1 =0;
			 
			}
    	  }
    	  else
    	  {

    		  if(ADC_DMA_Buff[7] <50)
    		  {

    			  if(ParameterSet.Mains_Fails_Detect_time_1++ > 10)
    			  {
                                  
              Mains.Fail_Timer = 15000;
              Mains.frq =0;
              Mains.voltage =0;
              Mains.start_rd_vol==RESET;

              Mains_flags.MAINS_OK = 0;
              Inverter_ParameterSet.PWM_gridChrg = 0;
              MAINS_OFF;
              Mains_flags.available = RESET; 
              LED.grid_charging_on=0;
              if(Mains.charging_disabled ==  RESET)
              {                                        
                Mains.charging_disabled =SET;
                Mains_flags.charging_start = 0;

                HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
                HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
                HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);  
                HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);

                htim1.Instance->CCR3 =0;
                htim1.Instance->CCR2=0;
              }
                               

    			  }
                        

    		  }
    		  else
			  {
				  Flags_F.mains_fails_detected_start_1 =0;
				  ParameterSet.Mains_Fails_Detect_time_1 = 0;
                                 
                                    
			  }


    	  }
      }
      if(Mains.Fail_Timer_1++ > 80)
      {
       Mains.Fail_Timer_1=0;
       Mains.Fail_Timer = 700;
       

        Flags_F.mains_fails_detected_start_1 =0;
        ParameterSet.Mains_Fails_Detect_time_1 = 0;
      }
      
        
        
        
         Mains.Fail_Timer = 0;
         Mains.ZCD_reset_Timer=0;
        Flags_F.mains_fails_detected_start =0;
      ParameterSet.Mains_Fails_Detect_time = 0;
        Mains.ZCD_Pre_state_lo = SET;
        Mains.start_rd_vol = SET;
        if(Mains.ZCD_Pre_state_hi == SET)
        {
          Inverter_timers.ZCD_counter_Lo++;
          Mains.ZCD_final_state = SET;
          
          
        }
      }
      else  if(ZCD_Read == SET && (Mains.ZCD_Pre_state_lo == SET))
      {   
        Mains.ZCD_Pre_state_hi = SET;
        Inverter_timers.ZCD_counter_Hi++;
        if(Mains.ZCD_final_state == SET)
        {
          Mains.Check_ZCD = SET;
          Mains.ZCD_Pre_state_lo = RESET;
        }
   
      }
    }
    else if (Mains.Check_ZCD == SET)
    {
      Mains.Check_ZCD =0;
      Mains.start_rd_vol = RESET;
      /*
      if(Inverter_timers.ZCD_counter_Hi < 82 && Inverter_timers.ZCD_counter_Hi > 69 && Inverter_timers.ZCD_counter_Lo < 72 && Inverter_timers.ZCD_counter_Lo > 62)
      {  
        Mains.frq =   ((float)(Inverter_timers.ZCD_counter_Hi+ Inverter_timers.ZCD_counter_Lo))/2.89;
      }
      */
       if(Inverter_timers.ZCD_counter_Hi < 30 && Inverter_timers.ZCD_counter_Hi > 24 && Inverter_timers.ZCD_counter_Lo < 28 && Inverter_timers.ZCD_counter_Lo > 20)
      {  
        Mains.frq =   ((float)(Inverter_timers.ZCD_counter_Hi+ Inverter_timers.ZCD_counter_Lo))/1.09;
      }
     
     
      Inverter_timers.ZCD_counter_Hi =0;
      Inverter_timers.ZCD_counter_Lo=0;
      Mains.ZCD_final_state =RESET;
      Mains.ZCD_Pre_state_lo = RESET;
      Mains.ZCD_Pre_state_hi = RESET;
      Mains.start_rd_vol==RESET;
      
    }
    if(ZCD_Read == SET)
    {
      Mains.Fail_Timer_1=0;
      if(Mains.Fail_Timer > 17 && Mains.Fail_Timer < 50)
      {
 
    	  if(Flags_F.mains_fails_detected_start == 0)
    	  {
			if(ADC_DMA_Buff[7] <50)
			{
				Flags_F.mains_fails_detected_start = 1;
				ParameterSet.Mains_Fails_Detect_time = 0;
			 
			}
    	  }
    	  else
    	  {

    		  if(ADC_DMA_Buff[7] <50)
    		  {

    			  if(ParameterSet.Mains_Fails_Detect_time++ > 7)
    			  {
                              
              Mains.Fail_Timer = 10000;
                                  
              ParameterSet.test++;

              Mains_flags.MAINS_OK = 0;
              Inverter_ParameterSet.PWM_gridChrg = 0;
              MAINS_OFF;
              Mains_flags.available = RESET; 
              LED.grid_charging_on=0;
              if(Mains.charging_disabled ==  RESET)
              {                                      
                Mains.charging_disabled =SET;
                Mains_flags.charging_start = 0;
                
                HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
                HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
                HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);  
                HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);
                
                htim1.Instance->CCR3 =0;
                htim1.Instance->CCR2=0;
              }                                                           
    			  }

    		  }
    		  else
          {
            Flags_F.mains_fails_detected_start =0;
            ParameterSet.Mains_Fails_Detect_time = 0;
                                      
          }
    	  }
      }
      if(Mains.Fail_Timer++ > 80)
      {
        Mains.start_rd_vol = RESET;
        Mains.Fail_Timer = 500;
        Inverter_timers.ZCD_counter_Hi =0;
        Inverter_timers.ZCD_counter_Lo=0;
        Mains.ZCD_final_state =RESET;
        Mains.ZCD_Pre_state_lo = RESET;
        Mains.ZCD_Pre_state_hi = RESET;
        Mains.start_rd_vol==RESET;
        Mains.frq =0;
        Mains.voltage =0;

        Flags_F.mains_fails_detected_start =0;
       ParameterSet.Mains_Fails_Detect_time = 0;
      }
      
      
      
    }
    // check mains available or not 
    if(Mains_flags.Grid_connect_recheck==0 && Mains.frq > 47.0 /*Mains.Frq_low_limit*/ && Mains.frq <  53.0 /*Mains.Frq_hi_limit*/ && Mains.voltage > (Inverter_ParameterSet.grid_on_voltage_level+10 ) && Mains.voltage < (Inverter_ParameterSet.grid_off_voltage_level-10))
    {
      Mains.mains_Fail_Timer = 0;
      if(Mains.mains_ok_Timer++ > 15000)
      {
      Mains.mains_ok_Timer=0;
      Mains_flags.MAINS_OK = 1;
      Mains_flags.under_voltage =0;
      Mains_flags.over_voltage =0;
     
      
      }
    }
    else if( ((Mains.voltage < Inverter_ParameterSet.grid_on_voltage_level-10) || (Mains.voltage > Inverter_ParameterSet.grid_off_voltage_level+10)))
    {
      
      if(Mains.mains_Fail_Timer++ > 850)
      {
      Mains_flags.Grid_connect_recheck=1;
      Mains.mains_Fail_Timer=0;
      Mains_flags.MAINS_OK = 0;
      Inverter_ParameterSet.PWM_gridChrg = 0;
      MAINS_OFF;
      Mains_flags.available = RESET; ///added 12-08-19
      LED.grid_charging_on=0;
      if(Mains.charging_disabled ==  RESET)
      {
       // MAINS_OFF;
        Mains.charging_disabled =SET;
              Mains_flags.charging_start = 0;
              ////added 01122018
        HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
        HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
        HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);  
        HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);
              
              ///
          htim1.Instance->CCR3 =0;//Inverter_ParameterSet.PWM_gridChrg;
         htim1.Instance->CCR2=0;// = Inverter_ParameterSet.PWM_gridChrg;
      }
      }
    }
   /* 
     if(Mains_flags.Grid_connect_recheck==1 && Mains.frq > 47.0  && Mains.frq <  53.0  && Mains.voltage > (Inverter_ParameterSet.grid_on_voltage_level+10 ) && Mains.voltage < (Inverter_ParameterSet.grid_off_voltage_level-10))
    {
      Mains.mains_Fail_Timer = 0;
      if(Mains.mains_ok_Timer++ > 15000)
      {
      Mains.mains_ok_Timer=0;
      Mains_flags.MAINS_OK = 1;
      Mains_flags.under_voltage =0;
      Mains_flags.over_voltage =0;
    
      
      }
    }  
    */ 

  }
  
  if(Mains.voltage < 100)
  {
  if(Mains.mains_absent_Timer++>1000)
     Mains_flags.Grid_connect_recheck=0;
    
  
  }
  
  
  
}

void Inverter__SinewaveGeneration (void)
{
 
  if(Stsrt_apps)
  {
   ZCD__Sensing();
   // Adc_Avg();

   // inv_op_err = 1000;
    if(++sampling_time >= 0/*5*/ && Inverter_flags.output_on && (Flags_F.inv_sw_state==SET) && Inverter_flags.overload==RESET && Flags_fault.NTC_INV_Fail == RESET)
    {
      Inverter_flags.inv_short_ckt_check=1;
      if(Inverter_flags.outputPWM_INV_Started == 0)
      {
        htim1.Instance->CCR2=0;
        htim1.Instance->CCR3=0;
        HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2);                //start pwm channel 1 //always after ADC conversion//test1
        HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_2);
        HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_3); 
        HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_3);
        htim1.Instance->CCR2=0;
        htim1.Instance->CCR3=0;
        Inverter_flags.outputPWM_INV_Started = SET;        
      }
       
      INVERTER_ON;
      sampling_time =0;
      ++sin_step;
      if(sin_step >= SINEBUFFERLENGTH)// 100
      {
        sin_step =0;
        pZCROSS = !pZCROSS;
         if (pZCROSS==0 && Changeover_timer ==1)
           
         { 
           sin_step=0;
          Changeover_timer =0;
         }
      }
      if(pZCROSS)
      {
        /* No need to restart again and again issues: Dead PWM coming on INV drive  */
        
        //HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2);                //start pwm channel 1 //always after ADC conversion//test1
       // HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_2);
        htim1.Instance->CCR2 =(uint16_t)(SineBuffer[sin_step]*(inv_op_err/1000));
        htim1.Instance->CCR3 =0;
      }
      else if (!pZCROSS)
      {
        htim1.Instance->CCR2=0;
      // HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_3);                //start pwm channel 1 //always after ADC conversion//test1
       //HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_3);
        htim1.Instance->CCR3 =(uint16_t)(SineBuffer[sin_step]*(inv_op_err/1000));
      }
      
    }
    
    else if(Inverter_flags.output_switched_off == RESET) /* if PWM already OFF then no need off again & again.
  //issue: TIMER Fun take time and main for loop unreachable and unexpected behaviours.*/
    {
      if(Inverter_flags.output_on == RESET || Flags_F.inv_sw_state == RESET || Inverter_flags.overload==SET || Flags_fault.NTC_INV_Fail == SET)
      {
        HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
        HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
        HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);
        HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);
        htim1.Instance->CCR2 =0;
        htim1.Instance->CCR3 =0;
        sampling_time =0;
        Inverter_flags.output_switched_off = SET;
        Inverter_flags.outputPWM_INV_Started = RESET;
        Mains_flags.soft_start = RESET;
       // delayUs(500);// never add such delay in Interrputs of fast & hight priority timers etc.
        if(Flags_F.inv_sw_state == RESET)
        {
          Inverter_flags.overload=RESET;
          pZCROSS = 1;
          sin_step=80;
        }
        inv_op_err = 101; //250
        INVERTER_OFF;
        
      }
    } 
  }
  
}

void Inverter__SwitchState(void)
{
	if(USER_INV_SWITCH)
	{
          if(TIMERt.Debounce_Counter++ > 250)//21000)
          {             
              TIMERt.Debounce_Counter =251;//21000;
          }
	}
        else if(TIMERt.Debounce_Counter > 70 && TIMERt.Debounce_Counter < 251)
        {
          TIMERt.Debounce_Counter =0;
          Flags_F.inv_sw_state = (!Flags_F.inv_sw_state);
          
        }
        else TIMERt.Debounce_Counter = 0;

}

void Inverter__ModeHandler(void)
{
 //if()




}
