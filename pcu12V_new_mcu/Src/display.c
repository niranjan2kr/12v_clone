
/*BUZZER PIN
BUZZER_Pin              
BUZZER_GPIO_Port        
*/
#include "Display.h"
#include <string.h>
#include "hal_types.h"
#include "main.h"
#include "global_var.h"
////
char parameter_name[20][20];
char unit_name[20][20];
float Display_Buff[20];
char float_buff[20];
uint8_t display=13;

enum{
      BAT_VOLTAGE_RANK 	        =	0,
      BAT_CHRG_RANK 		=	1,
      SOLAR_VOLTAGE_RANK 	=	2,
      SOLAR_CURRENT_RANK 	=	3,
      SOLAR_KW_RANK 		=	4,
      SOLAR_KWh_RANK 		=	5,
      INVERTER_OP_VOL_RANK 	=	6,
      INVERTER_OP_CUR_RANK 	=	7,
      INV_FRQ_RANK  		=	10,
      GRID_VOL_RANK		=	8,
      GRID_CUR_RANK             =       9,
      GRID_FRQ_RANK  		=	11,
      BAT_DSCHRE_RANK 	        =	12,
};

void Welcome_Show(void)
{
HAL_UART_Transmit(&huart2,(uint8_t *)"\r-----------------LCD-------------",36,50);
    LCD_INI();
    delayUs(1000);
    LCD_INI();
  delayUs(50000);
  LCD_Clear();
  delayUs(1000);
  LCD_Clear();
  delayUs(5000);
  LCD_Line(0,1);
  LCD_Puts(" PCU 24V - 1KVA ");
  delayUs(5000);
  LCD_Line(0,2);
  LCD_Puts("   Ver: 1.2" );

HAL_UART_Transmit(&huart2,(uint8_t *) "\n\rDisplay Initialized \n\r ",25,50);//debug
strcpy(parameter_name[0],"BATTERY");
strcpy(parameter_name[1],"SOLAR  ");
strcpy(parameter_name[2],"SOLAR  ");
strcpy(parameter_name[3],"OUTPUT ");//
strcpy(parameter_name[4],"GRID   ");
strcpy(parameter_name[5],"FAULTS ");

strcpy(unit_name[SOLAR_VOLTAGE_RANK],"V");
strcpy(unit_name[SOLAR_CURRENT_RANK],"A");
strcpy(unit_name[BAT_VOLTAGE_RANK],"V");
strcpy(unit_name[BAT_CHRG_RANK],"A");
strcpy(unit_name[INVERTER_OP_VOL_RANK],"V");
strcpy(unit_name[INVERTER_OP_CUR_RANK],"A");
strcpy(unit_name[GRID_VOL_RANK],"V");
strcpy(unit_name[GRID_CUR_RANK],"A");
strcpy(unit_name[BAT_DSCHRE_RANK],"A");
strcpy(unit_name[SOLAR_KW_RANK],"W");
strcpy(unit_name[SOLAR_KWh_RANK],"Kwh");
strcpy(unit_name[INV_FRQ_RANK],"Hz");
strcpy(unit_name[GRID_FRQ_RANK],"Hz");
  
}

_Bool batt_dis=0;

void Parameters_Display(void)
{  
 if(inc_key_dis_pressed)
 {
   if(display++>15)    //7
     display =0;
 }

 if(dec_key_dis_pressed)
 {
   if(display > 0)
     display --;
 else display = 15;      //7
 }
 inc_key_dis_pressed =0;
  inc_key_dis_pressed =0;
   
  //////
  Display_Buff[SOLAR_VOLTAGE_RANK]= Solar.Voltage;//Solar_V;//Batt_i_in;//ADC_Buff_float[0];//batt_i_+
  Display_Buff[SOLAR_CURRENT_RANK]=Solar.Current;
  Display_Buff[BAT_VOLTAGE_RANK]=Battery.Voltage;
 // if(Battery.Discharging_current>2.0)
    if(Battery.Discharging_current > Battery.Current)
  {
    batt_dis = 1;
    Display_Buff[BAT_CHRG_RANK]=Battery.Discharging_current;
  }
  else
  {
    Display_Buff[BAT_CHRG_RANK]=Battery.Current;
    batt_dis = 0;
  }
  Display_Buff[INVERTER_OP_VOL_RANK]=Inverter_OP.voltage;
  Display_Buff[INVERTER_OP_CUR_RANK]=Inverter_OP.current;
  Display_Buff[GRID_VOL_RANK]=Mains.voltage;
  Display_Buff[GRID_CUR_RANK] = Mains.curent;//
  Display_Buff[BAT_DSCHRE_RANK]=Battery.Discharging_current;
  Display_Buff[SOLAR_KW_RANK]=Solar.Voltage*Solar.Current;
  Display_Buff[SOLAR_KWh_RANK]= ParameterSet.test; //Battery.Current*Battery.Voltage/1000;
  Display_Buff[GRID_FRQ_RANK]=Mains.frq;
  Display_Buff[INV_FRQ_RANK]=Inverter_OP.frq;
  if(display<5)
  {
    LCD_INI();
    LCD_Clear();
    LCD_Line(5,1);
    LCD_Puts((char*)&parameter_name[display]);
    LCD_Line(0,2);
   
    sprintf((char*)float_buff,"%.1f %s",Display_Buff[display*2],unit_name[display*2]);
    LCD_Puts((char*)&float_buff);
    LCD_Line(9,2);
     if(batt_dis == 1 && display==0)
    {
      sprintf((char*)float_buff,"%s%.1f %s","-",Display_Buff[(display*2+1)],unit_name[(display*2)+1]);
   
    }
    else sprintf((char*)float_buff,"%.1f %s",Display_Buff[(display*2+1)],unit_name[(display*2)+1]);
    
    LCD_Puts((char*)&float_buff);
    
    if(display ==3   )
    {
      
      LCD_Line(9,1);
    sprintf((char*)float_buff,"%.1f %s",Display_Buff[INV_FRQ_RANK],unit_name[INV_FRQ_RANK]);
    LCD_Puts((char*)&float_buff);
      
    }
     if(display ==4 )
    {
      
      LCD_Line(9,1);
    sprintf((char*)float_buff,"%.1f %s",Display_Buff[GRID_FRQ_RANK],unit_name[GRID_FRQ_RANK]);
    LCD_Puts((char*)&float_buff);
      
    }
  } 
  /////
    if(display == 5)
    {
       if(Flags_fault.batt_under_voltage == 1)
      {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("     FAULT      ");
        LCD_Line(0,2);
        LCD_Puts("    Batt Low    ");
      }
      else display++;
    }
  if(display == 6)
    {
       if(Flags_fault.batt_over_voltage == 1)
      {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("     FAULT      ");
        LCD_Line(0,2);
        LCD_Puts("   Batt Over    ");
      }
      else display++;
    }
  if(display == 7)
    {
       if(Flags_fault.pv_over_voltage == 1)
      {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("     FAULT      ");
        LCD_Line(0,2);
        LCD_Puts("    PV Over     ");
      }
      else display++;
    }
  if(display == 8)
    {
       if(Flags_fault.pv_under_voltage == 1)
      {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("    PV STATUS      ");
        LCD_Line(0,2);
        LCD_Puts("     PV Low     ");
      }
      else display++;
    }
  if(display == 9)
    {
       if(Inverter_flags.overload == 1)
      {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("     FAULT      ");
        LCD_Line(0,2);
        LCD_Puts("    Over Load   ");
      }
      else display++;
    }
  
  if(display == 10)
    {
       if(Flags_fault.pv_over_load == 1)
      {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("     FAULT      ");
        LCD_Line(0,2);
        LCD_Puts("  PV Over Load  ");
      }
      else display++;
    }
  
  if(display == 11)
    {
       if(Flags_fault.NTC_INV_Fail == 1)
      {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("     FAULT      ");
        LCD_Line(0,2);
        LCD_Puts("Inverter NTC Flt");
      }
      else display++;
    }
  
   if(display == 12)
    {
       if(Mains_flags.under_voltage == 1)
      {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("     FAULT      ");
        LCD_Line(0,2);
        LCD_Puts("Mains under vol");
      }
      else display++;
    }
  
   if(display == 13)
    {
       if(Mains_flags.over_voltage == 1)
      {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("     FAULT      ");
        LCD_Line(0,2);
        LCD_Puts(" Mains over vol ");
      }
      else display++;
    }
  
  if(display == 14)
    {
       if(Flags_fault.Short_ckt == 1)
      {
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        LCD_Puts("     FAULT      ");
        LCD_Line(0,2);
        LCD_Puts(" INV SHORT CKT ");
      }
      else display++;
    }
  
  
  ////
  if(display==15)
  {
    char buff[5];  
    sprintf((char*)buff,"[%d%d%d%d%d%d]",PCUX_ID[0],PCUX_ID[1],PCUX_ID[2],PCUX_ID[3],PCUX_ID[4],PCUX_ID[5]);
    LCD_INI();
    LCD_Clear();
    LCD_Line(0,1);
    LCD_Puts("STATCON PCU ID");
    LCD_Line(0,2);
    LCD_Puts((char*)&buff);
  }
  if(display==16)
  {
   RTC_Get_Time_Date();
    LCD_INI();
    LCD_Clear();
    LCD_Line(0,1);
    LCD_Puts("DATE");
    LCD_Line(5,1);
    LCD_Puts((char*)&aShowDate);
    LCD_Line(0,2);
    LCD_Puts("TIME");
    LCD_Line(5,2);
    LCD_Puts((char*)&aShowTime);
  }
  if(Keypad.version_dis == 0)
  display++;
  else
  {
    LCD_Line(15,2);
    LCD_Puts("@");
    
  }
  if(display>16)
  {
    display=0;
  }
}



void Fault_led(void)
{
        if(Flags_fault.batt_under_voltage == 1 || Flags_fault.batt_over_voltage == 1 ||Flags_fault.pv_over_voltage == 1 
            || Flags_fault.pv_over_load == 1  || Flags_fault.NTC_INV_Fail == 1 || Flags_fault.NTC_MPPT_Fail == 1 ||
             Inverter_flags.overload ==1 || Mains_flags.over_voltage == 1)
          {
        FAULT_ON;
          }
        else
        {
          FAULT_OFF;
        }

      if((Flags_fault.batt_under_voltage == 1 || Flags_fault.batt_over_voltage == 1 ||Flags_fault.pv_over_voltage == 1 
            || Flags_fault.pv_over_load == 1  || Flags_fault.NTC_INV_Fail == 1 || Flags_fault.NTC_MPPT_Fail == 1 ||
             Inverter_flags.overload ==1 || Mains_flags.over_voltage == 1 || Flags_fault.Short_ckt == 1) && Flags_F.mute==0 )
          {
        BUZZER_ON;
          }
        else
        {
          BUZZER_OFF;
        }  
        
          if(Flags_fault.batt_under_voltage == 0 && Flags_fault.batt_over_voltage == 0 && Flags_fault.pv_over_voltage == 0 
            && Flags_fault.pv_over_load == 0  && Flags_fault.NTC_INV_Fail == 0 && Flags_fault.NTC_MPPT_Fail == 0 &&
             Inverter_flags.overload ==0 && Mains_flags.over_voltage == 0 && Flags_fault.Short_ckt ==0 && Flags_F.mute==1 )
          {
        Flags_F.mute=0;
         BUZZER_OFF;
          }

  
}

