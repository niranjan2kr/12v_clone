//LCD Display
//D4  LCD_D4_Pin  LCD_D4_GPIO_Port
//D5  LCD_D5_Pin  LCD_D5_GPIO_Port
//D6  LCD_D6_Pin  LCD_D6_GPIO_Port
//D7  LCD_D7_Pin  LCD_D7_GPIO_Port
//RS  LCD_RS_Pin  LCD_RS_GPIO_Port
//EN  LCD_EN_Pin  LCD_EN_GPIO_Port

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "LCD.h"
extern uint32_t uCount;
extern uint32_t Int_tim2_flag;
void delayUs(uint32_t micros);
void delayUs(__IO uint32_t micros);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
uint32_t getUs(void)
{
uint32_t usTicks = HAL_RCC_GetSysClockFreq() / 1000000;
register uint32_t ms, cycle_cnt;
do {
ms = HAL_GetTick();
cycle_cnt = SysTick->VAL;
} 
while (ms != HAL_GetTick());
return (ms * 1000) + (usTicks * 1000 - cycle_cnt) / usTicks;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

extern uint32_t SystemCoreClock;
 
void DWT_Init(void) 
{
  if (!(CoreDebug->DEMCR & CoreDebug_DEMCR_TRCENA_Msk)) 
  {
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    DWT->CYCCNT = 0;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
  }
}
 
uint32_t DWT_Get(void)
{
  return DWT->CYCCNT;
}
 
uint8_t DWT_Compare(int32_t tp)
{
  return (((int32_t)DWT_Get() - tp) < 0);
}
 /*
void delayUs(uint32_t us) // microseconds
{
  int32_t tp = DWT_Get() + us * (SystemCoreClock/1000000);
  while (DWT_Compare(tp));
}

void delayUs(uint32_t micros)
{
uint32_t start = getUs();
while (getUs()-start < (uint32_t) micros) {
asm("nop");
}
}
*/
void delayUs(uint32_t micros)
{
  uint32_t uCount=0;
while (uCount<(micros*10))
{
  uCount++;
}
uCount=0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LCD_Enable (void)
{
HAL_GPIO_WritePin(EN_GPIO_Port,EN_Pin,GPIO_PIN_RESET); 
delayUs(3);
HAL_GPIO_WritePin(EN_GPIO_Port,EN_Pin,GPIO_PIN_SET); 
delayUs(50);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LCD_Send(unsigned char DATA)
{
uint8_t val;

val=DATA&0x01;

if(val==0)
{
 HAL_GPIO_WritePin(D4_GPIO_Port,D4_Pin,GPIO_PIN_RESET); 
}
else
{
 HAL_GPIO_WritePin(D4_GPIO_Port,D4_Pin,GPIO_PIN_SET); 
}

val=(DATA>>1)&1;

if(val==0)
{
 HAL_GPIO_WritePin(D5_GPIO_Port,D5_Pin,GPIO_PIN_RESET); 
}
else
{
 HAL_GPIO_WritePin(D5_GPIO_Port,D5_Pin,GPIO_PIN_SET); 
}

val=(DATA>>2)&1;

if(val==0)
{
 HAL_GPIO_WritePin(D6_GPIO_Port,D6_Pin,GPIO_PIN_RESET); 
}
else
{
 HAL_GPIO_WritePin(D6_GPIO_Port,D6_Pin,GPIO_PIN_SET); 
}

val=(DATA>>3)&1;

if(val==0)
{
 HAL_GPIO_WritePin(D7_GPIO_Port,D7_Pin,GPIO_PIN_RESET); 
}
else
{
 HAL_GPIO_WritePin(D7_GPIO_Port,D7_Pin,GPIO_PIN_SET); 
}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LCD_SendCommand(unsigned char command)
{
  LCD_Send(command>>4);
  LCD_Enable();
  LCD_Send(command);
  LCD_Enable();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LCD_Line(unsigned char x, unsigned char y)
{
  unsigned char pos;
  
  if(y==1)
  pos=(Line_1+x);
  else if(y==2)
  pos=(Line_2+x);
  else if(y==3)
  pos=(Line_3+x);
  else if(y==4)
  pos=(Line_4+x);
  delayUs(100);
  LCD_SendCommand(pos);
  delayUs(900);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LCD_Clear(void)
{
  LCD_SendCommand(Clear_LCD);
  delayUs(10);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LCD_PutChar(unsigned char DATA)
{
  HAL_GPIO_WritePin(RS_GPIO_Port,RS_Pin,GPIO_PIN_SET);
  LCD_SendCommand(DATA);
  HAL_GPIO_WritePin(RS_GPIO_Port,RS_Pin,GPIO_PIN_RESET);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LCD_Puts(char *ptr)
{
  unsigned char L[20];
  int i=0;
  while(*ptr && i < 16)
  {
    L[i]=*ptr;
    LCD_PutChar(L[i]);
    ptr++;
    i++;
  }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LCD_INI(void)
{
  LCD_Send(0x00);
  delayUs(100);
  HAL_GPIO_WritePin(RS_GPIO_Port,RS_Pin,GPIO_PIN_RESET);
  LCD_Send(0x03);
  LCD_Enable();
  delayUs(600);
  LCD_Enable();
  delayUs(100);
  LCD_Enable();
  LCD_Send(0x02);
  LCD_Enable();
  LCD_SendCommand(0x28);
  LCD_SendCommand(0x0C);
  LCD_SendCommand(0x06);
  LCD_SendCommand(Clear_LCD);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////