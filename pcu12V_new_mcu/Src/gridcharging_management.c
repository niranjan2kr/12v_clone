#include "version_def.h"

extern TIM_HandleTypeDef htim1;

void GridCharging__Faults(void)
{
  if(Mains_flags.health_check == RESET && Mains_flags.available == SET)
  {
    if(TIMERt.Mains_HealthCHeckTimer++ > 35)// 2-4 mins approx
    {
      TIMERt.Mains_HealthCHeckTimer = 0;
      Mains_flags.health_check = SET;
      TIMERt.Manis_UV_Timer =0;
      TIMERt.Manis_OV_Timer =0;
    }
  }
  //////////////////////////////////////////////
  if(Mains_flags.health_check == SET)
  {
    if(Mains.voltage < Inverter_ParameterSet.gridcharging_start_voltage )
    {
      if(TIMERt.Manis_UV_Timer++ > 100)
      {
        Mains_flags.under_voltage = SET;
        TIMERt.Manis_UV_Timer = 0;
        
        Mains_flags.health_check = RESET;
      }
    }
    else if(Mains.voltage > Inverter_ParameterSet.gridcharging_stop_voltage)
    {
      if(TIMERt.Manis_OV_Timer++ > 150)
      {
         Mains_flags.over_voltage = SET;
         TIMERt.Manis_OV_Timer = 0; 
         Mains_flags.health_check = RESET;
      }
    }
    else
    {
      TIMERt.Manis_UV_Timer =0;
      TIMERt.Manis_OV_Timer =0;
      
    }
  }
  
  if( Flags_fault.NTC_INV_Fail == RESET && Mains_flags.available == SET && Inverter_flags.output_on == RESET && Mains_flags.under_voltage == RESET && Mains_flags.over_voltage == RESET)
  {
    Mains_flags.charging_start=SET;
 
    
  }
  else if( Flags_fault.NTC_INV_Fail == SET || Mains_flags.MAINS_OK == RESET /*|| Inverter_flags.output_on == SET  || Mains_flags.under_voltage == SET || Mains_flags.over_voltage == SET */ )
  {
      Mains_flags.charging_start = RESET;
       GRID_CHARGING_OFF;
                  TIMERt.GridDisconnect_delay = 0;
                  TIMERt.GridConnect_delay = 0;   
                  TIMERt.GridDisconnect_delay_2=0;
      // Inverter_ParameterSet.PWM_gridChrg = 0;
  }
 
}
  


void GridCharging__Management(void)
{
  if(Mains_flags.charging_start == SET &&  Mains_flags.charging_stop == RESET && Flags_fault.NTC_INV_Fail == RESET)
  {
    Mains.charging_disabled = RESET;
    Mains_flags.grid_charging_indication = SET;
    
    //if(Mains_flags.soft_start == RESET)
    {
      //Mains_flags.soft_start = SET;
      HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);                //start pwm channel 1 //always after ADC conversion//test1
      HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_2);
      HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);                //start pwm channel 1 //always after ADC conversion//test1
      HAL_TIMEx_PWMN_Start(&htim1,TIM_CHANNEL_3); 
    }
    //else
    {
      htim1.Instance->CCR3 =Inverter_ParameterSet.PWM_gridChrg;
      htim1.Instance->CCR2 = Inverter_ParameterSet.PWM_gridChrg;  
    }
     /* 
    if( Mains_flags.chargigng_boost_start && Battery.Voltage>23.3 && Mains_flags.chargigng_float_start)
      {
        if(Inverter_timers.Grid_chrg_Stop_boost2float++ > 15000)
        {
          Inverter_timers.Grid_chrg_Stop_boost2float = 0;
          Inverter_ParameterSet.PWM_gridChrg =0;
          Mains_flags.charging_stop = SET;
          htim1.Instance->CCR3 =Inverter_ParameterSet.PWM_gridChrg;
          htim1.Instance->CCR2 = Inverter_ParameterSet.PWM_gridChrg;
        }
      }
      else if( Mains_flags.chargigng_boost_start && Battery.Voltage<23.0)
      {
        Inverter_timers.Grid_chrg_Stop_boost2float = 0;
        Mains_flags.chargigng_boost_start = 0;
      }
    if(Mains_flags.chargigng_float_start)
      {
        if(Inverter_timers.Grid_chrg_Stop_float++ > 15000)
        {
          Inverter_timers.Grid_chrg_Stop_float = 0;
          Inverter_ParameterSet.PWM_gridChrg =0;
          Mains_flags.charging_stop = SET;
          htim1.Instance->CCR3 =Inverter_ParameterSet.PWM_gridChrg;
          htim1.Instance->CCR2 = Inverter_ParameterSet.PWM_gridChrg;
        }
      }
      */
    ///////////////////////////////////////////////////////////
    
    if(Battery.Current <= Inverter_ParameterSet.gridcharging_min_cur)
    {
       Mains_flags.chargigng_float_start = SET;
      if(Battery.Voltage <= Inverter_ParameterSet.gridcharging_float_voltage-0.5)
      {
        if(Inverter_ParameterSet.PWM_gridChrg <  750)//Inverter_ParameterSet.PWM_grid_limt )
        {
           Inverter_ParameterSet.PWM_gridChrg = Inverter_ParameterSet.PWM_gridChrg+ 1;//Inverter_ParameterSet.PWM_GridStep_inc;
        }
      }
      else if(Battery.Voltage > Inverter_ParameterSet.gridcharging_float_voltage+0.5)
      {
        if(Inverter_ParameterSet.PWM_gridChrg > Inverter_ParameterSet.PWM_grid_min )
        {
           Inverter_ParameterSet.PWM_gridChrg = Inverter_ParameterSet.PWM_gridChrg- 1;//Inverter_ParameterSet.PWM_GridStep_dec;
        }
      }
    }
    else if(Battery.Current > Inverter_ParameterSet.gridcharging_min_cur)
    {
      Mains_flags.chargigng_boost_start = SET;
      Mains_flags.chargigng_float_start = RESET;
      if(Battery.Voltage <= Inverter_ParameterSet.gridcharging_boost_voltage-0.25 && Battery.Current < Inverter_ParameterSet.gridcharging_BCL-0.5 )
      {
        if(Inverter_ParameterSet.PWM_gridChrg < 750)
        {
           Inverter_ParameterSet.PWM_gridChrg = Inverter_ParameterSet.PWM_gridChrg + 1;
        }
      }
      else if(Battery.Voltage > Inverter_ParameterSet.gridcharging_boost_voltage + 0.25 || Battery.Current > Inverter_ParameterSet.gridcharging_BCL +0.5)
      {
        if(Inverter_ParameterSet.PWM_gridChrg > 10)
        {
          
          if(Battery.Voltage>Inverter_ParameterSet.gridcharging_boost_voltage+1)
          {
          Inverter_ParameterSet.PWM_gridChrg = Inverter_ParameterSet.PWM_gridChrg - 10;
          }
          else
             Inverter_ParameterSet.PWM_gridChrg = Inverter_ParameterSet.PWM_gridChrg - 1;
        
          }
      }
     }
   } 
  else if(Flags_fault.NTC_INV_Fail == SET)
  {
      HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);                //start pwm channel 1 //always after ADC conversion//test1
      HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
       HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);                //start pwm channel 1 //always after ADC conversion//test1
      HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);
      Inverter_ParameterSet.PWM_gridChrg =0;
      htim1.Instance->CCR3 =0;
       htim1.Instance->CCR2 =0;
       //FAULT_ON;
  }
  
  if(Mains_flags.charging_start == RESET)
  {
    
    Mains_flags.grid_charging_indication= RESET;
  }
  
 
}

