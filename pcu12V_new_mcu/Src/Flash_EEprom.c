/*
storage in EEPROM
All EEPROM read and WRite functions and address must be written Here
*/
#include "version_def.h"

///////Page Enum//////////


#define PAGE_LENGTH	100
#define STRT_CHR        'I'
uint8_t DataBase[6][100];
uint8_t DataBaseMirror[6][100];



extern I2C_HandleTypeDef hi2c1;
uint32_t address=0x08060000;


uint8_t EE_buffer[PAGE_LENGTH*TOTAL_PAGE];
void Write_data_to_Flash(void)
{
  
      Write_DataFlash(&DataBaseMirror[0][0],PAGE_LENGTH*TOTAL_PAGE);
}

void update_to_flash(void)// i.e user
{
   DataBase_Update(INIT_PAGE,INV_MODE_PRIORITY,Inverter_Mode.Mode_data);
  
    DataBase_Update(USER_PAGE,CELL_TYPE,HI_BYTE((uint16_t)(ParameterSet.batt_type)));
    
    DataBase_Update(USER_PAGE,INV_OP_CUR_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.op_current_max*10)));//
    DataBase_Update(USER_PAGE,INV_OP_CUR_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.op_current_max*10)));//
    
    DataBase_Update(USER_PAGE,BAT_LOWCUT_REC_HI,HI_BYTE((uint16_t)ParameterSet.batt_reconnect_vol*10));//
    DataBase_Update(USER_PAGE,BAT_LOWCUT_REC_LO,LO_BYTE((uint16_t)ParameterSet.batt_reconnect_vol*10));
    
    DataBase_Update(USER_PAGE,CELL_NUM_HI,HI_BYTE((uint16_t)(ParameterSet.cell_number*10)));
    DataBase_Update(USER_PAGE,CELL_NUM_LO,LO_BYTE((uint16_t)(ParameterSet.cell_number*10)));
  
    DataBase_Update(USER_PAGE,BATT_CAPCITY_HI,HI_BYTE((uint16_t)(ParameterSet.batt_capacity*10)));
    DataBase_Update(USER_PAGE,BATT_CAPCITY_LO,LO_BYTE((uint16_t)(ParameterSet.batt_capacity*10)));
    
    DataBase_Update(USER_PAGE,PV_UNDER_V_HI,HI_BYTE((uint16_t)(ParameterSet.PV_undervoltage*10)));
    DataBase_Update(USER_PAGE,PV_UNDER_V_LO,LO_BYTE((uint16_t)(ParameterSet.PV_undervoltage*10)));

    DataBase_Update(USER_PAGE,PV_CUR_LIMT_HI,HI_BYTE((uint16_t)(ParameterSet.pv_overload_watts*10)));
    DataBase_Update(USER_PAGE,PV_CUR_LIMT_LO,LO_BYTE((uint16_t)(ParameterSet.pv_overload_watts*10)));

    DataBase_Update(USER_PAGE,PV_OVER_V_HI,HI_BYTE((uint16_t)(ParameterSet.PV_overvoltage*10)));
    DataBase_Update(USER_PAGE,PV_OVER_V_LO,LO_BYTE((uint16_t)(ParameterSet.PV_overvoltage*10)));

    DataBase_Update(USER_PAGE,BAT_MIN_CUR_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_min_current*10)));
    DataBase_Update(USER_PAGE,BAT_MIN_CUR_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_min_current*10)));

    DataBase_Update(USER_PAGE,BAT_MAX_CUR_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_max_current*10)));
    DataBase_Update(USER_PAGE,BAT_MAX_CUR_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_max_current*10)));

    DataBase_Update(USER_PAGE,BAT_BOOST_V_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_Boost_voltage*10)));
    DataBase_Update(USER_PAGE,BAT_BOOST_V_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_Boost_voltage*10)));

    DataBase_Update(USER_PAGE,BAT_FLOAT_V_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_Float_voltage*10)));
    DataBase_Update(USER_PAGE,BAT_FLOAT_V_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_Float_voltage*10)));

    DataBase_Update(USER_PAGE,BAT_LOWCUT_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_undervoltage*10)));
    DataBase_Update(USER_PAGE,BAT_LOWCUT_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_undervoltage*10)));

    DataBase_Update(USER_PAGE,BAT_HICUT_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_overvoltage*10)));
    DataBase_Update(USER_PAGE,BAT_HICUT_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_overvoltage*10)));

    DataBase_Update(USER_PAGE,BAT_HICUT_REC_HI,HI_BYTE((uint16_t)(ParameterSet.batt_Overreconnect_vol*10)));
    DataBase_Update(USER_PAGE,BAT_HICUT_REC_LO,LO_BYTE((uint16_t)(ParameterSet.batt_Overreconnect_vol*10)));
    
    DataBase_Update(USER_PAGE,BAT_DIS_V_HI,HI_BYTE((uint16_t)(ParameterSet.grid_discon_battvol*10)));
    DataBase_Update(USER_PAGE,BAT_DIS_V_LO,LO_BYTE((uint16_t)(ParameterSet.grid_discon_battvol*10)));

    DataBase_Update(USER_PAGE,BAT_RECON_V_HI,HI_BYTE((uint16_t)(ParameterSet.grid_reconnect_battvol*10)));
    DataBase_Update(USER_PAGE,BAT_RECON_V_LO,LO_BYTE((uint16_t)(ParameterSet.grid_reconnect_battvol*10)));
    
    DataBase_Update(USER_PAGE,GRID_DIS_B_C_HI,HI_BYTE((uint16_t)(ParameterSet.grid_discon_battCur*10)));
    DataBase_Update(USER_PAGE,GRID_DIS_B_C_LO,LO_BYTE((uint16_t)(ParameterSet.grid_discon_battCur*10)));

    DataBase_Update(USER_PAGE,GRIDCHRG_STOP_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_stop_voltage*10)));
    DataBase_Update(USER_PAGE,GRIDCHRG_STOP_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_stop_voltage*10)));

    DataBase_Update(USER_PAGE,GRID_UNDER_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.grid_on_voltage_level*10)));
    DataBase_Update(USER_PAGE,GRID_UNDER_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.grid_on_voltage_level*10)));

    DataBase_Update(USER_PAGE,GRID_OVER_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.grid_off_voltage_level*10)));
    DataBase_Update(USER_PAGE,GRID_OVER_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.grid_off_voltage_level*10)));

    DataBase_Update(USER_PAGE,GRIDCHRG_START_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_start_voltage*10)));
    DataBase_Update(USER_PAGE,GRIDCHRG_START_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_start_voltage*10)));

    //GRIDCHRG_BATT_START_V_HI
    
    DataBase_Update(USER_PAGE,GRIDCHRG_BATT_START_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_start_Battvoltage*10)));
    DataBase_Update(USER_PAGE,GRIDCHRG_BATT_START_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_start_Battvoltage*10)));
    
     DataBase_Update(USER_PAGE,GRIDCHRG_BATT_STOP_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_stop_Battvoltage*10)));
    DataBase_Update(USER_PAGE,GRIDCHRG_BATT_STOP_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_stop_Battvoltage*10)));
    
    
    DataBase_Update(USER_PAGE,GRIDCHRG_MIN_I_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_min_cur*10)));
    DataBase_Update(USER_PAGE,GRIDCHRG_MIN_I_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_min_cur*10)));

    DataBase_Update(USER_PAGE,GRIDCHRG_BAT_FLOAT_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_float_voltage*10)));
    DataBase_Update(USER_PAGE,GRIDCHRG_BAT_FLOAT_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_float_voltage*10)));

    DataBase_Update(USER_PAGE,GRIDCHRG_BAT_BOOST_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_boost_voltage*10)));
    DataBase_Update(USER_PAGE,GRIDCHRG_BAT_BOOST_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_boost_voltage*10)));

    DataBase_Update(USER_PAGE,GRIDCHRG_CUR_LIMIT_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_BCL*10)));
    DataBase_Update(USER_PAGE,GRIDCHRG_CUR_LIMIT_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_BCL*10)));

    DataBase_Update(USER_PAGE,PWM_GRIDCHRG,LO_BYTE((uint16_t)(Inverter_ParameterSet.PWM_gridChrg)));

    DataBase_Update(USER_PAGE,PWM_GRID_LIMIT_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.PWM_grid_limt*10)));
    DataBase_Update(USER_PAGE,PWM_GRID_LIMIT_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.PWM_grid_limt*10)));

    DataBase_Update(USER_PAGE,PWM_GRID_MIN,LO_BYTE((uint16_t)(Inverter_ParameterSet.PWM_grid_min)));

    DataBase_Update(USER_PAGE,PWM_GRID_STEP_INC,LO_BYTE((uint16_t)(Inverter_ParameterSet.PWM_GridStep_inc)));


    DataBase_Update(USER_PAGE,PWM_GRID_STEP_DEC,LO_BYTE((uint16_t)(Inverter_ParameterSet.PWM_GridStep_dec)));


    DataBase_Update(USER_PAGE,PWM_SOFT_FINAL_OFFSET,LO_BYTE((uint16_t)PWM.Soft2final_offset));


    DataBase_Update(USER_PAGE,PWM_DUTY_LIMIT_HI,HI_BYTE((uint16_t)PWM.Duty_limt*10));
    DataBase_Update(USER_PAGE,PWM_DUTY_LIMIT_LO,LO_BYTE((uint16_t)PWM.Duty_limt*10));

    DataBase_Update(USER_PAGE,PWM_DUTY_MIN,LO_BYTE((uint16_t)PWM.Duty_min));


    DataBase_Update(USER_PAGE,FRQ_MIN,LO_BYTE((uint16_t)Mains.Frq_low_limit));


    DataBase_Update(USER_PAGE,FRQ_MAX,LO_BYTE((uint16_t)Mains.Frq_hi_limit));

    /// factory //////////
    
    DataBase_Update(FACTORY_PAGE,LIMIT_INV_OP_CUR_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.op_current_max_Limit*10)));//
    DataBase_Update(FACTORY_PAGE,LIMIT_INV_OP_CUR_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.op_current_max_Limit*10)));
    
    DataBase_Update(FACTORY_PAGE,LIMIT_CELL_NUM_HI,HI_BYTE((uint16_t)(ParameterSet.cell_number_limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_CELL_NUM_LO,LO_BYTE((uint16_t)(ParameterSet.cell_number_limit*10)));
    
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_LOWCUT_REC_HI,HI_BYTE((uint16_t)(ParameterSet.batt_reconnect_vol_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_LOWCUT_REC_LO,LO_BYTE((uint16_t)(ParameterSet.batt_reconnect_vol_Limit*10)));
    
    DataBase_Update(FACTORY_PAGE,LIMIT_BATT_CAPCITY_HI,HI_BYTE((uint16_t)(ParameterSet.cell_capacity_limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BATT_CAPCITY_LO,LO_BYTE((uint16_t)(ParameterSet.cell_capacity_limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_PV_UNDER_V_HI,HI_BYTE((uint16_t)(ParameterSet.PV_undervoltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_PV_UNDER_V_LO,LO_BYTE((uint16_t)(ParameterSet.PV_undervoltage_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_PV_CUR_LIMT_HI,HI_BYTE((uint16_t)(ParameterSet.pv_overload_watts_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_PV_CUR_LIMT_LO,LO_BYTE((uint16_t)(ParameterSet.pv_overload_watts_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_PV_OVER_V_HI,HI_BYTE((uint16_t)(ParameterSet.PV_overvoltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_PV_OVER_V_LO,LO_BYTE((uint16_t)(ParameterSet.PV_overvoltage_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_MIN_CUR_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_min_current_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_MIN_CUR_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_min_current_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_MAX_CUR_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_max_current_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_MAX_CUR_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_max_current_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_BOOST_V_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_Boost_voltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_BOOST_V_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_Boost_voltage_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_FLOAT_V_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_Float_voltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_FLOAT_V_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_Float_voltage_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_LOWCUT_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_undervoltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_LOWCUT_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_undervoltage_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_HICUT_HI,HI_BYTE((uint16_t)(ParameterSet.Batt_overvoltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_HICUT_LO,LO_BYTE((uint16_t)(ParameterSet.Batt_overvoltage_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_DIS_V_HI,HI_BYTE((uint16_t)(ParameterSet.grid_discon_battvol_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_DIS_V_LO,LO_BYTE((uint16_t)(ParameterSet.grid_discon_battvol_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_RECON_V_HI,HI_BYTE((uint16_t)(ParameterSet.grid_reconnect_battvol_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_RECON_V_LO,LO_BYTE((uint16_t)(ParameterSet.grid_reconnect_battvol_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_GRID_DIS_B_C_HI,HI_BYTE((uint16_t)(ParameterSet.grid_discon_battCur_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRID_DIS_B_C_LO,LO_BYTE((uint16_t)(ParameterSet.grid_discon_battCur_Limit*10)));

    
    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_STOP_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_stop_voltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_STOP_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_stop_voltage_Limit*10)));
    
     DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BATT_START_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_start_Battvoltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BATT_START_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_start_Battvoltage_Limit*10)));
    
     DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BATT_STOP_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_stop_Battvoltage*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BATT_STOP_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_stop_Battvoltage*10)));
    
    

    DataBase_Update(FACTORY_PAGE,LIMIT_GRID_UNDER_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.grid_on_voltage_level_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRID_UNDER_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.grid_on_voltage_level_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_GRID_OVER_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.grid_off_voltage_level_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRID_OVER_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.grid_off_voltage_level_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_START_V_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_start_voltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_START_V_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_start_voltage_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_MIN_I_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_min_cur_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_MIN_I_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_min_cur_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BAT_FLOAT_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_float_voltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BAT_FLOAT_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_float_voltage_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BAT_BOOST_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_boost_voltage_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BAT_BOOST_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_boost_voltage_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_CUR_LIMIT_HI,HI_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_BCL_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_CUR_LIMIT_LO,LO_BYTE((uint16_t)(Inverter_ParameterSet.gridcharging_BCL_Limit*10)));

    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_HICUT_REC_HI,HI_BYTE((uint16_t)(ParameterSet.batt_Overreconnect_vol_Limit*10)));
    DataBase_Update(FACTORY_PAGE,LIMIT_BAT_HICUT_REC_LO,LO_BYTE((uint16_t)(ParameterSet.batt_Overreconnect_vol_Limit*10)));

    //calibration data
    DataBase_Update(CALIB_PAGE,CALIB_BATT_VOL_HI ,HI_BYTE((uint16_t)(Adc_Multiplier[FACTOR_POS_BATT_VOL] *10)));
    DataBase_Update(CALIB_PAGE,CALIB_BATT_VOL_LO,LO_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_BATT_VOL] *10 )));

    DataBase_Update(CALIB_PAGE,CALIB_BATT_CHRG_CUR_HI,HI_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_BATT_ICUR] *10 )));
    DataBase_Update(CALIB_PAGE,CALIB_BATT_CHRG_CUR_LO,LO_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_BATT_ICUR] *10 )));

    DataBase_Update(CALIB_PAGE,CALIB_BATT_DISCHRG_CUR_HI ,HI_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_BATT_OCUR] *10 )));
    DataBase_Update(CALIB_PAGE,CALIB_BATT_DISCHRG_CUR_LO,LO_BYTE((uint16_t)(Adc_Multiplier[FACTOR_POS_BATT_OCUR] *10  )));

    DataBase_Update(CALIB_PAGE,CALIB_INVERTER_OP_VOL_HI,HI_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_INVOP_VOL] *10 )));
    DataBase_Update(CALIB_PAGE,CALIB_INVERTER_OP_VOL_LO ,LO_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_INVOP_VOL] *10 )));

    DataBase_Update(CALIB_PAGE,CALIB_INVERTER_OP_CUR_HI,HI_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_INVOP_CUR] *10 )));
    DataBase_Update(CALIB_PAGE,CALIB_INVERTER_OP_CUR_LO ,LO_BYTE((uint16_t)(Adc_Multiplier[FACTOR_POS_INVOP_CUR] *10  )));

    DataBase_Update(CALIB_PAGE,CALIB_MAINS_VOL_HI,HI_BYTE((uint16_t)(Adc_Multiplier[FACTOR_POS_MAIN_VOL] *10  )));
    DataBase_Update(CALIB_PAGE,CALIB_MAINS_VOL_LO,LO_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_MAIN_VOL] *10 )));

    DataBase_Update(CALIB_PAGE,CALIB_MAINS_CUR_HI,HI_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_MAIN_CUR] *10 )));
    DataBase_Update(CALIB_PAGE,CALIB_MAINS_CUR_LO,LO_BYTE((uint16_t)(Adc_Multiplier[FACTOR_POS_MAIN_CUR] *10  )));

    DataBase_Update(CALIB_PAGE,CALIB_SOLAR_VOL_HI,HI_BYTE((uint16_t)(Adc_Multiplier[FACTOR_POS_SOLOAR_VOL] *10  )));
    DataBase_Update(CALIB_PAGE,CALIB_SOLAR_VOL_LO,LO_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_SOLOAR_VOL] *10 )));

    DataBase_Update(CALIB_PAGE,CALIB_SOLAR_CUR_HI,HI_BYTE((uint16_t)(Adc_Multiplier[FACTOR_POS_SOLOAR_CUR] *10  )));
    DataBase_Update(CALIB_PAGE,CALIB_SOLAR_CUR_LO,LO_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_SOLOAR_CUR] *10 )));

    DataBase_Update(CALIB_PAGE,CALIB_TEMP_COMP_HI,HI_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_TEMP_COMP] *10 )));
    DataBase_Update(CALIB_PAGE,CALIB_TEMP_COMP_LO,LO_BYTE((uint16_t)( Adc_Multiplier[FACTOR_POS_TEMP_COMP] *10 )));


    Write_data_to_Flash();


  
  
}

void read_data(void)
{
  HAL_Delay(2000);
  Read__DataBase();
  delayUs(5000);
   UpdateSystem__FromFlash(INIT_PAGE);
  UpdateSystem__FromFlash(USER_PAGE);
  UpdateSystem__FromFlash(FACTORY_PAGE);
  UpdateSystem__FromFlash(CALIB_PAGE);
  UpdateSystem__FromFlash(SYSCONFIG_PAGE);
  UpdateSystem__FromFlash(DEVELOPER_PAGE);
  
  
}

_Bool init_first =0;
////////////
void Initialization_DataBase(void)
{
	Read__DataBase();
  if(DataBaseMirror[INIT_CHAR_ADRS/PAGE_LENGTH][INIT_CHAR_ADRS%PAGE_LENGTH] != STRT_CHR)
  {
    init_first = 1;
	  	Clear_DataBase();// i.e erase Data base -----> i.e write 0xFF @ all database.
	  	DataBase_Update(INIT_PAGE,INIT_CHAR_ADRS,STRT_CHR);
                
                // System config parameter //
                ParameterSet.cell_number=12;//
                ParameterSet.batt_capacity = 150.0;//
                ParameterSet.batt_type=1;//1-->LM, 2--> VRLA, 3---> Li-Ion, 4----Li-Po4
                 Inverter_Mode.Mode_data = 1;//2  //3  //4  /5 
                //////////
		ParameterSet.PV_undervoltage=34.0;//27.6;
		ParameterSet.pv_overload_watts=750.0;// use in solar over load
		ParameterSet.PV_overvoltage=90.0;  //72.0;

		ParameterSet.Batt_min_current= 3.0;
		ParameterSet.Batt_BF_Cur = 1.8;
		ParameterSet.Batt_FB_Cur= 2.5;
		ParameterSet.Batt_max_current=ParameterSet.batt_capacity/10.0;//20.0;
		ParameterSet.Batt_Boost_voltage=29.0;
		ParameterSet.Batt_Float_voltage=27.5;
		ParameterSet.Batt_undervoltage=21.0;
		ParameterSet.batt_reconnect_vol = 25.0;
		ParameterSet.Batt_overvoltage=35.0;
                ParameterSet.batt_Overreconnect_vol = ParameterSet.Batt_overvoltage-4.0;
		ParameterSet.grid_discon_battvol= 27.0; //ParameterSet.Batt_Boost_voltage-1.5;
		ParameterSet.grid_reconnect_battvol = 23.8;
		ParameterSet.grid_discon_battCur = 13.0;

		Inverter_ParameterSet.gridcharging_stop_Battvoltage = 28.0;
		Inverter_ParameterSet.gridcharging_start_Battvoltage = 23.8;
		Inverter_ParameterSet.gridcharging_stop_voltage = 280;
		Inverter_ParameterSet.grid_on_voltage_level = 160;
		Inverter_ParameterSet.grid_off_voltage_level = 280;
		Inverter_ParameterSet.gridcharging_start_voltage = 160;
		Inverter_ParameterSet.gridcharging_min_cur = 3.0;
		Inverter_ParameterSet.gridcharging_float_voltage = 27.0;
		Inverter_ParameterSet.gridcharging_boost_voltage = 28.5;
		Inverter_ParameterSet.gridcharging_BCL = 10.0;
		Inverter_ParameterSet.PWM_gridChrg = 10;
		Inverter_ParameterSet.PWM_grid_limt = 350;
		Inverter_ParameterSet.PWM_grid_min = 10;
		Inverter_ParameterSet.PWM_GridStep_inc = 5;
		Inverter_ParameterSet.PWM_GridStep_dec = 5;
                ParameterSet.minimum_current_led_on= 1.0;

		PWM.Soft2final_offset = 40;
		PWM.Duty_limt = 815;
		PWM.Duty_min = 300;
		Mains.Frq_low_limit = 47.0;
		Mains.Frq_hi_limit = 53.0;
                Flag_State.SMR_INPUT=1;
                
                ///////// factor////
                Adc_Multiplier[FACTOR_POS_BATT_VOL]= 1000;//(((float)(DataBase[page][CALIB_BATT_VOL_HI] << 8 | DataBase[page][CALIB_BATT_VOL_LO]))/10.0);
                Adc_Multiplier[FACTOR_POS_BATT_ICUR]= 1000;//(((float)(DataBase[page][CALIB_BATT_CHRG_CUR_HI] << 8 | DataBase[page][CALIB_BATT_CHRG_CUR_LO]))/10.0);
                Adc_Multiplier[FACTOR_POS_BATT_OCUR]=1000;// (((float)(DataBase[page][CALIB_BATT_DISCHRG_CUR_HI] << 8 | DataBase[page][CALIB_BATT_DISCHRG_CUR_LO]))/10.0);
                Adc_Multiplier[FACTOR_POS_INVOP_VOL]=1000;//  (((float)(DataBase[page][CALIB_INVERTER_OP_VOL_HI] << 8 | DataBase[page][CALIB_INVERTER_OP_VOL_LO]))/10.0);
                Adc_Multiplier[FACTOR_POS_INVOP_CUR]=1000;// (((float)(DataBase[page][CALIB_INVERTER_OP_CUR_HI] <<8 | DataBase[page][CALIB_INVERTER_OP_CUR_LO]))/10.0);
                Adc_Multiplier[FACTOR_POS_SOLOAR_VOL]=1000;// (((float)(DataBase[page][CALIB_SOLAR_VOL_HI] << 8 | DataBase[page][CALIB_SOLAR_VOL_LO]))/10.0);
                Adc_Multiplier[FACTOR_POS_SOLOAR_CUR]= 1000;//(((float)(DataBase[page][CALIB_SOLAR_CUR_HI] << 8 | DataBase[page][CALIB_SOLAR_CUR_LO]))/10.0);
                Adc_Multiplier[FACTOR_POS_MAIN_VOL]= 1000;//(((float)(DataBase[page][CALIB_MAINS_VOL_HI] << 8 | DataBase[page][CALIB_MAINS_VOL_LO]))/10.0);
                Adc_Multiplier[FACTOR_POS_MAIN_CUR]= 1000;//(((float)(DataBase[page][CALIB_MAINS_CUR_HI] << 8 | DataBase[page][CALIB_MAINS_CUR_LO]))/10.0);
                Adc_Multiplier[FACTOR_POS_TEMP_COMP] = 1000;//
                
                
                // factory setting ///
                ParameterSet.cell_capacity_limit=200;
                ParameterSet.cell_number_limit =12;//
                ParameterSet.PV_undervoltage_Limit=44.0;//27.6;
		ParameterSet.pv_overload_watts_Limit=1000.0;// use in solar over load
		ParameterSet.PV_overvoltage_Limit=90.0;  //72.0;
		ParameterSet.Batt_min_current_Limit= 5.0;
		ParameterSet.Batt_BF_Cur_Limit = 3.0;
		ParameterSet.Batt_FB_Cur_Limit= 3.0;
		ParameterSet.Batt_max_current_Limit=30.0;//20.0;
                Inverter_ParameterSet.op_current_max = 4.5;
               // Inverter_ParameterSet.op_current_110 = Inverter_ParameterSet.op_current_max*1.1 ;
                //Inverter_ParameterSet.op_current_125 = Inverter_ParameterSet.op_current_max*1.25;
                //Inverter_ParameterSet.op_current_150 = Inverter_ParameterSet.op_current_max*1.5 ;
                //Inverter_ParameterSet.op_current_200 = Inverter_ParameterSet.op_current_max*2.0 ;
               // Inverter_ParameterSet.op_current_250 = Inverter_ParameterSet.op_current_max*2.5 ;
                //Inverter_ParameterSet.op_current_300 = Inverter_ParameterSet.op_current_max*3.0 ;
		ParameterSet.Batt_Boost_voltage_Limit=31.0;
		ParameterSet.Batt_Float_voltage_Limit=28.5;
		ParameterSet.Batt_undervoltage_Limit=21.5;
		ParameterSet.batt_reconnect_vol_Limit = 25.0;
		ParameterSet.Batt_overvoltage_Limit=35.0;
                ParameterSet.batt_Overreconnect_vol_Limit = ParameterSet.Batt_overvoltage_Limit-5;//
		ParameterSet.grid_discon_battvol_Limit= 27.0; //ParameterSet.Batt_Boost_voltage-1.5;
		ParameterSet.grid_reconnect_battvol_Limit = 22.8;
		ParameterSet.grid_discon_battCur_Limit = 10.0;
                Inverter_ParameterSet.op_current_max_Limit =4.5;
		Inverter_ParameterSet.gridcharging_stop_Battvoltage_Limit = 30.0;
		Inverter_ParameterSet.gridcharging_start_Battvoltage_Limit = 22.8;
		Inverter_ParameterSet.gridcharging_stop_voltage_Limit = 280;
		Inverter_ParameterSet.grid_on_voltage_level_Limit = 190;
		Inverter_ParameterSet.grid_off_voltage_level_Limit = 280;
		Inverter_ParameterSet.gridcharging_start_voltage_Limit = 190;
		Inverter_ParameterSet.gridcharging_min_cur_Limit = 2.0;
		Inverter_ParameterSet.gridcharging_float_voltage_Limit = 29.0;
		Inverter_ParameterSet.gridcharging_boost_voltage_Limit = 30.0;
		Inverter_ParameterSet.gridcharging_BCL_Limit = 15.0;

	  /*
	   *
	   *
	   * write all detauls value here
	   *
	   *
	   *
	   */
                update_to_flash();
		

  }
  
  read_data();
  
  if(init_first || SET_KEY == 1)
  {
    init_first = 0;
    time_date_function();
  }
  
}
void UpdateSystem__FromFlash(uint8_t page)
{
  if(page == INIT_PAGE)
  {
    Inverter_Mode.Mode_data =  DataBase[page][INV_MODE_PRIORITY];
    if( Inverter_Mode.Mode_data  == 1)
    {
      Inverter_Mode.S_B_G = 1;
      Inverter_Mode.S_G_B = 0;
      Inverter_Mode.G_S_B = 0;
    }
    else if( Inverter_Mode.Mode_data  == 2)
    {
      Inverter_Mode.S_B_G = 0;
      Inverter_Mode.S_G_B = 1;
      Inverter_Mode.G_S_B = 0;
    }
    else if( Inverter_Mode.Mode_data  == 3)
    {
      Inverter_Mode.S_B_G = 0;
      Inverter_Mode.S_G_B = 0;
      Inverter_Mode.G_S_B = 1;
    }
  }
  else if(page == USER_PAGE)
  {
    ParameterSet.batt_type = DataBase[page][CELL_TYPE];
    
    ParameterSet.cell_number =  (DataBase[page][CELL_NUM_HI]<<8 | DataBase[page][CELL_NUM_LO])/10;
    ParameterSet.batt_capacity = (DataBase[page][BATT_CAPCITY_HI]<<8 | DataBase[page][BATT_CAPCITY_LO])/10;
        
    ParameterSet.PV_undervoltage = (((float)(DataBase[page][PV_UNDER_V_HI]<<8 | DataBase[page][PV_UNDER_V_LO]))/10);
    ParameterSet.pv_overload_watts=(((float)(DataBase[page][PV_CUR_LIMT_HI]<<8 | DataBase[page][PV_CUR_LIMT_LO]))/10);
    ParameterSet.PV_overvoltage=(((float)(DataBase[page][PV_OVER_V_HI]<<8 | DataBase[page][PV_OVER_V_LO]))/10);

    ParameterSet.Batt_min_current= (((float)(DataBase[page][BAT_MIN_CUR_HI]<<8 | DataBase[page][BAT_MIN_CUR_LO]))/10);
    ParameterSet.Batt_max_current=(((float)(DataBase[page][BAT_MAX_CUR_HI]<<8 | DataBase[page][BAT_MAX_CUR_LO]))/10);
    ParameterSet.Batt_Boost_voltage=(((float)(DataBase[page][BAT_BOOST_V_HI]<<8 | DataBase[page][BAT_BOOST_V_LO]))/10);
    ParameterSet.Batt_Float_voltage=(((float)(DataBase[page][BAT_FLOAT_V_HI]<<8 | DataBase[page][BAT_FLOAT_V_LO]))/10);
    ParameterSet.Batt_undervoltage=(((float)(DataBase[page][BAT_LOWCUT_HI]<<8 | DataBase[page][BAT_LOWCUT_LO]))/10);
    ParameterSet.batt_reconnect_vol = (((float)(DataBase[page][BAT_LOWCUT_REC_HI]<<8 | DataBase[page][BAT_LOWCUT_REC_LO]))/10);
    ParameterSet.Batt_overvoltage=(((float)(DataBase[page][BAT_HICUT_HI]<<8 | DataBase[page][BAT_HICUT_LO]))/10);
    ParameterSet.batt_Overreconnect_vol = (((float)(DataBase[page][BAT_HICUT_REC_HI]<<8 | DataBase[page][BAT_HICUT_REC_LO]))/10);
    ParameterSet.grid_discon_battvol=(((float)(DataBase[page][BAT_DIS_V_HI]<<8 | DataBase[page][BAT_DIS_V_LO]))/10);
    ParameterSet.grid_reconnect_battvol = (((float)(DataBase[page][BAT_RECON_V_HI]<<8 | DataBase[page][BAT_RECON_V_LO]))/10);
    ParameterSet.grid_discon_battCur = (((float)(DataBase[page][GRID_DIS_B_C_HI]<<8 | DataBase[page][GRID_DIS_B_C_LO]))/10);

    Inverter_ParameterSet.gridcharging_stop_voltage = (((float)(DataBase[page][GRIDCHRG_STOP_V_HI]<<8 | DataBase[page][GRIDCHRG_STOP_V_LO]))/10);
    Inverter_ParameterSet.grid_on_voltage_level = (((float)(DataBase[page][GRID_UNDER_V_HI]<<8 | DataBase[page][GRID_UNDER_V_LO]))/10);
    Inverter_ParameterSet.grid_off_voltage_level = (((float)(DataBase[page][GRID_OVER_V_HI]<<8 | DataBase[page][GRID_OVER_V_LO]))/10);
    Inverter_ParameterSet.gridcharging_start_voltage = (((float)(DataBase[page][GRIDCHRG_START_V_HI]<<8 | DataBase[page][GRIDCHRG_START_V_LO]))/10);
    Inverter_ParameterSet.gridcharging_min_cur = (((float)(DataBase[page][GRIDCHRG_MIN_I_HI]<<8 | DataBase[page][GRIDCHRG_MIN_I_LO]))/10);
    Inverter_ParameterSet.gridcharging_float_voltage = (((float)(DataBase[page][GRIDCHRG_BAT_FLOAT_HI]<<8 | DataBase[page][GRIDCHRG_BAT_FLOAT_LO]))/10);
    Inverter_ParameterSet.gridcharging_boost_voltage = (((float)(DataBase[page][GRIDCHRG_BAT_BOOST_HI]<<8 | DataBase[page][GRIDCHRG_BAT_BOOST_LO]))/10);
    Inverter_ParameterSet.gridcharging_BCL = (((float)(DataBase[page][GRIDCHRG_CUR_LIMIT_HI]<<8 | DataBase[page][GRIDCHRG_CUR_LIMIT_LO]))/10);
    Inverter_ParameterSet.PWM_gridChrg = (((DataBase[page][PWM_GRIDCHRG])));
    Inverter_ParameterSet.PWM_grid_limt = (((DataBase[page][PWM_GRID_LIMIT_HI]<<8 | DataBase[page][PWM_GRID_LIMIT_LO]))/10);
    Inverter_ParameterSet.PWM_grid_min = (((DataBase[page][PWM_GRID_MIN])));
    Inverter_ParameterSet.PWM_GridStep_inc = (((DataBase[page][PWM_GRID_STEP_INC])));
    Inverter_ParameterSet.PWM_GridStep_dec = (((DataBase[page][PWM_GRID_STEP_INC])));
    
    Inverter_ParameterSet.gridcharging_start_Battvoltage = (((float)(DataBase[page][GRIDCHRG_BATT_START_V_HI]<<8 | DataBase[page][GRIDCHRG_BATT_START_V_LO]))/10);
    Inverter_ParameterSet.gridcharging_stop_Battvoltage = (((float)(DataBase[page][GRIDCHRG_BATT_STOP_V_HI]<<8 | DataBase[page][GRIDCHRG_BATT_STOP_V_LO]))/10);
    Inverter_ParameterSet.op_current_max = (((float)(DataBase[page][INV_OP_CUR_HI]<<8 | DataBase[page][INV_OP_CUR_LO]))/10);
    
    //Inverter_ParameterSet.op_current_110 = (((float)(DataBase[page][INV_OP_110_HI]<<8 | DataBase[page][INV_OP_110_LO]))/1000);
    //Inverter_ParameterSet.op_current_125 = (((float)(DataBase[page][INV_OP_125_HI]<<8 | DataBase[page][INV_OP_125_LO]))/1000);
   // Inverter_ParameterSet.op_current_150 = (((float)(DataBase[page][INV_OP_150_HI]<<8 | DataBase[page][INV_OP_150_LO]))/1000);
   // Inverter_ParameterSet.op_current_200 = (((float)(DataBase[page][INV_OP_200_HI]<<8 | DataBase[page][INV_OP_200_LO]))/1000);
   // Inverter_ParameterSet.op_current_250 = (((float)(DataBase[page][INV_OP_250_HI]<<8 | DataBase[page][INV_OP_250_LO]))/1000);
   // Inverter_ParameterSet.op_current_300 = (((float)(DataBase[page][INV_OP_300_HI]<<8 | DataBase[page][INV_OP_300_LO]))/1000);

    PWM.Soft2final_offset = ((DataBase[page][PWM_SOFT_FINAL_OFFSET]));
    PWM.Duty_limt = ((float)(DataBase[page][PWM_DUTY_LIMIT_HI]<<8 | DataBase[page][PWM_DUTY_LIMIT_LO])/10);
    PWM.Duty_min = ((float)(DataBase[page][PWM_DUTY_MIN]));
   // PWM.Duty_min = ((float)(DataBase[page][PWM_DUTY_MIN_HI]<<8 | DataBase[page][PWM_DUTY_MIN_LO])/10);

    PWM.Duty_min=300;
   // PWM.Duty_limt=815;
    Mains.Frq_low_limit = ((float)(DataBase[page][FRQ_MIN]));
    Mains.Frq_hi_limit = ((float)(DataBase[page][FRQ_MAX]));

    
  }
  
  else if(page == FACTORY_PAGE)
  {
     ParameterSet.cell_number_limit =  (DataBase[page][LIMIT_CELL_NUM_HI]<<8 | DataBase[page][LIMIT_CELL_NUM_LO])/10;
    ParameterSet.cell_capacity_limit = (DataBase[page][LIMIT_BATT_CAPCITY_HI]<<8 | DataBase[page][LIMIT_BATT_CAPCITY_LO])/10;
    ParameterSet.PV_undervoltage_Limit = (((float)(DataBase[page][LIMIT_PV_UNDER_V_HI]<<8 | DataBase[page][LIMIT_PV_UNDER_V_LO]))/10);
    ParameterSet.pv_overload_watts_Limit=(((float)(DataBase[page][LIMIT_PV_CUR_LIMT_HI]<<8 | DataBase[page][LIMIT_PV_CUR_LIMT_LO]))/10);
    ParameterSet.PV_overvoltage_Limit=(((float)(DataBase[page][LIMIT_PV_OVER_V_HI]<<8 | DataBase[page][LIMIT_PV_OVER_V_LO]))/10);

    ParameterSet.Batt_min_current_Limit= (((float)(DataBase[page][LIMIT_BAT_MIN_CUR_HI]<<8 | DataBase[page][LIMIT_BAT_MIN_CUR_LO]))/10);
    ParameterSet.Batt_max_current_Limit=(((float)(DataBase[page][LIMIT_BAT_MAX_CUR_HI]<<8 | DataBase[page][LIMIT_BAT_MAX_CUR_LO]))/10);
    ParameterSet.Batt_Boost_voltage_Limit=(((float)(DataBase[page][LIMIT_BAT_BOOST_V_HI]<<8 | DataBase[page][LIMIT_BAT_BOOST_V_LO]))/10);
    ParameterSet.Batt_Float_voltage_Limit=(((float)(DataBase[page][LIMIT_BAT_FLOAT_V_HI]<<8 | DataBase[page][LIMIT_BAT_FLOAT_V_LO]))/10);
    ParameterSet.Batt_undervoltage_Limit=(((float)(DataBase[page][LIMIT_BAT_LOWCUT_HI]<<8 | DataBase[page][LIMIT_BAT_LOWCUT_LO]))/10);
    ParameterSet.Batt_overvoltage_Limit=(((float)(DataBase[page][LIMIT_BAT_HICUT_HI]<<8 | DataBase[page][LIMIT_BAT_HICUT_LO]))/10);
    ParameterSet.grid_discon_battvol_Limit=(((float)(DataBase[page][LIMIT_BAT_DIS_V_HI]<<8 | DataBase[page][LIMIT_BAT_DIS_V_LO]))/10);
    ParameterSet.grid_reconnect_battvol_Limit = (((float)(DataBase[page][LIMIT_BAT_RECON_V_HI]<<8 | DataBase[page][LIMIT_BAT_RECON_V_LO]))/10);
    ParameterSet.grid_discon_battCur_Limit = (((float)(DataBase[page][LIMIT_GRID_DIS_B_C_HI]<<8 | DataBase[page][LIMIT_GRID_DIS_B_C_LO]))/10);
    
    Inverter_ParameterSet.gridcharging_start_Battvoltage_Limit = (((float)(DataBase[page][LIMIT_GRIDCHRG_BATT_START_V_HI]<<8 | DataBase[page][LIMIT_GRIDCHRG_BATT_START_V_LO]))/10);
    Inverter_ParameterSet.gridcharging_stop_Battvoltage_Limit = (((float)(DataBase[page][LIMIT_GRIDCHRG_BATT_STOP_V_HI]<<8 | DataBase[page][LIMIT_GRIDCHRG_BATT_STOP_V_LO]))/10);
    
    Inverter_ParameterSet.op_current_max_Limit = (((float)(DataBase[page][LIMIT_INV_OP_CUR_HI]<<8 | DataBase[page][LIMIT_INV_OP_CUR_LO]))/10);

    Inverter_ParameterSet.gridcharging_stop_voltage_Limit = (((float)(DataBase[page][LIMIT_GRIDCHRG_STOP_V_HI]<<8 | DataBase[page][LIMIT_GRIDCHRG_STOP_V_LO]))/10);
    Inverter_ParameterSet.grid_on_voltage_level_Limit = (((float)(DataBase[page][LIMIT_GRID_UNDER_V_HI]<<8 | DataBase[page][LIMIT_GRID_UNDER_V_LO]))/10);
    Inverter_ParameterSet.grid_off_voltage_level_Limit = (((float)(DataBase[page][LIMIT_GRID_OVER_V_HI]<<8 | DataBase[page][LIMIT_GRID_OVER_V_LO]))/10);
    Inverter_ParameterSet.gridcharging_start_voltage_Limit = (((float)(DataBase[page][LIMIT_GRIDCHRG_START_V_HI]<<8 | DataBase[page][LIMIT_GRIDCHRG_START_V_LO]))/10);
    Inverter_ParameterSet.gridcharging_min_cur_Limit = (((float)(DataBase[page][LIMIT_GRIDCHRG_MIN_I_HI]<<8 | DataBase[page][LIMIT_GRIDCHRG_MIN_I_LO]))/10);
    Inverter_ParameterSet.gridcharging_float_voltage_Limit = (((float)(DataBase[page][LIMIT_GRIDCHRG_BAT_FLOAT_HI]<<8 | DataBase[page][LIMIT_GRIDCHRG_BAT_FLOAT_LO]))/10);
    Inverter_ParameterSet.gridcharging_boost_voltage_Limit = (((float)(DataBase[page][LIMIT_GRIDCHRG_BAT_BOOST_HI]<<8 | DataBase[page][LIMIT_GRIDCHRG_BAT_BOOST_LO]))/10);
    Inverter_ParameterSet.gridcharging_BCL_Limit = (((float)(DataBase[page][LIMIT_GRIDCHRG_CUR_LIMIT_HI]<<8 | DataBase[page][LIMIT_GRIDCHRG_CUR_LIMIT_LO]))/10);
    ParameterSet.batt_reconnect_vol_Limit = (((float)(DataBase[page][LIMIT_BAT_LOWCUT_REC_HI]<<8 | DataBase[page][LIMIT_BAT_LOWCUT_REC_LO]))/10);
    ParameterSet.batt_Overreconnect_vol_Limit = (((float)(DataBase[page][LIMIT_BAT_HICUT_REC_HI]<<8 | DataBase[page][LIMIT_BAT_HICUT_REC_LO]))/10);
  }
  else if(page == CALIB_PAGE)
  {   
    Adc_Multiplier[FACTOR_POS_BATT_VOL]= (((float)(DataBase[page][CALIB_BATT_VOL_HI] << 8 | DataBase[page][CALIB_BATT_VOL_LO]))/10.0);
    Adc_Multiplier[FACTOR_POS_BATT_ICUR]= (((float)(DataBase[page][CALIB_BATT_CHRG_CUR_HI] << 8 | DataBase[page][CALIB_BATT_CHRG_CUR_LO]))/10.0);
    Adc_Multiplier[FACTOR_POS_BATT_OCUR]= (((float)(DataBase[page][CALIB_BATT_DISCHRG_CUR_HI] << 8 | DataBase[page][CALIB_BATT_DISCHRG_CUR_LO]))/10.0);
    Adc_Multiplier[FACTOR_POS_INVOP_VOL]=  (((float)(DataBase[page][CALIB_INVERTER_OP_VOL_HI] << 8 | DataBase[page][CALIB_INVERTER_OP_VOL_LO]))/10.0);
    Adc_Multiplier[FACTOR_POS_INVOP_CUR]= (((float)(DataBase[page][CALIB_INVERTER_OP_CUR_HI] <<8 | DataBase[page][CALIB_INVERTER_OP_CUR_LO]))/10.0);
    Adc_Multiplier[FACTOR_POS_SOLOAR_VOL]= (((float)(DataBase[page][CALIB_SOLAR_VOL_HI] << 8 | DataBase[page][CALIB_SOLAR_VOL_LO]))/10.0);
    Adc_Multiplier[FACTOR_POS_SOLOAR_CUR]= (((float)(DataBase[page][CALIB_SOLAR_CUR_HI] << 8 | DataBase[page][CALIB_SOLAR_CUR_LO]))/10.0);
    Adc_Multiplier[FACTOR_POS_MAIN_VOL]= (((float)(DataBase[page][CALIB_MAINS_VOL_HI] << 8 | DataBase[page][CALIB_MAINS_VOL_LO]))/10.0);
    Adc_Multiplier[FACTOR_POS_MAIN_CUR]= (((float)(DataBase[page][CALIB_MAINS_CUR_HI] << 8 | DataBase[page][CALIB_MAINS_CUR_LO]))/10.0);
    Adc_Multiplier[FACTOR_POS_TEMP_COMP]= (((float)(DataBase[page][CALIB_TEMP_COMP_HI] << 8 | DataBase[page][CALIB_TEMP_COMP_LO]))/10.0);                  
    
  }

}


void DataBase_Update(uint8_t pg,uint8_t pg_adrs, uint8_t Data)
{

	DataBaseMirror[pg][pg_adrs] = Data;

}

void Write_DataFlash(uint8_t *buffer, uint16_t length)
{

  uint16_t l=0;
  uint32_t address_loc=0;
      memcpy(DataBase,DataBaseMirror,PAGE_LENGTH*TOTAL_PAGE);
      memcpy(EE_buffer,DataBase,PAGE_LENGTH*TOTAL_PAGE);
	HAL_FLASH_Unlock();
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );
        FLASH_Erase_Sector(FLASH_SECTOR_7, VOLTAGE_RANGE_3);
           while(l<length)
          {
                HAL_FLASH_Program(TYPEPROGRAM_BYTE,address+address_loc,EE_buffer[l]);
                address_loc=address_loc+0x01;               
                l++;
          }
	HAL_FLASH_Lock();
}


void Clear_DataBase(void)
{
      HAL_FLASH_Unlock();
      __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );

      FLASH_Erase_Sector(FLASH_SECTOR_7, VOLTAGE_RANGE_3);

      HAL_FLASH_Lock();
}

uint32_t Read_Data(uint32_t Address)
{
	uint32_t Data_read;
	Data_read=  *(__IO uint32_t*)Address;
	return Data_read;
}

void Read__DataBase(void)
{
   uint16_t size=0;

   HAL_FLASH_Unlock();
   while(size < (PAGE_LENGTH*TOTAL_PAGE))
   {

           EE_buffer[size] = *(__IO uint32_t*)(0x08060000+size);
           size++;

   }
   HAL_FLASH_Lock();
   memcpy(DataBaseMirror,EE_buffer,PAGE_LENGTH*TOTAL_PAGE);
   memcpy(DataBase,DataBaseMirror,PAGE_LENGTH*TOTAL_PAGE);

}
