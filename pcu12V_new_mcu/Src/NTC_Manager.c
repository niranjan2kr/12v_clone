#include "version_def.h"

// ADC tem signal reading   
// Note: Adc signal decrease with temp increase.
#define FAN_ON_NTC              1000
#define FAN_OFF_REC_NTC         1200

#define SHUT_DN_NTC             230 //  230--------> 90 degree
#define SHUT_DN_REC_NTC         650 //  650--------> 60 degree

void NTC__Control(void)
{
  /// Fan Control ////
  if(Temperature.MPPT_NTC < FAN_ON_NTC || Temperature.INV_NTC < FAN_ON_NTC)
  {
    TIMERt.NTC_Fan_OFF =0;
    if(TIMERt.NTC_Fan_ON++ > 100)
    {
      TIMERt.NTC_Fan_ON =0;
      //FAN_ON;    
    }

  }
  else TIMERt.NTC_Fan_ON =0;
  
  if(Temperature.MPPT_NTC > FAN_OFF_REC_NTC && Temperature.INV_NTC > FAN_OFF_REC_NTC)
  {
    
    if(TIMERt.NTC_Fan_OFF++ > 100)
    {
      TIMERt.NTC_Fan_OFF =0;
      //FAN_OFF;    
    }
    
  }
  // NTC fail
 if(Temperature.MPPT_NTC < SHUT_DN_NTC)// || Temperature.INV_NTC < SHUT_DN_NTC)
 {
   TIMERt.NTC_ShutDownRec =0;
   if(TIMERt.NTC_ShutDown++ > 100)
   {
     TIMERt.NTC_ShutDown =0;
     Flags_fault.NTC_MPPT_Fail = 1;
   }
   
 }
 else TIMERt.NTC_ShutDown =0;
 
 if(Temperature.MPPT_NTC > SHUT_DN_REC_NTC)
  {
    if(TIMERt.NTC_ShutDownRec++ > 100)
    {
      TIMERt.NTC_ShutDownRec = 0;
      Flags_fault.NTC_MPPT_Fail = 0;
      
    }
    
  }
 
 ///
 
  if(Temperature.INV_NTC < SHUT_DN_NTC)
 {
   TIMERt.NTC_ShutDownRecI =0;
   if(TIMERt.NTC_ShutDownI++ > 100)
   {
     TIMERt.NTC_ShutDownI =0;
     Flags_fault.NTC_INV_Fail = 1;
   }
   
 }
 else TIMERt.NTC_ShutDownI =0;
 
 if(Temperature.INV_NTC > SHUT_DN_REC_NTC)// && Temperature.INV_NTC > SHUT_DN_REC_NTC)
  {
    if(TIMERt.NTC_ShutDownRecI++ > 100)
    {
      TIMERt.NTC_ShutDownRecI = 0;
      Flags_fault.NTC_INV_Fail = 0;
      
    }
    
  }
  
}