#include "version_def.h"

void DCLoad__Management (void)
{
  
  if((Load.Voltage * Load.Current ) > 125 || Battery.Voltage < 22.0)
  {
    if(TIMERt.dc_overload_timer++ > 2000)
    {
      TIMERt.dc_overload_timer =0;
      Flags_F.dc_overload_set = SET;
      //LOAD_OFF;      //CHANGED AS PER INSTRUCTION
    }
    
  }
  else if(Flags_F.dc_overload_set)
  {
    if(TIMERt.dc_overload_timer++ > 2000 && (Load.Voltage * Load.Current ) < 120)
    {
      Flags_F.dc_overload_set =0;
      TIMERt.dc_overload_timer =0;
     // LOAD_ON;
      
    }
    
  }
  else TIMERt.dc_overload_timer =0;
  
}
