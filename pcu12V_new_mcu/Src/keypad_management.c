
#include "version_def.h"
#include "lcd.h"
#include "Display.h"

uint16_t time_out;

unsigned int NO_OF_CELLS=0,WRITE_VALUE=0,count_delay=0,indication_resistor=0;
uint8_t key_pressed,key_value,key_count,key_value_one,key_value_two,key_value_three,key_value_four,count,disp_postion;
static _Bool user_pass_flag=0,key_flag=0,open_loop=0,CELL_NO_CHANGE_FLAG=0;
static _Bool pass_flag =0,hold_mode=0,FAULT_FLAG=0,SLEEP_MODE=0,setflag,BATT_TYPE_CHANGE_FLAG=0;
unsigned int time_count_hold_mode=0,menu_count=0;
unsigned char secure_value[10],disp_postion=0;
unsigned char value_position=0,key_count=0;
unsigned char adfst=0,adsnd=0,adthrd=0,adfrth=0,adffth=0,adfsixth=0,log_disp_count=0;
unsigned int disp_value=0,com_value=0;
unsigned int bat_current_limit=0;
unsigned char menu_select=0,Time_menu_select=0,Sub_menu=0,Sub_menu_loc=0;;

char parameter_name_key[15][16];
char unit_name_key[15][16];
char float_buff_key[16];
float Display_Buff_key[16];

uint16_t clb_set_val=0;
float clb_read_val=0;
enum{
	BAT_V_CLBR=0,
	BAT_I_CLBR,
        BAT_DIS_I_CLBR,
	MAINS_VOl_CLBR,
        INV_VOl_CLBR,
        INV_I_CLBR,
        SLR_V_CLBR,
	SLR_I_CLBR,
	TEMP_COMPN_CLBR,

        TOTAL_PARAM,
};
// user setting
enum{
        PV_BOOST_USER,
	PV_CHARGING_USER,
	BATT_NUM_USER,
	BATT_TYP_USER,
	BATT_CAP_USER,
        
	INV_MODE_USER,
	/*charging parameter */
	GRID_FLOAT_VOL_USER,
	GRID_BOOST_VOL_USER,
	GRID_CHARGING_CUR_USER,
	PV_FLOAT_USER,
	
        //INVERTER_OVERLOAD_USER,
	//GRID_CHARGING_SELECTION_USER,
	//GRID_RECONNECT_BATTERY_VOL_USER,
	//GRID_DISCONNECT_BATTERY_VOL_USER,
        //GRID_CHG_START_BATTERY_VOL_USER,
	//GRID_DISCONNECT_BATTERY_CUR_USER,
	//GRID_LOW_CUT_USER,
	//GRID_HI_CUT_USER,
	//BATTERY_CUR_F_B_USER,
	//BATTERY_CUR_B_F_USER,
	//PV_OVERLOAD_WATTS_USER,

	//BATTERY_UNDER_CUT_USER,
	//BATTERY_UNDER_CUT_RECONNECT_USER,
	//BATTERY_OVER_CUT_USER,
	//BATTERY_OVER_CUT_RECONNECT_USER,

        //SYSTEM_DEMO_MODE_USER,

	///
	SIZE_LMT_USER,
};
// factory setting limit
enum{
	BATT_NUM_LMT=1,
	BATT_TYP,
	INV_MODE,
        BATT_CAP_LMT,
	/*charging parameter */
	GRID_FLOAT_VOL_LMT,
	GRID_BOOST_VOL_LMT,
	GRID_CHARGING_CUR_LMT,
	PV_FLOAT_LMT,
	PV_BOOST_LMT,
	PV_CHARGING_LMT,
        INVERTER_OVERLOAD_LMT,
        
	GRID_CHARGING_SELECTION,
	GRID_RECONNECT_BATTERY_VOL_LMT,
	GRID_DISCONNECT_BATTERY_VOL_LMT,
        GRID_CHG_START_BATTERY_VOL_LMT,
	GRID_DISCONNECT_BATTERY_CUR_LMT,
	GRID_LOW_CUT_LMT,
	GRID_HI_CUT_LMT,
	//BATTERY_CUR_F_B_LMT,
	//BATTERY_CUR_B_F_LMT,
	PV_OVERLOAD_WATTS_LMT,

	BATTERY_UNDER_CUT_LIMIT,
	BATTERY_UNDER_CUT_RECONNECT_LIMIT,
	BATTERY_OVER_CUT_LIMIT,
	BATTERY_OVER_CUT_RECONNECT_LIMIT,

        //SYSTEM_DEMO_MODE,

	///
	SIZE_LMT,
};

static char USER_ALLOW_PARAM[SIZE_LMT_USER]=\
  {
                
                BATT_NUM_USER,
                
		BATT_TYP_USER,
               
		INV_MODE,
                
		BATT_CAP_USER,
                GRID_FLOAT_VOL_USER,
		GRID_BOOST_VOL_USER,
		GRID_CHARGING_CUR_USER,
		PV_FLOAT_USER,
		PV_BOOST_USER,
		PV_CHARGING_USER,
		//GRID_CHARGING_SELECTION_USER,
		//GRID_RECONNECT_BATTERY_VOL_USER,
		//GRID_DISCONNECT_BATTERY_VOL_USER,
		//GRID_DISCONNECT_BATTERY_CUR_USER,
		//GRID_LOW_CUT_USER,
		//GRID_HI_CUT_USER,
		//BATTERY_CUR_F_B_USER,
		//BATTERY_CUR_B_F_USER,
		//PV_OVERLOAD_WATTS_USER,

		//BATTERY_UNDER_CUT_USER,
		//BATTERY_UNDER_CUT_RECONNECT_USER,
		//BATTERY_OVER_CUT_USER,
                
		//BATTERY_OVER_CUT_RECONNECT_USER,

                //SYSTEM_DEMO_MODE_USER,
                
                
  };

unsigned int Parameter_set_val[SIZE_LMT_USER];
unsigned int Parameter_factory_val[SIZE_LMT];

void KeyPressed__check(void)
{

if(key_pressed==0)	//if key pressed is 0
key_value=20;		//set key value as 20

if(INC_KEY ==1 && DEC_KEY ==1)// display version name
  {
    Keypad.version_dis =1;
   // Display__VersionName();
  }
else
{
  
  if(SET_KEY==1 && key_pressed==0)//if SET_KEY key   pressed
  {
          key_count++;
          
          if((key_count>0) && (key_count< 5))
          {
                  //time_count=0;
                  key_value_one=1;//forSET KEY
                  key_pressed=1; //set key pressed as 1
                  //buzz=1;	//set buz as 1
                  //back_light_cntrl=1;
          }
          else if(key_count>5)
          {
              key_pressed=1; //set key pressed as 1
                  key_value=1;
          }
          
          time_out=0;
  }
  else if((SET_KEY==0) && (key_value_one==1))
  {
          key_value=1;
          key_value_one=0;
          //value_position++;
          //secure_value[value_position]=key_value;
  }
  
  if(INC_KEY==1 && key_pressed==0)//if key INC_KEY pressed
  {
          key_count++;
    
          
          if((key_count>0) && (key_count<5))
          {
             key_value_two=1;//for key INC_KEY
             key_pressed=1;//set key pressed as 1
            // buzz=1;//set buz as 1
            // back_light_cntrl=1;
             if(Keypad.version_dis==0 && inc_key_dis_pressed==0)
              {   
                
                    //display_time = 0;
                   // display2lcd =1;
                   // inc_key_dis_pressed = 1;
              }
          }
          else if(key_count>5)
          {
                  key_value=2;
                  key_pressed=1;//set key pressed as 1
          }
          time_out=0;
  }
  else if((INC_KEY==0) && (key_value_two==1))
  {
          key_value=2;
          key_value_two=0;
          display++;
          //value_position++;
          //secure_value[value_position]=key_value;
  }
  
  if(DEC_KEY==1 && key_pressed==0)//if key DEC_KEY  pressed
  {
    
          key_count++;
          if((key_count>0) && (key_count<5))
          {
                  key_value_three=1;//for key DEC_KEY
                  key_pressed=1;//set key pressed as 1
                  //buzz=1;//set buz as 1
                  //back_light_cntrl=1;
                  if(dec_key_dis_pressed==0)
                    {

                      //  display2lcd =1;
                       // dec_key_dis_pressed = 1;
                    }
          }
          else if(key_count>5)
          {
                  key_value=3;
                  key_pressed=1;//set key pressed as 1
          }
          
          time_out=0;
  }
  else if((DEC_KEY==0) && (key_value_three==1))
  {
          key_value_three=0;
          key_value=3;
          //value_position++;
          //secure_value[value_position]=key_value;
  }
  
  if(ENT_KEY==1 && key_pressed==0)//if key ENT_KEY pressed
  {
          key_count++;
          if((key_count>0) && (key_count<5))
          {
                  key_value_four=1;//for key ENT_KEY
                  key_pressed=1;//set key pressed as 1
                  //buzz=1;//set buz as 1
                  //back_light_cntrl=1;
                  Flags_F.mute =1;
                  BUZZER_OFF;

          }
          else if(key_count>5)
          {
                  key_value=4;
                  key_pressed=1;//set key pressed as 1
          }
          
          time_out=0;
  }
  else if((ENT_KEY==0) && (key_value_four==1))
  {
          key_value_four=0;
          key_value=4;
          //value_position++;
          //secure_value[value_position]=key_value;
  }

  if(ENT_KEY == 1)
  {
    key_pressed =0;
    Sub_menu =0;
    if(Keypad.version_dis == 1)
    {
      Keypad.version_dis =0;
      return;
    }
    if(Flags_fault.factory_reset_todo ==0)
    {
      Keypad.factory_reset =0;
    }
      
	  if(Keypad.password_scan_mode ==1 || Keypad.clb_password_scan_mode ==1 || Keypad.password_scan_factory_reset==1)
	  {
            

//password   =====  >  IISS;//SIDDS
		  if(1)//secure_value[1]==2 && secure_value[2]==2 && secure_value[3]==1 && secure_value[4]==1)// && secure_value[5]==1 && secure_value[6]==2&& secure_value[7]==3&& secure_value[8]==3&& secure_value[9]==1)
		  	{
		  		pass_flag=1;
		  		if(Keypad.password_scan_mode ==1)
		  		{
                                    Keypad.parameter_setting =1;
                                    Keypad.password_scan_mode =0;
		  		}
		  		if(Keypad.clb_password_scan_mode ==1)
		  		{
		  			Keypad.sub_menu_clb_mode = 1;
		  			Keypad.clb_password_scan_mode =0;
		  		}
                                if(Keypad.password_scan_factory_reset ==1)
                                {
                                  Keypad.password_scan_factory_reset = 0;
                                  Keypad.factory_reset =1;
                                  
                                }
		  		  Keypad.menu_mode =0;
                                  while(ENT_KEY==1);
		  		
		  	}
		  	else
		  		{
		  		Keypad.clb_password_scan_mode =0;
		  		 Keypad.password_scan_mode =0;
                                 Keypad.password_scan_factory_reset =0;
		  		pass_flag=0;
		  		 LCD_Line(0,1);
                                delayUs(100);
                                LCD_Puts("Incorrect       ");
                                delayUs(100);
                                LCD_Line(0,2);
                                LCD_Puts("Password        ");

                                delayUs(65000);
                                delayUs(65000);
                                delayUs(65000);
                                delayUs(65000);
		  		}
	  }

	  else
	  {
           if(Keypad.user_setting == 1 || Keypad.parameter_setting == 1 || Flags_fault.factory_reset_todo==1)
            {
              LCD_INI();
              LCD_Clear();
              LCD_Line(0,1);
              LCD_Puts("Data Saving.....");
              //Write_data_to_Flash();
              // read_data();
              Flags_fault.save_to_flash = 1;
              Flags_fault.save_to_flash_user_fac=1;
              
           }
           else if(Keypad.sub_menu_clb_mode==1)          
           {
              LCD_INI();
              LCD_Clear();
              LCD_Line(0,1);
              LCD_Puts("Data Saving.....");
                
             Adc_Multiplier[FACTOR_POS_BATT_VOL] *= Calib_Scalning_Factor[FACTOR_POS_BATT_VOL];
             Adc_Multiplier[FACTOR_POS_BATT_ICUR] *= Calib_Scalning_Factor[FACTOR_POS_BATT_ICUR];
             Adc_Multiplier[FACTOR_POS_BATT_OCUR] *= Calib_Scalning_Factor[FACTOR_POS_BATT_OCUR];
             Adc_Multiplier[FACTOR_POS_INVOP_VOL] *= Calib_Scalning_Factor[FACTOR_POS_INVOP_VOL];
             Adc_Multiplier[FACTOR_POS_INVOP_CUR] *= Calib_Scalning_Factor[FACTOR_POS_INVOP_CUR];
             Adc_Multiplier[FACTOR_POS_SOLOAR_VOL] *= Calib_Scalning_Factor[FACTOR_POS_SOLOAR_VOL];
             Adc_Multiplier[FACTOR_POS_SOLOAR_CUR] *= Calib_Scalning_Factor[FACTOR_POS_SOLOAR_CUR];
            // Adc_Multiplier[FACTOR_POS_MAIN_VOL] *= Calib_Scalning_Factor[FACTOR_POS_MAIN_VOL];
             Adc_Multiplier[FACTOR_POS_MAIN_CUR] *= Calib_Scalning_Factor[FACTOR_POS_MAIN_CUR];
             Adc_Multiplier[FACTOR_POS_TEMP_COMP] *= Calib_Scalning_Factor[FACTOR_POS_TEMP_COMP];
            // update_to_flash();
            // Write_data_to_Flash();
            
            Flags_fault.save_to_flash = 1;
            Flags_fault.save_to_flash_clb = 1;
             
           }

	  // calling save context
           
		time_out=0;
		LCD_Line(0,1);
		if(Keypad.sub_menu_clb_mode ==1)
		LCD_Puts("Calibration Done");
		else if(Keypad.parameter_setting ==1)
			LCD_Puts("Factory Done !! ");
                else if(Keypad.user_setting ==1)
			LCD_Puts("User Set Done !!");
		while(ENT_KEY==1);
                HAL_Delay(1000);//
		//LCD_Clear();
	  
		Keypad.sub_menu_clb_mode =0;
		Keypad.parameter_setting =0;
		Keypad.user_setting =0;
                }

  }

if((SET_KEY==0) && (ENT_KEY==0) && (INC_KEY==0) && (DEC_KEY==0))
  key_count=0;
}
  
  
}


void KeyScan__FastHandler(void)
{
  	
	if(key_pressed==1)
	{
		key_pressed=0;
		count=0;
	}
	if(key_value==4) 				//if enter key pressed
	{
		hold_mode=0;
		key_value=0;	
		if(FAULT_FLAG==1)				//if fault
		{
			;//accept_fault=1;					//accept fault
			//buzz=0;		
			//Buzz_flag=0;
			//display_fault();				//display the different fault
		
		}
		if(SLEEP_MODE==1)//if it is in sleep mode and key pressed 
		{
			;//sleep_mode_status=0;//then come out of sleep mode
			//wakeup_sleep_mode_count=0;
		}
	}
	else if((key_value==3) && (hold_mode==1))				//if enter key pressed
	{
		key_value=0;						//SET_KEY key value to 0
		hold_mode=0;
		if(SLEEP_MODE==1)//if it is in sleep mode and key pressed 
		{
			;//sleep_mode_status=0;//then come out of sleep mode
			//wakeup_sleep_mode_count=0;
		}
	}
	else if((key_value==2) && (hold_mode==0))					//ifinc key pressed
	{
		key_value=0;
		hold_mode=1;
		if(SLEEP_MODE==1)//if it is in sleep mode and key pressed 
		{
			;//sleep_mode_status=0;//then come out of sleep mode
			//wakeup_sleep_mode_count=0;
		}
	}
              else if((key_value==2) && (hold_mode==1))
              {
                      if(disp_postion<6)
                        disp_postion++;
                      
                      else disp_postion=0;
                      if(SLEEP_MODE==1)//if it is in sleep mode and key pressed 
                      {
                              ;//sleep_mode_status=0;//then come out of sleep mode
                              //wakeup_sleep_mode_count=0;
                      }
              }
              if(Keypad.menu_mode ==1)
                {
                  if(Keypad.password_scan_mode==1)
                  {
                    Password__Scan();
                  }
                  else Menu__Mode();

                }
                if(Keypad.password_scan_factory_reset == 1)
                {
                  Password__Scan();
                }
                else if(Keypad.clb_password_scan_mode ==1)
                {
                  Password__Scan();
                }
                else if(Keypad.set_date_time ==1)
                {
                  Set_date_time();
                }
		else if(Keypad.sub_menu_clb_mode == 1)
		{

			SubMenu__CalbMode();

		}
		else if(Keypad.parameter_setting==1)
		{
			SubMenu__Factory();
		}
		else if(Keypad.user_setting == 1)
		{
			SubMenu__User();

		}
                else if(Keypad.factory_reset==1)
                {
                  Reset_factory();
                  
                }
		else if(key_value==1)//IF SET_KEY KEY PRESSED
	{
       // Menu__Mode();
	    menu_count++;
		key_value=0;
		if(SET_KEY==0)
		  menu_count=0;
		if(menu_count>10)
		{
		   	menu_count=0;
			setflag=1;
			Keypad.menu_mode =1;
			menu_select =0;
			//Menu__Mode();
			setflag=0;
		}
		if(SLEEP_MODE==1)//if it is in sleep mode and key pressed 
		{
			;//sleep_mode_status=0;//then come out of sleep mode
			//wakeup_sleep_mode_count=0;
		}
	}

  
}

void Reset_factory(void)
{
  if(time_out++ <600 && key_value!=4)
	{

	   LCD_Line(0,1);
          
                
		LCD_Puts("FACT RESET START");
		LCD_Line(0,2);

			LCD_Line(0,2);

			if(key_value==2 )//&& clb_set_val< 1)
			{
                           time_out =0;
                          Flags_fault.factory_reset_todo = !Flags_fault.factory_reset_todo;
			}
			else if(key_value==3)// && clb_set_val >= 0)
			{
                           time_out =0;
                          Flags_fault.factory_reset_todo = !Flags_fault.factory_reset_todo;
			}
			
                        if(Flags_fault.factory_reset_todo == 1)
			{
				LCD_Puts("      YES       ");                               
			}
			else 
			{
				LCD_Puts("       NO       ");
                         }
        }
  else
  {
    Keypad.factory_reset =0;
    Flags_fault.factory_reset_todo =0;
    Keypad.password_scan_factory_reset =0;
  }
		
}

void Menu__Mode(void)
{
	if(time_out++ <600 && key_value!=4)
	{

		if(key_pressed==1)
		{
			key_pressed=0;
			time_out=0;
			//back_light_cntrl=1;
			count=0;
		}

		user_pass_flag=1;
		if((key_value==2 ||key_value==3) && menu_select<6)//set key pressed
		{
			key_value=0;
                        Sub_menu_loc =0;
			menu_select++;
			if(menu_select>5)
			menu_select=1;
			time_out=0;
		}
		if(user_pass_flag==0 && menu_select==2)
		 menu_select=3;
	}
	else
	{
          Keypad.menu_mode =0;
          time_out =0;
		key_value=0;
		LCD_Clear();
	}//// cases
	switch(menu_select)
	{
	case 0:
		LCD_INI();
		LCD_Clear();
		time_out=0;
		LCD_Line(0,1);
		LCD_Puts("Please Wait...  ");
		while(SET_KEY==1);
		LCD_Clear();
		key_value=0;
		///
		menu_select++;// go to next case
		break;
	case 1:
		LCD_Line(0,1);
		delayUs(100);
		LCD_Puts("Factory Login   ");
		delayUs(100);
		LCD_Line(0,2);
		LCD_Puts("                ");
                switch(Sub_menu_loc)
                {
                case 0:
                    Sub_menu_loc++;
                    key_value =0;
                    break;
                case 1:
                    {
                      if(key_value==1)//set key pressed
                      {
                          time_out =0;
                          key_value=0;
                          LCD_Clear();
                          time_out=0;
                    	  	secure_value[0]=0;
                    	      secure_value[1]=0;
                    	      secure_value[2]=0;
                    	      secure_value[3]=0;
                    	      secure_value[4]=0;
                    	  	secure_value[5]=0;
                    	      secure_value[6]=0;
                    	  	secure_value[7]=0;
                    	      secure_value[8]=0;
                    	  	secure_value[9]=0;

                    	  	key_value=0;
                    	  	value_position=0;
                    	  	value_position=0;
                    	  	value_position=0;
                    	  	//buzz=0;
                    	  	key_value=0;
                        Sub_menu_loc =0;
                              delayUs(50);
                              key_value=0;
                              key_pressed=0;
                              time_out=0;
                              LCD_Line(0,1);
                              LCD_Puts("Enter Password  ");
                              LCD_Line(0,2);
                              LCD_Puts("                ");
                           // Password__Scan();
                              Keypad.password_scan_mode =1;
                              pass_flag =1;// no need password
                              //password();
                              key_value=0;
                              delayUs(5000);

                      }
                    }
		break;
                }
                break;

		case 2:
		LCD_Line(0,1);
		LCD_Puts("User Login      ");
		delayUs(100);
		LCD_Line(0,2);
		LCD_Puts("                ");
		delayUs(100);
		if(key_value==1)//set key pressed
		{
			key_value=0;
			time_out=0;
			Keypad.user_setting =1;
			Keypad.menu_mode =0;
		}
		break;

                
               

                case 4:
		LCD_Line(0,1);
		LCD_Puts(" Factory Reset  ");
		delayUs(100);
		LCD_Line(0,2);
		LCD_Puts("                ");
		delayUs(100);
		if(key_value==1)//set key pressed
		{
			key_value=0;
			time_out=0;
			
                        
                         Keypad.password_scan_factory_reset =1;
                              pass_flag =1;// no need password
                              LCD_Line(0,1);
                              LCD_Puts("Enter Password  ");
                              LCD_Line(0,2);
                              LCD_Puts("                ");

                              //password();
                        
			Keypad.menu_mode =0;
		}
		break;
                
		case 3:
                LCD_Line(0,1);
		LCD_Puts("Calibration Mode");
		delayUs(100);
		LCD_Line(0,2);
		LCD_Puts("                ");
		delayUs(100);
		if(key_value==1)//set key pressed
		{
                  key_value=0;
                  time_out=0;
                  time_out =0;
                  key_value=0;
                  LCD_Clear();
                  time_out=0;
                  secure_value[0]=0;
                  secure_value[1]=0;
                  secure_value[2]=0;
                  secure_value[3]=0;
                  secure_value[4]=0;
                  secure_value[5]=0;
                  secure_value[6]=0;
                  secure_value[7]=0;
                  secure_value[8]=0;
                  secure_value[9]=0;

                  key_value=0;
                  value_position=0;
                  value_position=0;
                  value_position=0;
                  //buzz=0;
                  key_value=0;
                  Sub_menu_loc =0;
                  delayUs(50);
                  key_value=0;
                  key_pressed=0;
                  time_out=0;
                  LCD_Line(0,1);
                  LCD_Puts("Enter Password  ");
                  LCD_Line(0,2);
                  LCD_Puts("                ");
                  Keypad.clb_password_scan_mode =1;
                  //Keypad.sub_menu_clb_mode =1;
                  Keypad.menu_mode =0;
                  menu_select =0; // cler menu
                  Sub_menu=0;
		}
		break;
        }           
}


uint16_t YEAR=2020,YEAR_dsp=2020,sec=0,mon=0;

void time_date_function(void)
{
    unsigned char menu_select=1;
	unsigned int time_out=0,jj=0,a=0;
        RTC_Get_Time_Date();
        
      min=min;
      hour=hour;
      sec= secnd;
      YEAR_dsp = 2000+year;
      mon = month;
      day=day;
        
        LCD_INI();
        LCD_Clear();
        LCD_Line(0,1);
        
	while(SET_KEY==1)
	;
	if(YEAR<2020||YEAR_dsp<2020)
	{
	 //YEAR=2012;
	 YEAR_dsp=2020;
	}
    else if(YEAR>2022||YEAR_dsp>2022)
    {
	 //YEAR=2012;
	 YEAR_dsp=2020;
	}	

	while(ENT_KEY==0  && time_out<2500)
    {
      HAL_Delay(100);
	     time_out++;
		 if(INC_KEY==1 || DEC_KEY==1)
		 time_out=0;

		if(SET_KEY==1 && menu_select<8)//set key pressed
		{
			while(SET_KEY==1);
			
			menu_select++;
			if(menu_select>7)
			menu_select=1;
			time_out=0;
		}

		
        switch(menu_select)
		{
		
		 case 1:
		 case 2:
		 case 3:
		  LCD_Line(0,1);
		 LCD_Puts("Time-");
		 AdcData__Conv(hour);
                LCD_PutChar(adsnd+48);
                LCD_PutChar(adfst+48); 
		 LCD_Puts(":");  // 12 
		 AdcData__Conv(min);
		 LCD_PutChar(adsnd+48);  // 11
		 LCD_PutChar(adfst+48);  // 12 
		 LCD_Puts(":");  // 12 
		 AdcData__Conv(sec);
		 LCD_PutChar(adsnd+48);  // 11
		 LCD_PutChar(adfst+48);  // 12 
		 LCD_Puts("   ");  // 11
                 
                LCD_Line(0,2);
		 
         if(menu_select==1)
		 {
		     if(INC_KEY==1 && hour<23)
		       hour++;
			 else if(DEC_KEY==1 && hour>0)
		       hour--;  
		     LCD_Puts("Set HOUR        ");
		 }
		 else  if(menu_select==2)
		 {
		     if(INC_KEY==1 && min<59)
		      min++;
			 else if(DEC_KEY==1 && min>0)
		      min--;  
		     LCD_Puts("Set Minute      ");
		 }
		 else  if(menu_select==3)
		 {
		    if(INC_KEY==1 && sec<59)
		      sec++;
			else if(DEC_KEY==1 && sec>0)
		      sec--;  
		     LCD_Puts("Set Second      ");
		 }
		 break;
		 case 4:////type of battery (character)
		 case 5:////type of battery (character)
		 case 6:////type of battery (character)
		 
		  LCD_Line(0,1);
		 LCD_Puts("Date- ");
                  AdcData__Conv(day);
		 LCD_PutChar(adsnd+48);  // 11
		 LCD_PutChar(adfst+48);  // 12 
		  LCD_Puts(":");  // 12 
		 AdcData__Conv(mon);
		LCD_PutChar(adsnd+48);  // 11
		 LCD_PutChar(adfst+48);  // 12 
		  LCD_Puts(":");  // 12
		 AdcData__Conv(YEAR_dsp);
		 LCD_PutChar(adfrth+48);  // 11
		 LCD_PutChar(adthrd+48);  // 11
		 LCD_PutChar(adsnd+48);  // 11
		 LCD_PutChar(adfst+48);  // 12 
		 LCD_Line(0,2);
		 if(menu_select==4)
		 {
		     if(INC_KEY==1 && day<31)
		       day++;
			 else if(DEC_KEY==1 && day>1)
		       day--;  
		     LCD_Puts("Set DAY         ");
		 }
		 else  if(menu_select==5)
		 {
		     if(INC_KEY==1 && mon<12)
		       mon++;
			 else if(DEC_KEY==1 && mon>1)
		       mon--;  
		     LCD_Puts("Set MONTH       ");
		 }
		 else  if(menu_select==6)
		 {
		     if(INC_KEY==1 && YEAR_dsp<4000)
		       YEAR_dsp++;
			 else if(DEC_KEY==1 && YEAR_dsp>2020)
		       YEAR_dsp--;  
		     LCD_Puts("Set YEAR        ");
		 }
         break;
		 
		
	   }
	}
	 // pre_YEAR_dsp=YEAR_dsp;
	 // pre_day=day;
	 // pre_mon=mon;
        
       Set__Date(day,mon,YEAR_dsp);
       Set__Time(hour,min,sec);

    a=0;


}


void Set_date_time(void)
{
	if(time_out++ <600 && key_value!=4)
	{

		user_pass_flag=1;
		if((key_value==2 ||key_value==3) && Time_menu_select<3)//set key pressed
		{
			key_value=0;
                        Sub_menu_loc =0;
			Time_menu_select++;
			if(menu_select>2)
			Time_menu_select=1;
			time_out=0;
		}
	}//// cases
	switch(Time_menu_select)
	{
	case 0:
		LCD_INI();
		LCD_Clear();
		time_out=0;
		LCD_Line(0,1);
		LCD_Puts("Please Wait...  ");
		while(SET_KEY==1);
		LCD_Clear();
		key_value=0;
		///
		Time_menu_select++;// go to next case
		break;

        case 1:
		LCD_Line(0,1);
		LCD_Puts("Date:         ");
		delayUs(100);
		LCD_Line(0,2);
		LCD_Puts("                ");
		delayUs(100);
		if(key_value==1)//set key pressed
		{
			key_value=0;
			time_out=0;
			Keypad.user_setting =1;
			Keypad.menu_mode =0;
		}
		break;

                
         case 2:
                LCD_Line(0,1);
		LCD_Puts("TIME           ");
		delayUs(100);
		LCD_Line(0,2);
		LCD_Puts("                ");
		delayUs(100);
                Time_menu_select++;
		if(key_value==1)//set key pressed
		{
			key_value=0;
			time_out=0;
			Keypad.set_date_time =1;
			Keypad.menu_mode =0;
                        user_pass_flag=0;
                        Time_menu_select=0;
		}
        
		break;
        }
        
  Keypad.set_date_time=0;
                

}


void SubMenu__User(void)
{
	if(time_out++ <800 && key_value!=4)
			{

			Display_Buff_key[0]= Battery.Voltage;
				if(key_pressed==1)
				{
					key_pressed=0;
					time_out=0;
					//back_light_cntrl=1;
					count=0;
				}

				user_pass_flag=1;
				if((key_value==1) && menu_select<SIZE_LMT_USER)//set key pressed
				{
					key_value=0;
					Sub_menu =0;
	                                clb_set_val =0;
	                                clb_read_val=0;
                                        menu_select++;
					//while((!CheckAllow__Security(++menu_select,USER_ALLOW_PARAM)) && menu_select<SIZE_LMT_USER)
					//{

					//};
					if(menu_select>SIZE_LMT_USER-1)
					menu_select=0;
					time_out=0;
                                       while(SET_KEY==1);// wait untill release button
				}
				if(user_pass_flag==0 && menu_select==2)
				 menu_select=3;
			}
			else
			{
				Keypad.user_setting =0;
				Keypad.menu_mode =0;
				time_out =0;
				key_value=0;
				LCD_Clear();
			}//// cases

	switch(menu_select)
        {
	case INV_MODE_USER:
	   LCD_Line(0,1);

		LCD_Puts("    INV_MODE    ");
		LCD_Line(0,2);
		switch(Sub_menu)
		{
		case 0:

			LCD_Line(0,2);

                          clb_set_val = Inverter_Mode.Mode_data;
			if(key_value==2 )//&& clb_set_val< 1)
			{
			  if(clb_set_val >= 3)
				clb_set_val =1;
			else clb_set_val = clb_set_val+1;
			}
			else if(key_value==3)// && clb_set_val >= 0)
			{
			  if(clb_set_val<=1)
				clb_set_val =3;
			else clb_set_val = clb_set_val-1;

			}
			if(clb_set_val == 1)
			{
				LCD_Puts("  S->B->G MODE  ");
                                ParameterSet.mode_sys = 0;// flash
                                
                                    //Inverter_Mode.S_B_G = 1;
                                    //Inverter_Mode.S_G_B = 0;
                                    //Inverter_Mode.G_S_B = 0;
			}
			else if(clb_set_val == 2)
			{
				LCD_Puts("  S->G->B MODE  ");
                               ParameterSet.mode_sys = 1;// flash
                               
                                     // Inverter_Mode.S_B_G = 0;
                                    //Inverter_Mode.S_G_B = 1;
                                    //Inverter_Mode.G_S_B = 0;
			}
			else if(clb_set_val == 3)
			{
				LCD_Puts("  G->S->B MODE  ");
                                
                               // Inverter_Mode.S_B_G = 0;
                                 //   Inverter_Mode.S_G_B = 0;
                                   // Inverter_Mode.G_S_B = 1;
                                ParameterSet.mode_sys = 2;//flash
												//ParameterSet.mode = 1;
			}
			else if(clb_set_val == 4)
			{
				LCD_Puts(" Peak Hour MODE ");
												//ParameterSet.mode = 1;
			}
		   else LCD_Puts("      Err       ");

                        Inverter_Mode.Mode_data =  clb_set_val;
			Sub_menu = 0;
			
                DataBase_Update(INIT_PAGE,INV_MODE_PRIORITY,(uint8_t)clb_set_val);
                break;
		}


          break;

	//
          /*
    case SYSTEM_DEMO_MODE_USER:
       LCD_Line(0,1);

        LCD_Puts("     SYS_MOD    ");
        LCD_Line(0,2);
	switch(Sub_menu)
	{
	case 0:

		LCD_Line(0,2);


		if(key_value==2 )//&& clb_set_val< 1)
		{
		  if(clb_set_val ==1)
			clb_set_val =0;
		else clb_set_val = clb_set_val+1;
		}
		else if(key_value==3)// && clb_set_val >= 0)
		{
		  if(clb_set_val==0)
			clb_set_val =1;
		else clb_set_val = clb_set_val-1;

		}
		if(clb_set_val == 0)
		{
			LCD_Puts("    RUN MODE    ");
                                           // ParameterSet.mode = 0;
		}
		else if(clb_set_val == 1)
		{
			LCD_Puts("   DEBUG MODE   ");
                                            //ParameterSet.mode = 1;
		}
		else LCD_Puts("     Err     ");


		Sub_menu = 0;
		break;

	}


      break;
		*/
		 case BATT_NUM_USER:
			 LCD_Line(0,1);
               LCD_Puts("CELL_LMT:");
             LCD_Line(9,1);
             AdcData__Conv((unsigned int)(ParameterSet.cell_number));
             LCD_PutChar(adthrd+48);
             LCD_PutChar(adsnd+48);
             LCD_PutChar(adfst+48);
             LCD_Line(12,1);
             LCD_Puts("    ");
				switch(Sub_menu)
				{
				case 0:
				
				  clb_set_val = (uint16_t)(ParameterSet.cell_number);

					LCD_Line(0,2);
					LCD_Puts("SET_VAL: ");
					LCD_Line(9,2);
					AdcData__Conv((unsigned int)(ParameterSet.cell_number));
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_PutChar(adfst+48);
					LCD_Line(12,2);
					LCD_Puts("    ");


					Sub_menu = 1;

				break;

				case 1:
					if(key_value==2 && clb_set_val< ParameterSet.cell_number_limit)
					{
						clb_set_val = clb_set_val+1;
					}
					else if(key_value==3 && clb_set_val>6)
					{
						clb_set_val = clb_set_val-1;

					}
					 
                                        DataBase_Update(USER_PAGE,CELL_NUM_HI,HI_BYTE(clb_set_val*10));
                                        DataBase_Update(USER_PAGE,CELL_NUM_LO,LO_BYTE(clb_set_val*10));
                                       
					AdcData__Conv((unsigned int)clb_set_val);
					LCD_Line(9,2);
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_PutChar(adfst+48);
						LCD_Line(12,1);
						LCD_Puts("    ");

				  break;
					}
				break;
          
				case BATT_TYP_USER:

                                  LCD_Line(0,1);
                                   LCD_Puts("BATT_TYP: ");
                                  LCD_Line(10,1);
                                  if(ParameterSet.batt_type == 1)
                                  {
                                          LCD_Puts("LM  ");
                                  }
                                  else if(ParameterSet.batt_type == 2)
                                  {
                                          LCD_Puts("VRLA");
                                  }
                                  else if(ParameterSet.batt_type == 3)
                                  {
                                          LCD_Puts("Li-Ion");
                                  }
                                  else
                                  {
                                          LCD_Puts("       ");
                                  }

                                  LCD_Line(14,1);
                                  LCD_Puts("  ");
                                  switch(Sub_menu)
                                  {
                                  case 0:

                                    clb_set_val = (uint16_t)(ParameterSet.batt_type);

                                          LCD_Line(0,2);
                                          LCD_Puts("SET_TYP: ");
                                          LCD_Line(10,1);
                                          if(clb_set_val == 1)
                                          {
                                                  LCD_Puts("LM  ");
                                          }
                                          else if(clb_set_val == 2)
                                          {
                                                  LCD_Puts("VRLA");
                                          }
                                          else if(clb_set_val == 3)
                                          {
                                                  LCD_Puts("Li-Ion");
                                          }

                                          Sub_menu = 1;

                                  break;

                                  case 1:
                                          if(key_value==2 )//&& clb_set_val< 1)
                                          {
                                          if(clb_set_val >= 3)
                                                clb_set_val =1;
                                          else clb_set_val = clb_set_val+1;
                                          }
                                          else if(key_value==3)// && clb_set_val >= 0)
                                          {
                                               if(clb_set_val<=1)
                                                clb_set_val =3;
                                            else clb_set_val = clb_set_val-1;
                                       
                                          

                                          }
                                          ParameterSet.batt_type = clb_set_val;// no change here.
                                          
                                          LCD_Line(9,2);
                                          if(clb_set_val == 1)
                                          {
                                                  LCD_Puts("LM  ");
                                          }
                                          else if(clb_set_val == 2)
                                          {
                                                  LCD_Puts("VRLA ");
                                          }
                                          else if(clb_set_val == 3)
                                          {
                                                  LCD_Puts("Li-Ion");
                                          }
                                                  LCD_Puts("   ");
                                           DataBase_Update(USER_PAGE,CELL_TYPE,(uint8_t)clb_set_val);
                                    break;
                                          }
                                  break;
	 case BATT_CAP_USER:
		 LCD_Line(0,1);
	  LCD_Puts("BAT_CAP: ");
	LCD_Line(9,1);
	AdcData__Conv((unsigned int)(ParameterSet.batt_capacity));
	LCD_PutChar(adthrd+48);
	LCD_PutChar(adsnd+48);
	LCD_PutChar(adfst+48);
	LCD_Line(12,1);
	LCD_Puts(" A  ");
	switch(Sub_menu)
	{
	case 0:
	//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

                clb_set_val = (uint16_t)(ParameterSet.batt_capacity*10);

		LCD_Line(0,2);
		LCD_Puts("SET_VAL: ");
		LCD_Line(9,2);
		AdcData__Conv((unsigned int)(ParameterSet.batt_capacity));
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_PutChar(adfst+48);
		LCD_Line(12,2);
		LCD_Puts(" A  ");


		Sub_menu = 1;

	break;

	case 1:
		if(key_value==2 && clb_set_val<ParameterSet.cell_capacity_limit*10)//591
		{
			clb_set_val = clb_set_val+10;
		}
		else if(key_value==3 && clb_set_val>=500)
		{
			clb_set_val = clb_set_val-10;

		}
                //ParameterSet.batt_capacity = clb_set_val;// flash
              //  Parameter_set_val[BATT_CAP_USER] = clb_set_val;
 
                DataBase_Update(USER_PAGE,BATT_CAPCITY_HI,HI_BYTE(clb_set_val));
                DataBase_Update(USER_PAGE,BATT_CAPCITY_LO,LO_BYTE(clb_set_val));
                
		AdcData__Conv((unsigned int)(clb_set_val/10));
		LCD_Line(9,2);
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_PutChar(adfst+48);
		LCD_Line(12,2);
		LCD_Puts(" A  ");

	  break;
		}
	break;
                case GRID_FLOAT_VOL_USER:
                LCD_Line(0,1);
                LCD_Puts("GRID_F_V:");
                LCD_Line(9,1);
                AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_float_voltage*10));
                LCD_PutChar(adthrd+48);
                LCD_PutChar(adsnd+48);
                LCD_Puts(".");
                LCD_PutChar(adfst+48);
                LCD_Line(13,1);
                LCD_Puts(" V ");
                switch(Sub_menu)
                {
                case 0:
                //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

                  clb_set_val = (uint16_t)(Inverter_ParameterSet.gridcharging_float_voltage*10);

                        LCD_Line(0,2);
                        LCD_Puts("SET_VAL: ");
                        LCD_Line(9,2);
                        AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_float_voltage*10));
                        LCD_PutChar(adthrd+48);
                        LCD_PutChar(adsnd+48);
                        LCD_Puts(".");
                        LCD_PutChar(adfst+48);
                        LCD_Line(13,2);
                        LCD_Puts(" V ");


                        Sub_menu = 1;

                break;

                case 1:
                        if(key_value==2 && clb_set_val< Inverter_ParameterSet.gridcharging_float_voltage_Limit*10)
                        {
                                clb_set_val = clb_set_val+1;
                        }
                        else if(key_value==3 && clb_set_val>50)
                        {
                                clb_set_val = clb_set_val-1;

                        }
                        
                        DataBase_Update(USER_PAGE,GRIDCHRG_BAT_FLOAT_HI,HI_BYTE(clb_set_val));
                        DataBase_Update(USER_PAGE,GRIDCHRG_BAT_FLOAT_LO,LO_BYTE(clb_set_val));
                        
                        AdcData__Conv((unsigned int)clb_set_val);
                        LCD_Line(9,2);
                        LCD_PutChar(adthrd+48);
                        LCD_PutChar(adsnd+48);
                        LCD_Puts(".");
                        LCD_PutChar(adfst+48);
                        LCD_Line(13,1);
                        LCD_Puts(" V ");

                  break;
                        }
                break;
                case GRID_BOOST_VOL_USER:
                        LCD_Line(0,1);
                        LCD_Puts("GRID_B_V:");
                        LCD_Line(9,1);
                        AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_boost_voltage*10));
                        LCD_PutChar(adthrd+48);
                        LCD_PutChar(adsnd+48);
                        LCD_Puts(".");
                        LCD_PutChar(adfst+48);
                        LCD_Line(13,1);
                        LCD_Puts(" V ");
                        switch(Sub_menu)
                        {
                        case 0:
			//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

			  clb_set_val = (uint16_t)(Inverter_ParameterSet.gridcharging_boost_voltage*10);

				LCD_Line(0,2);
				LCD_Puts("SET_VAL: ");
				LCD_Line(9,2);
				AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_boost_voltage*10));
				LCD_PutChar(adthrd+48);
				LCD_PutChar(adsnd+48);
				LCD_Puts(".");
				LCD_PutChar(adfst+48);
				LCD_Line(13,2);
				LCD_Puts(" V ");


				Sub_menu = 1;

			break;

			case 1:
				if(key_value==2 && clb_set_val< Inverter_ParameterSet.gridcharging_boost_voltage_Limit*10)
				{
					clb_set_val = clb_set_val+1;
				}
				else if(key_value==3 && clb_set_val>240)
				{
					clb_set_val = clb_set_val-1;

				}
				
                        DataBase_Update(USER_PAGE,GRIDCHRG_BAT_BOOST_HI,HI_BYTE(clb_set_val));
                        DataBase_Update(USER_PAGE,GRIDCHRG_BAT_BOOST_LO,LO_BYTE(clb_set_val));
                        
				AdcData__Conv((unsigned int)clb_set_val);
				LCD_Line(9,2);
                                LCD_PutChar(adthrd+48);
                                LCD_PutChar(adsnd+48);
                                LCD_Puts(".");
                                LCD_PutChar(adfst+48);
                                LCD_Line(13,1);
                                LCD_Puts(" V ");

			  break;
		}
	break;
	case GRID_CHARGING_CUR_USER:
LCD_Line(0,1);
LCD_Puts("GRID_C_C:  ");
LCD_Line(9,1);
AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_BCL*10));
LCD_PutChar(adthrd+48);
LCD_PutChar(adsnd+48);
LCD_Puts(".");
LCD_PutChar(adfst+48);
LCD_Line(13,1);
LCD_Puts(" A ");
switch(Sub_menu)
{
case 0:
//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

  clb_set_val = (uint16_t)(Inverter_ParameterSet.gridcharging_BCL*10);

	LCD_Line(0,2);
	LCD_Puts("SET_VAL: ");
	LCD_Line(9,2);
	AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_BCL*10));
	LCD_PutChar(adthrd+48);
	LCD_PutChar(adsnd+48);
	LCD_Puts(".");
	LCD_PutChar(adfst+48);
	LCD_Line(13,2);
	LCD_Puts(" A ");


	Sub_menu = 1;

break;

case 1:
	if(key_value==2 && clb_set_val< Inverter_ParameterSet.gridcharging_BCL_Limit*10)
	{
		clb_set_val = clb_set_val+1;
	}
	else if(key_value==3 && clb_set_val>20)
	{
		clb_set_val = clb_set_val-1;

	}
        
        DataBase_Update(USER_PAGE,GRIDCHRG_CUR_LIMIT_HI,HI_BYTE(clb_set_val));
        DataBase_Update(USER_PAGE,GRIDCHRG_CUR_LIMIT_LO,LO_BYTE(clb_set_val));
        
	AdcData__Conv((unsigned int)clb_set_val);
	LCD_Line(9,2);
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_Puts(".");
		LCD_PutChar(adfst+48);
		LCD_Line(13,1);
		LCD_Puts(" A ");

  break;
}
break;
/*
case INVERTER_OVERLOAD_USER:
LCD_Line(0,1);
LCD_Puts("INV_curr.");
LCD_Line(9,1);
AdcData__Conv((unsigned int)(Inverter_ParameterSet.op_current_max*10));
LCD_PutChar(adthrd+48);
LCD_PutChar(adsnd+48);
LCD_Puts(".");
LCD_PutChar(adfst+48);
LCD_Line(13,1);
LCD_Puts(" A ");
switch(Sub_menu)
{
case 0:
//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

        clb_set_val = (uint16_t)(Inverter_ParameterSet.op_current_max*10);

	LCD_Line(0,2);
	LCD_Puts("SET_VAL: ");
	LCD_Line(9,2);
	AdcData__Conv((unsigned int)(Inverter_ParameterSet.op_current_max*10));
	LCD_PutChar(adthrd+48);
	LCD_PutChar(adsnd+48);
	LCD_Puts(".");
	LCD_PutChar(adfst+48);
	LCD_Line(13,2);
	LCD_Puts(" A ");


	Sub_menu = 1;

break;

case 1:
	if(key_value==2 && clb_set_val<  Inverter_ParameterSet.op_current_max_Limit*10)
	{
		clb_set_val = clb_set_val+1;
	}
	else if(key_value==3 && clb_set_val>10)
	{
		clb_set_val = clb_set_val-1;

	}
        
        DataBase_Update(USER_PAGE,INV_OP_CUR_HI,HI_BYTE(clb_set_val));
        DataBase_Update(USER_PAGE,INV_OP_CUR_LO,LO_BYTE(clb_set_val));
        
      //  DataBase_Update(USER_PAGE,INV_OP_110_HI,HI_BYTE(clb_set_val*110));
      //  DataBase_Update(USER_PAGE,INV_OP_110_LO,LO_BYTE(clb_set_val*110));
        
      //  DataBase_Update(USER_PAGE,INV_OP_125_HI,HI_BYTE(clb_set_val*125));
       // DataBase_Update(USER_PAGE,INV_OP_125_LO,LO_BYTE(clb_set_val*125));
        
       // DataBase_Update(USER_PAGE,INV_OP_150_HI,HI_BYTE(clb_set_val*150));
      //  DataBase_Update(USER_PAGE,INV_OP_150_LO,LO_BYTE(clb_set_val*150));
        
      //  DataBase_Update(USER_PAGE,INV_OP_200_HI,HI_BYTE(clb_set_val*200));
      //  DataBase_Update(USER_PAGE,INV_OP_200_LO,LO_BYTE(clb_set_val*200));
        
      //  DataBase_Update(USER_PAGE,INV_OP_250_HI,HI_BYTE(clb_set_val*250));
       // DataBase_Update(USER_PAGE,INV_OP_250_LO,LO_BYTE(clb_set_val*250));
        
      //  DataBase_Update(USER_PAGE,INV_OP_300_HI,HI_BYTE(clb_set_val*300));
      //  DataBase_Update(USER_PAGE,INV_OP_300_LO,LO_BYTE(clb_set_val*300));
        
	AdcData__Conv((unsigned int)clb_set_val);
	LCD_Line(9,2);
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_Puts(".");
		LCD_PutChar(adfst+48);
		LCD_Line(13,1);
		LCD_Puts(" A ");

  break;
}
break;
*/
case PV_FLOAT_USER:
LCD_Line(0,1);
LCD_Puts("PV_F_V: ");
LCD_Line(9,1);
AdcData__Conv((unsigned int)(ParameterSet.Batt_Float_voltage*10));
LCD_PutChar(adthrd+48);
LCD_PutChar(adsnd+48);
LCD_Puts(".");
LCD_PutChar(adfst+48);
LCD_Line(13,1);
LCD_Puts(" V ");
switch(Sub_menu)
{
case 0:
//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

clb_set_val = (uint16_t)(ParameterSet.Batt_Float_voltage*10);

LCD_Line(0,2);
LCD_Puts("SET_VAL: ");
LCD_Line(9,2);
AdcData__Conv((unsigned int)(ParameterSet.Batt_Float_voltage*10));
LCD_PutChar(adthrd+48);
LCD_PutChar(adsnd+48);
LCD_Puts(".");
LCD_PutChar(adfst+48);
LCD_Line(13,2);
LCD_Puts(" V ");


Sub_menu = 1;

break;

case 1:
if(key_value==2 && clb_set_val< ParameterSet.Batt_Float_voltage_Limit*10)
{
	clb_set_val = clb_set_val+1;
}
else if(key_value==3 && clb_set_val>240)
{
	clb_set_val = clb_set_val-1;

}
        DataBase_Update(USER_PAGE,BAT_FLOAT_V_HI,HI_BYTE(clb_set_val));
        DataBase_Update(USER_PAGE,BAT_FLOAT_V_LO,LO_BYTE(clb_set_val));
        AdcData__Conv((unsigned int)clb_set_val);
        LCD_Line(9,2);
	LCD_PutChar(adthrd+48);
	LCD_PutChar(adsnd+48);
	LCD_Puts(".");
	LCD_PutChar(adfst+48);
	LCD_Line(13,1);
	LCD_Puts(" V ");

break;
}
break;
case PV_BOOST_USER:
LCD_Line(0,1);
LCD_Puts("PV_B_V: ");
LCD_Line(9,1);
AdcData__Conv((unsigned int)(ParameterSet.Batt_Boost_voltage*10));
LCD_PutChar(adthrd+48);
LCD_PutChar(adsnd+48);
LCD_Puts(".");
LCD_PutChar(adfst+48);
LCD_Line(13,1);
LCD_Puts(" V ");
switch(Sub_menu)
{
case 0:
		//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

		  clb_set_val = (uint16_t)(ParameterSet.Batt_Boost_voltage*10);

			LCD_Line(0,2);
			LCD_Puts("SET_VAL: ");
			LCD_Line(9,2);
			AdcData__Conv((unsigned int)(ParameterSet.Batt_Boost_voltage*10));
			LCD_PutChar(adthrd+48);
			LCD_PutChar(adsnd+48);
			LCD_Puts(".");
			LCD_PutChar(adfst+48);
			LCD_Line(13,2);
			LCD_Puts(" V ");


			Sub_menu = 1;

		break;

		case 1:
			if(key_value==2 && clb_set_val< ParameterSet.Batt_Boost_voltage_Limit*10)
			{
				clb_set_val = clb_set_val+1;
			}
			else if(key_value==3 && clb_set_val>240)
			{
				clb_set_val = clb_set_val-1;

			}
                        
                      DataBase_Update(USER_PAGE,BAT_BOOST_V_HI,HI_BYTE(clb_set_val));
                      DataBase_Update(USER_PAGE,BAT_BOOST_V_LO,LO_BYTE(clb_set_val));
        
			AdcData__Conv((unsigned int)clb_set_val);
			LCD_Line(9,2);
			LCD_PutChar(adthrd+48);
			LCD_PutChar(adsnd+48);
			LCD_Puts(".");
			LCD_PutChar(adfst+48);
			LCD_Line(13,1);
			LCD_Puts(" V ");

		  break;
	}
break;
case PV_CHARGING_USER:
LCD_Line(0,1);
LCD_Puts("PV_BCL:  ");
LCD_Line(9,1);
AdcData__Conv((unsigned int)(ParameterSet.Batt_max_current*10));
LCD_PutChar(adthrd+48);
LCD_PutChar(adsnd+48);
LCD_Puts(".");
LCD_PutChar(adfst+48);
LCD_Line(13,1);
LCD_Puts(" A ");
switch(Sub_menu)
{
case 0:
//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

  clb_set_val = (uint16_t)(ParameterSet.Batt_max_current*10);

	LCD_Line(0,2);
	LCD_Puts("SET_VAL: ");
	LCD_Line(9,2);
	AdcData__Conv((unsigned int)(ParameterSet.Batt_max_current*10));
	LCD_PutChar(adthrd+48);
	LCD_PutChar(adsnd+48);
	LCD_Puts(".");
	LCD_PutChar(adfst+48);
	LCD_Line(13,2);
	LCD_Puts(" A ");


	Sub_menu = 1;

break;

case 1:
	if(key_value==2 && clb_set_val< ParameterSet.Batt_max_current_Limit*10)
	{
		clb_set_val = clb_set_val+1;
	}
	else if(key_value==3 && clb_set_val>20)
	{
		clb_set_val = clb_set_val-1;

	}
        DataBase_Update(USER_PAGE,BAT_MAX_CUR_HI,HI_BYTE(clb_set_val));
        DataBase_Update(USER_PAGE,BAT_MAX_CUR_LO,LO_BYTE(clb_set_val));
        
	AdcData__Conv((unsigned int)clb_set_val);
	LCD_Line(9,2);
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_Puts(".");
		LCD_PutChar(adfst+48);
		LCD_Line(13,1);
		LCD_Puts(" A ");

  break;
	}
break;

////
/*
case GRID_RECONNECT_BATTERY_VOL_USER:
	LCD_Line(0,1);
	LCD_Puts("GRID_RBV:");
	LCD_Line(9,1);
	AdcData__Conv((unsigned int)(ParameterSet.grid_reconnect_battvol*10));
	LCD_PutChar(adthrd+48);
	LCD_PutChar(adsnd+48);
	LCD_Puts(".");
	LCD_PutChar(adfst+48);
	LCD_Line(13,1);
	LCD_Puts(" V ");
	switch(Sub_menu)
	{
	case 0:
	//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

	  clb_set_val = (uint16_t)(ParameterSet.grid_reconnect_battvol*10);

		LCD_Line(0,2);
		LCD_Puts("SET_VAL: ");
		LCD_Line(9,2);
		AdcData__Conv((unsigned int)(ParameterSet.grid_reconnect_battvol*10));
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_Puts(".");
		LCD_PutChar(adfst+48);
		LCD_Line(13,2);
		LCD_Puts(" V ");


		Sub_menu = 1;

	break;

	case 1:
		if(key_value==2 && clb_set_val< ParameterSet.grid_reconnect_battvol_Limit*10)
		{
			clb_set_val = clb_set_val+1;
		}
		else if(key_value==3 && clb_set_val>200)
		{
			clb_set_val = clb_set_val-1;

		}
                
              DataBase_Update(USER_PAGE,BAT_RECON_V_HI,HI_BYTE(clb_set_val));
              DataBase_Update(USER_PAGE,BAT_RECON_V_LO,LO_BYTE(clb_set_val));
        
		AdcData__Conv((unsigned int)clb_set_val);
		LCD_Line(9,2);
			LCD_PutChar(adthrd+48);
			LCD_PutChar(adsnd+48);
			LCD_Puts(".");
			LCD_PutChar(adfst+48);
			LCD_Line(13,1);
			LCD_Puts(" V ");

	  break;
		}
	break;
        
        
        case GRID_CHG_START_BATTERY_VOL_USER:
	LCD_Line(0,1);
	LCD_Puts("GRID_CSBV:");
	LCD_Line(9,1);
	AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_start_Battvoltage*10));
	LCD_PutChar(adthrd+48);
	LCD_PutChar(adsnd+48);
	LCD_Puts(".");
	LCD_PutChar(adfst+48);
	LCD_Line(13,1);
	LCD_Puts(" V ");
	switch(Sub_menu)
	{
	case 0:
	//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

	  clb_set_val = (uint16_t)(ParameterSet.grid_reconnect_battvol*10);

		LCD_Line(0,2);
		LCD_Puts("SET_VAL: ");
		LCD_Line(9,2);
		AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_start_Battvoltage*10));
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_Puts(".");
		LCD_PutChar(adfst+48);
		LCD_Line(13,2);
		LCD_Puts(" V ");


		Sub_menu = 1;

	break;

	case 1:
		if(key_value==2 && clb_set_val< Inverter_ParameterSet.gridcharging_start_Battvoltage_Limit*10)
		{
			clb_set_val = clb_set_val+1;
		}
		else if(key_value==3 && clb_set_val>200)
		{
			clb_set_val = clb_set_val-1;

		}
                
              DataBase_Update(USER_PAGE,GRIDCHRG_BATT_START_V_HI,HI_BYTE(clb_set_val));
              DataBase_Update(USER_PAGE,GRIDCHRG_BATT_START_V_LO,LO_BYTE(clb_set_val));
        
		AdcData__Conv((unsigned int)clb_set_val);
		LCD_Line(9,2);
			LCD_PutChar(adthrd+48);
			LCD_PutChar(adsnd+48);
			LCD_Puts(".");
			LCD_PutChar(adfst+48);
			LCD_Line(13,1);
			LCD_Puts(" V ");

	  break;
		}
	break;

	case GRID_DISCONNECT_BATTERY_VOL_USER:
        LCD_Line(0,1);
        LCD_Puts("GRID_DBV:");
        LCD_Line(9,1);
        AdcData__Conv((unsigned int)(ParameterSet.grid_discon_battvol*10));
        LCD_PutChar(adthrd+48);
        LCD_PutChar(adsnd+48);
        LCD_Puts(".");
        LCD_PutChar(adfst+48);
        LCD_Line(13,1);
        LCD_Puts(" V ");
        switch(Sub_menu)
        {
        case 0:
        //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

          clb_set_val = (uint16_t)(ParameterSet.grid_discon_battvol*10);

                LCD_Line(0,2);
                LCD_Puts("SET_VAL: ");
                LCD_Line(9,2);
                AdcData__Conv((unsigned int)(ParameterSet.grid_discon_battvol*10));
                LCD_PutChar(adthrd+48);
                LCD_PutChar(adsnd+48);
                LCD_Puts(".");
                LCD_PutChar(adfst+48);
                LCD_Line(13,2);
                LCD_Puts(" V ");


                Sub_menu = 1;

        break;

        case 1:
                if(key_value==2 && clb_set_val< ParameterSet.grid_discon_battvol_Limit*10)
                {
                        clb_set_val = clb_set_val+1;
                }
                else if(key_value==3 && clb_set_val>200)
                {
                        clb_set_val = clb_set_val-1;

                }
                      DataBase_Update(USER_PAGE,BAT_DIS_V_HI,HI_BYTE(clb_set_val));
                      DataBase_Update(USER_PAGE,BAT_DIS_V_LO,LO_BYTE(clb_set_val));
                      
                AdcData__Conv((unsigned int)clb_set_val);
                LCD_Line(9,2);
                        LCD_PutChar(adthrd+48);
                        LCD_PutChar(adsnd+48);
                        LCD_Puts(".");
                        LCD_PutChar(adfst+48);
                        LCD_Line(13,1);
                        LCD_Puts(" V ");

          break;
                }
        break;

        case GRID_DISCONNECT_BATTERY_CUR_USER:
        LCD_Line(0,1);
        LCD_Puts("GRID_DBC:");
        LCD_Line(9,1);
        AdcData__Conv((unsigned int)(ParameterSet.grid_discon_battCur*10));
        LCD_PutChar(adthrd+48);
        LCD_PutChar(adsnd+48);
        LCD_Puts(".");
        LCD_PutChar(adfst+48);
        LCD_Line(13,1);
        LCD_Puts(" A ");
        switch(Sub_menu)
        {
        case 0:
        //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

        clb_set_val = (uint16_t)(ParameterSet.grid_discon_battCur*10);

        LCD_Line(0,2);
        LCD_Puts("SET_VAL: ");
        LCD_Line(9,2);
        AdcData__Conv((unsigned int)(ParameterSet.grid_discon_battCur*10));
        LCD_PutChar(adthrd+48);
        LCD_PutChar(adsnd+48);
        LCD_Puts(".");
        LCD_PutChar(adfst+48);
        LCD_Line(13,2);
        LCD_Puts(" A ");


        Sub_menu = 1;

        break;

        case 1:
        if(key_value==2 && clb_set_val< ParameterSet.grid_discon_battCur_Limit*10)
        {
                clb_set_val = clb_set_val+1;
        }
        else if(key_value==3 && clb_set_val>50)
        {
                clb_set_val = clb_set_val-1;

        }
        
                      DataBase_Update(USER_PAGE,GRID_DIS_B_C_HI,HI_BYTE(clb_set_val));
                      DataBase_Update(USER_PAGE,GRID_DIS_B_C_LO,LO_BYTE(clb_set_val));

        
     // clb_set_val = ParameterSet.grid_discon_battCur*10;// no change allow
                      
        AdcData__Conv((unsigned int)clb_set_val);
        LCD_Line(9,2);
        LCD_PutChar(adthrd+48);
        LCD_PutChar(adsnd+48);
        LCD_Puts(".");
        LCD_PutChar(adfst+48);
        LCD_Line(13,1);
        LCD_Puts(" A ");

        break;
        }
        break;

        case GRID_LOW_CUT_USER:
        LCD_Line(0,1);
        LCD_Puts("GRID_LCV:");
        LCD_Line(9,1);
        AdcData__Conv((unsigned int)(Inverter_ParameterSet.grid_on_voltage_level));
        LCD_PutChar(adthrd+48);
        LCD_PutChar(adsnd+48);
        LCD_PutChar(adfst+48);
        LCD_Puts(" ");
        LCD_Line(13,1);
        LCD_Puts(" V ");
        switch(Sub_menu)
        {
        case 0:
          clb_set_val = (uint16_t)(Inverter_ParameterSet.grid_on_voltage_level);

                LCD_Line(0,2);
                LCD_Puts("SET_VAL: ");
                LCD_Line(9,2);
                AdcData__Conv((unsigned int)(Inverter_ParameterSet.grid_on_voltage_level));
                LCD_PutChar(adthrd+48);
                LCD_PutChar(adsnd+48);
                LCD_PutChar(adfst+48);
                LCD_Puts(" ");
                LCD_Line(13,2);
                LCD_Puts(" V ");


                Sub_menu = 1;

        break;

        case 1:
	if(key_value==2 && clb_set_val<Inverter_ParameterSet.grid_on_voltage_level_Limit)
	{
		clb_set_val = clb_set_val+1;
	}
	else if(key_value==3 && clb_set_val>160)
	{
		clb_set_val = clb_set_val-1;

	}
        
        
        DataBase_Update(USER_PAGE,GRID_UNDER_V_HI,HI_BYTE(clb_set_val*10));
        DataBase_Update(USER_PAGE,GRID_UNDER_V_LO,LO_BYTE(clb_set_val*10));
                      
	AdcData__Conv((unsigned int)clb_set_val);
	LCD_Line(9,2);
	LCD_PutChar(adthrd+48);
	LCD_PutChar(adsnd+48);
	LCD_PutChar(adfst+48);
	LCD_Puts(" ");
	LCD_Line(13,1);
	LCD_Puts(" V ");

  break;
	}
break;

case GRID_HI_CUT_USER:
	LCD_Line(0,1);
	LCD_Puts("GRID_HCV:");
	LCD_Line(9,1);
	AdcData__Conv((unsigned int)(Inverter_ParameterSet.grid_off_voltage_level));
	LCD_PutChar(adthrd+48);
	LCD_PutChar(adsnd+48);
	LCD_PutChar(adfst+48);
	LCD_Puts(" ");
	LCD_Line(13,1);
	LCD_Puts(" V ");
	switch(Sub_menu)
	{
	case 0:
	//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

	  clb_set_val = (uint16_t)(Inverter_ParameterSet.grid_off_voltage_level);

		LCD_Line(0,2);
		LCD_Puts("SET_VAL: ");
		LCD_Line(9,2);
		AdcData__Conv((unsigned int)(Inverter_ParameterSet.grid_off_voltage_level));
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_PutChar(adfst+48);
		LCD_Puts(" ");
		LCD_Line(13,2);
		LCD_Puts(" V ");


		Sub_menu = 1;

	break;

	case 1:
		if(key_value==2 && clb_set_val< Inverter_ParameterSet.grid_off_voltage_level_Limit)
		{
			clb_set_val = clb_set_val+1;
		}
		else if(key_value==3 && clb_set_val>240)
		{
			clb_set_val = clb_set_val-1;

		}
        DataBase_Update(USER_PAGE,GRID_OVER_V_HI,HI_BYTE(clb_set_val*10));
        DataBase_Update(USER_PAGE,GRID_OVER_V_LO,LO_BYTE(clb_set_val*10));
        
		AdcData__Conv((unsigned int)clb_set_val);
		LCD_Line(9,2);
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_PutChar(adfst+48);
		LCD_Puts(" ");
		LCD_Line(13,1);
		LCD_Puts(" V ");

	  break;
		}
	break;
        
        /*
	case BATTERY_CUR_F_B_USER:
		LCD_Line(0,1);
		LCD_Puts("BATT_FBC:");
		LCD_Line(9,1);
		AdcData__Conv((unsigned int)(ParameterSet.Batt_FB_Cur*10));
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_Puts(".");
		LCD_PutChar(adfst+48);
		LCD_Line(13,1);
		LCD_Puts(" A ");
		switch(Sub_menu)
		{
		case 0:
		//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

		  clb_set_val = (uint16_t)(ParameterSet.Batt_FB_Cur*10);

			LCD_Line(0,2);
			LCD_Puts("SET_VAL: ");
			LCD_Line(9,2);
			AdcData__Conv((unsigned int)(ParameterSet.Batt_FB_Cur*10));
			LCD_PutChar(adthrd+48);
			LCD_PutChar(adsnd+48);
			LCD_Puts(".");
			LCD_PutChar(adfst+48);
			LCD_Line(13,2);
			LCD_Puts(" A ");


			Sub_menu = 1;

		break;

		case 1:
			if(key_value==2 && clb_set_val< 700)
			{
				clb_set_val = clb_set_val+1;
			}
			else if(key_value==3 && clb_set_val>50)
			{
				clb_set_val = clb_set_val-1;

			}
                        
                        clb_set_val = (uint16_t)ParameterSet.Batt_FB_Cur*10;/// no change allowd
        
        
			AdcData__Conv((unsigned int)clb_set_val);
			LCD_Line(9,2);
			LCD_PutChar(adthrd+48);
			LCD_PutChar(adsnd+48);
			LCD_Puts(".");
			LCD_PutChar(adfst+48);
			LCD_Line(13,1);
			LCD_Puts(" A ");

		  break;
			}
		break;
               
		case BATTERY_CUR_B_F_USER:
			LCD_Line(0,1);
			LCD_Puts("BATT_BFC:");
			LCD_Line(9,1);
			AdcData__Conv((unsigned int)(ParameterSet.Batt_BF_Cur*10));
			LCD_PutChar(adthrd+48);
			LCD_PutChar(adsnd+48);
			LCD_Puts(".");
			LCD_PutChar(adfst+48);
			LCD_Line(13,1);
			LCD_Puts(" A ");
			switch(Sub_menu)
			{
			case 0:
			//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

			  clb_set_val = (uint16_t)(ParameterSet.Batt_BF_Cur*10);

				LCD_Line(0,2);
				LCD_Puts("SET_VAL: ");
				LCD_Line(9,2);
				AdcData__Conv((unsigned int)(ParameterSet.Batt_BF_Cur*10));
				LCD_PutChar(adthrd+48);
				LCD_PutChar(adsnd+48);
				LCD_Puts(".");
				LCD_PutChar(adfst+48);
				LCD_Line(13,2);
				LCD_Puts(" A ");


				Sub_menu = 1;

			break;

			case 1:
				if(key_value==2 && clb_set_val< 700)
				{
					clb_set_val = clb_set_val+1;
				}
				else if(key_value==3 && clb_set_val>50)
				{
					clb_set_val = clb_set_val-1;

				}
                                clb_set_val = (uint16_t)(ParameterSet.Batt_BF_Cur*10);// no change allow
                                
				AdcData__Conv((unsigned int)clb_set_val);
				LCD_Line(9,2);
				LCD_PutChar(adthrd+48);
				LCD_PutChar(adsnd+48);
				LCD_Puts(".");
				LCD_PutChar(adfst+48);
				LCD_Line(13,1);
				LCD_Puts(" A ");

			  break;
				}
			break;
                
				case PV_OVERLOAD_WATTS_USER:
				LCD_Line(0,1);
				LCD_Puts("PV_OVR_W:");
				LCD_Line(9,1);
				AdcData__Conv((unsigned int)(ParameterSet.pv_overload_watts));
				LCD_PutChar(adfrth+48);
                                LCD_PutChar(adthrd+48);
				LCD_PutChar(adsnd+48);
				//LCD_Puts(".");
				LCD_PutChar(adfst+48);
				LCD_Line(13,1);
				LCD_Puts(" W ");
				switch(Sub_menu)
				{
				case 0:
				//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

				  clb_set_val = (uint16_t)(ParameterSet.pv_overload_watts);

					LCD_Line(0,2);
					LCD_Puts("SET_VAL: ");
					LCD_Line(9,2);
					AdcData__Conv((unsigned int)(ParameterSet.pv_overload_watts));
					LCD_PutChar(adfrth+48);
                                        LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					//LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,2);
					LCD_Puts(" W ");


					Sub_menu = 1;

				break;

				case 1:
					if(key_value==2 && clb_set_val<ParameterSet.pv_overload_watts_Limit)
					{
						clb_set_val = clb_set_val+1;
					}
					else if(key_value==3 && clb_set_val>100)
					{
						clb_set_val = clb_set_val-1;

					}
					
                                      DataBase_Update(USER_PAGE,PV_CUR_LIMT_HI,HI_BYTE(clb_set_val*10));
                                      DataBase_Update(USER_PAGE,PV_CUR_LIMT_LO,LO_BYTE(clb_set_val*10));
        
					AdcData__Conv((unsigned int)clb_set_val);
					LCD_Line(9,2);
                                        LCD_PutChar(adfrth+48);
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					//LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,1);
					LCD_Puts(" W ");

				  break;
					}
				break;
				case BATTERY_UNDER_CUT_USER:
				LCD_Line(0,1);
				LCD_Puts("BATT_UV: ");
				LCD_Line(9,1);
				AdcData__Conv((unsigned int)(ParameterSet.Batt_undervoltage*10));
				LCD_PutChar(adthrd+48);
				LCD_PutChar(adsnd+48);
				LCD_Puts(".");
				LCD_PutChar(adfst+48);
				LCD_Line(13,1);
				LCD_Puts(" V ");
				switch(Sub_menu)
				{
				case 0:
				//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

				  clb_set_val = (uint16_t)(ParameterSet.Batt_undervoltage*10);

					LCD_Line(0,2);
					LCD_Puts("SET_VAL: ");
					LCD_Line(9,2);
					AdcData__Conv((unsigned int)(ParameterSet.Batt_undervoltage*10));
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,2);
					LCD_Puts(" V ");


					Sub_menu = 1;

				break;

				case 1:
					if(key_value==2 && clb_set_val< ParameterSet.Batt_undervoltage_Limit*10)
					{
						clb_set_val = clb_set_val+1;
					}
					else if(key_value==3 && clb_set_val>200)
					{
						clb_set_val = clb_set_val-1;

					}
                                      DataBase_Update(USER_PAGE,BAT_LOWCUT_HI,HI_BYTE(clb_set_val));
                                      DataBase_Update(USER_PAGE,BAT_LOWCUT_LO,LO_BYTE(clb_set_val));
                                      
					AdcData__Conv((unsigned int)clb_set_val);
					LCD_Line(9,2);
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,1);
					LCD_Puts(" V ");

				  break;
					}
				break;
				case BATTERY_UNDER_CUT_RECONNECT_USER:
					LCD_Line(0,1);
					LCD_Puts("BATT_UVR:");
					LCD_Line(9,1);
					AdcData__Conv((unsigned int)(ParameterSet.batt_reconnect_vol*10));
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,1);
					LCD_Puts(" V ");
					switch(Sub_menu)
					{
					case 0:
					//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

					  clb_set_val = (uint16_t)(ParameterSet.batt_reconnect_vol*10);

						LCD_Line(0,2);
						LCD_Puts("SET_VAL: ");
						LCD_Line(9,2);
						AdcData__Conv((unsigned int)(ParameterSet.batt_reconnect_vol*10));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,2);
						LCD_Puts(" V ");


						Sub_menu = 1;

					break;

					case 1:
						if(key_value==2 && clb_set_val<ParameterSet.batt_reconnect_vol_Limit*10)
						{
							clb_set_val = clb_set_val+1;
						}
						else if(key_value==3 && clb_set_val>220)
						{
							clb_set_val = clb_set_val-1;

						}
                                                
                                                DataBase_Update(USER_PAGE,BAT_LOWCUT_REC_HI,HI_BYTE(clb_set_val));
                                                DataBase_Update(USER_PAGE,BAT_LOWCUT_REC_LO,LO_BYTE(clb_set_val));
                                                
						AdcData__Conv((unsigned int)clb_set_val);
						LCD_Line(9,2);
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,1);
						LCD_Puts(" V ");

					  break;
						}
					break;
					case BATTERY_OVER_CUT_USER:
						LCD_Line(0,1);
						LCD_Puts("BATT_OV: ");
						LCD_Line(9,1);
						AdcData__Conv((unsigned int)(ParameterSet.Batt_overvoltage*10));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,1);
						LCD_Puts(" V ");
						switch(Sub_menu)
						{
						case 0:
						//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

						  clb_set_val = (uint16_t)(ParameterSet.Batt_overvoltage*10);

							LCD_Line(0,2);
							LCD_Puts("SET_VAL: ");
							LCD_Line(9,2);
							AdcData__Conv((unsigned int)(ParameterSet.Batt_overvoltage*10));
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,2);
							LCD_Puts(" V ");


							Sub_menu = 1;

						break;

						case 1:
							if(key_value==2 && clb_set_val< ParameterSet.Batt_overvoltage_Limit*10)
							{
								clb_set_val = clb_set_val+1;
							}
							else if(key_value==3 && clb_set_val>240)
							{
								clb_set_val = clb_set_val-1;

							}
                                                DataBase_Update(USER_PAGE,BAT_HICUT_HI,HI_BYTE(clb_set_val));
                                                DataBase_Update(USER_PAGE,BAT_HICUT_LO,LO_BYTE(clb_set_val));
                                                
							AdcData__Conv((unsigned int)clb_set_val);
							LCD_Line(9,2);
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,1);
							LCD_Puts(" V ");

						  break;
							}
						break;

	case BATTERY_OVER_CUT_RECONNECT_USER:
		LCD_Line(0,1);
		LCD_Puts("BATT_OVR:");
		LCD_Line(9,1);
		AdcData__Conv((unsigned int)(ParameterSet.batt_Overreconnect_vol*10));
		LCD_PutChar(adthrd+48);
		LCD_PutChar(adsnd+48);
		LCD_Puts(".");
		LCD_PutChar(adfst+48);
		LCD_Line(13,1);
		LCD_Puts(" V ");
		switch(Sub_menu)
		{
		case 0:
		//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

		  clb_set_val = (uint16_t)(ParameterSet.batt_Overreconnect_vol*10);

			LCD_Line(0,2);
			LCD_Puts("SET_VAL: ");
			LCD_Line(9,2);
			AdcData__Conv((unsigned int)(ParameterSet.batt_Overreconnect_vol*10));
			LCD_PutChar(adthrd+48);
			LCD_PutChar(adsnd+48);
			LCD_Puts(".");
			LCD_PutChar(adfst+48);
			LCD_Line(13,2);
			LCD_Puts(" V ");


			Sub_menu = 1;

		break;

		case 1:
			if(key_value==2 && clb_set_val< ParameterSet.batt_Overreconnect_vol_Limit*10)
			{
				clb_set_val = clb_set_val+1;
			}
			else if(key_value==3 && clb_set_val>200)
			{
				clb_set_val = clb_set_val-1;

			}
                        
                        DataBase_Update(USER_PAGE,BAT_HICUT_REC_HI,HI_BYTE(clb_set_val));
                        DataBase_Update(USER_PAGE,BAT_HICUT_REC_LO,LO_BYTE(clb_set_val));
                        
			AdcData__Conv((unsigned int)clb_set_val);
			LCD_Line(9,2);
			LCD_PutChar(adthrd+48);
			LCD_PutChar(adsnd+48);
			LCD_Puts(".");
			LCD_PutChar(adfst+48);
			LCD_Line(13,1);
			LCD_Puts(" V ");

		  break;
			}
		break;
*/

		}
        

   


}

void SubMenu__Factory(void)
{
	if(time_out++ <800 && key_value!=4)
			{

			Display_Buff_key[0]= Battery.Voltage;
				if(key_pressed==1)
				{
					key_pressed=0;
					time_out=0;
					//back_light_cntrl=1;
					count=0;
				}

				user_pass_flag=1;
				if((key_value==1) && menu_select<SIZE_LMT)//set key pressed
				{
					key_value=0;
					Sub_menu =0;
	                                clb_set_val =0;
	                                clb_read_val=0;
					menu_select++;
					if(menu_select>SIZE_LMT-1)
					menu_select=0;
					time_out=0;
				}
				if(user_pass_flag==0 && menu_select==2)
				 menu_select=3;
			}
			else
			{
				Keypad.parameter_setting =0;
				Keypad.menu_mode =0;
				time_out =0;
				key_value=0;
				LCD_Clear();
			}//// cases

	switch(menu_select)
                        {
						case INV_MODE:
						   LCD_Line(0,1);

							LCD_Puts("    INV_MODE    ");
							LCD_Line(0,2);
							switch(Sub_menu)
							{
							case 0:

								LCD_Line(0,2);


								if(key_value==2 )//&& clb_set_val< 1)
								{
								  if(clb_set_val ==3)
									clb_set_val =0;
								else clb_set_val = clb_set_val+1;
								}
								else if(key_value==3)// && clb_set_val >= 0)
								{
								  if(clb_set_val==0)
									clb_set_val =1;
								else clb_set_val = clb_set_val-1;

								}
								if(clb_set_val == 0)
								{
									LCD_Puts("  S->B->G MODE  ");
	                                                               // ParameterSet.mode = 0;
								}
								else if(clb_set_val == 1)
								{
									LCD_Puts("  S->G->B MODE  ");
	                                                                //ParameterSet.mode = 1;
								}
								else if(clb_set_val == 2)
								{
									LCD_Puts("  G->S->B MODE  ");
																	//ParameterSet.mode = 1;
								}
								else if(clb_set_val == 3)
								{
									LCD_Puts(" Peak Hour MODE ");
																	//ParameterSet.mode = 1;
								}
							   else LCD_Puts("      Err       ");


								Sub_menu = 0;
								break;

							}


	                          break;

			/*			//
                        case SYSTEM_DEMO_MODE:
                           LCD_Line(0,1);

                            LCD_Puts("     SYS_MOD    ");
                            LCD_Line(0,2);
						switch(Sub_menu)
						{
						case 0:
						
							LCD_Line(0,2);
                                                        
                                                        
							if(key_value==2 )//&& clb_set_val< 1)
							{
							  if(clb_set_val ==1)
								clb_set_val =0;
							else clb_set_val = clb_set_val+1;
							}
							else if(key_value==3)// && clb_set_val >= 0)
							{
							  if(clb_set_val==0)
								clb_set_val =1;
							else clb_set_val = clb_set_val-1;

							}
							if(clb_set_val == 0)
							{
								LCD_Puts("    RUN MODE    ");
                                                               // ParameterSet.mode = 0;
							}
							else if(clb_set_val == 1)
							{
								LCD_Puts("   DEBUG MODE   ");
                                                                //ParameterSet.mode = 1;
							}
							else LCD_Puts("     Err     ");


							Sub_menu = 0;
							break;

						}
                          
                          
                          break;
*/
							 case BATT_NUM_LMT:
								 LCD_Line(0,1);
		 		                  LCD_Puts("BAT_LMT: ");
		 		                LCD_Line(9,1);
		 		                AdcData__Conv((unsigned int)(ParameterSet.cell_number_limit));
		 		                LCD_PutChar(adthrd+48);
		 		                LCD_PutChar(adsnd+48);
		 		                LCD_PutChar(adfst+48);
		 		                LCD_Line(12,1);
		 		                LCD_Puts("    ");
		 						switch(Sub_menu)
		 						{
		 						case 0:
		 						//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

		 						  clb_set_val = (uint16_t)(ParameterSet.cell_number_limit);

		 							LCD_Line(0,2);
		 							LCD_Puts("SET_VAL: ");
		 							LCD_Line(9,2);
		 							AdcData__Conv((unsigned int)(ParameterSet.cell_number_limit));
		 							LCD_PutChar(adthrd+48);
		 							LCD_PutChar(adsnd+48);
		 							LCD_PutChar(adfst+48);
		 							LCD_Line(12,2);
		 							LCD_Puts("    ");


		 							Sub_menu = 1;

		 						break;

		 						case 1:
		 							if(key_value==2 && clb_set_val< 24)
		 							{
		 								clb_set_val = clb_set_val+1;
		 							}
		 							else if(key_value==3 && clb_set_val>6)
		 							{
		 								clb_set_val = clb_set_val-1;

		 							}
                                                                        DataBase_Update(FACTORY_PAGE,LIMIT_CELL_NUM_HI,HI_BYTE(clb_set_val*10));
                                                                        DataBase_Update(FACTORY_PAGE,LIMIT_CELL_NUM_HI,LO_BYTE(clb_set_val*10));
                                                                        
		 							AdcData__Conv((unsigned int)clb_set_val);
		 							LCD_Line(9,2);
		 								LCD_PutChar(adthrd+48);
		 								LCD_PutChar(adsnd+48);
		 								LCD_PutChar(adfst+48);
		 								LCD_Line(12,1);
		 								LCD_Puts("    ");

		 						  break;
		 							}
		 						break;
		 						case BATT_TYP:
							LCD_Line(0,1);
							//LCD_Puts("FLOAT V: ");
							  LCD_Puts("BATT_TYP: ");
							LCD_Line(10,1);
							if(ParameterSet.batt_type == 1)
							{
								LCD_Puts("LM  ");
							}
							else if(ParameterSet.batt_type == 2)
							{
								LCD_Puts("VRLA");
							}
							else if(ParameterSet.batt_type == 3)
							{
								LCD_Puts("Li-Ion");
							}
                                                          else 
							{
								LCD_Puts("      ");
							}
                                                        
							LCD_Line(14,1);
							LCD_Puts("  ");
							switch(Sub_menu)
							{
							case 0:

							  clb_set_val = (uint16_t)(ParameterSet.batt_type);

								LCD_Line(0,2);
								LCD_Puts("SET_TYP: ");
								LCD_Line(10,1);
								if(clb_set_val == 1)
								{
									LCD_Puts("LM  ");
								}
								else if(clb_set_val == 2)
								{
									LCD_Puts("VRLA");
								}
								else if(clb_set_val == 3)
								{
									LCD_Puts("Li-Ion");
								}

								Sub_menu = 1;

							break;

							case 1:
								if(key_value==2 )//&& clb_set_val< 1)
								{
                                                                if(clb_set_val >= 3)
                                                                      clb_set_val =1;
								else clb_set_val = clb_set_val+1;
								}
								else if(key_value==3)// && clb_set_val >= 0)
								{
                                                                     if(clb_set_val<=1)
                                                                      clb_set_val =3;
                                                                  else clb_set_val = clb_set_val-1;
                                                             
								

								}
                                                                ParameterSet.batt_type = clb_set_val;// no change here.
                                                                
								LCD_Line(9,2);
								if(clb_set_val == 1)
								{
									LCD_Puts("LM  ");
								}
								else if(clb_set_val == 2)
								{
									LCD_Puts("VRLA ");
								}
								else if(clb_set_val == 3)
								{
									LCD_Puts("Li-Ion");
								}
									LCD_Puts("   ");
                                                                 DataBase_Update(USER_PAGE,CELL_TYPE,(uint8_t)clb_set_val);
							  break;
								}
							break;

						 case BATT_CAP_LMT:
							 LCD_Line(0,1);
						  LCD_Puts("BAT_CAP: ");
						LCD_Line(9,1);
						AdcData__Conv((unsigned int)(ParameterSet.cell_capacity_limit));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_PutChar(adfst+48);
						LCD_Line(12,1);
						LCD_Puts(" A  ");
						switch(Sub_menu)
						{
						case 0:
						//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

						  clb_set_val = (uint16_t)(ParameterSet.cell_capacity_limit);

							LCD_Line(0,2);
							LCD_Puts("SET_VAL: ");
							LCD_Line(9,2);
							AdcData__Conv((unsigned int)(ParameterSet.cell_capacity_limit));
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_PutChar(adfst+48);
							LCD_Line(12,2);
							LCD_Puts(" A  ");


							Sub_menu = 1;

						break;

						case 1:
							if(key_value==2 && clb_set_val< 291)
							{
								clb_set_val = clb_set_val+10;
							}
							else if(key_value==3 && clb_set_val>=50)
							{
								clb_set_val = clb_set_val-10;

							}
							 DataBase_Update(FACTORY_PAGE,LIMIT_BATT_CAPCITY_HI,HI_BYTE(clb_set_val*10));
                                                         DataBase_Update(FACTORY_PAGE,LIMIT_BATT_CAPCITY_LO,LO_BYTE(clb_set_val*10));
                                                         
							AdcData__Conv((unsigned int)clb_set_val);
							LCD_Line(9,2);
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_PutChar(adfst+48);
							LCD_Line(12,2);
							LCD_Puts(" A  ");

						  break;
							}
						break;
                    case GRID_FLOAT_VOL_LMT:
	                LCD_Line(0,1);
	                LCD_Puts("GRID_F_V:");
	                LCD_Line(9,1);
	                AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_float_voltage_Limit*10));
	                LCD_PutChar(adthrd+48);
	                LCD_PutChar(adsnd+48);
	                LCD_Puts(".");
	                LCD_PutChar(adfst+48);
	                LCD_Line(13,1);
	                LCD_Puts(" V ");
					switch(Sub_menu)
					{
					case 0:
					//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

					  clb_set_val = (uint16_t)(ParameterSet.Batt_Float_voltage_Limit*10);

						LCD_Line(0,2);
						LCD_Puts("SET_VAL: ");
						LCD_Line(9,2);
						AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_float_voltage_Limit*10));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,2);
						LCD_Puts(" V ");


						Sub_menu = 1;

					break;

					case 1:
						if(key_value==2 && clb_set_val< 300)
						{
							clb_set_val = clb_set_val+1;
						}
						else if(key_value==3 && clb_set_val>240)
						{
							clb_set_val = clb_set_val-1;

						}
						 DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BAT_FLOAT_HI,HI_BYTE(clb_set_val));
                                                 DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BAT_FLOAT_LO,LO_BYTE(clb_set_val));
                                                         
						AdcData__Conv((unsigned int)clb_set_val);
						LCD_Line(9,2);
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,1);
							LCD_Puts(" V ");

					  break;
						}
					break;
					case GRID_BOOST_VOL_LMT:
						LCD_Line(0,1);
						LCD_Puts("GRID_B_V:");
						LCD_Line(9,1);
						AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_boost_voltage_Limit*10));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,1);
						LCD_Puts(" V ");
						switch(Sub_menu)
						{
						case 0:
								//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

								  clb_set_val = (uint16_t)(Inverter_ParameterSet.gridcharging_boost_voltage_Limit*10);

									LCD_Line(0,2);
									LCD_Puts("SET_VAL: ");
									LCD_Line(9,2);
									AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_boost_voltage_Limit*10));
									LCD_PutChar(adthrd+48);
									LCD_PutChar(adsnd+48);
									LCD_Puts(".");
									LCD_PutChar(adfst+48);
									LCD_Line(13,2);
									LCD_Puts(" V ");


									Sub_menu = 1;

								break;

								case 1:
									if(key_value==2 && clb_set_val< 300)
									{
										clb_set_val = clb_set_val+1;
									}
									else if(key_value==3 && clb_set_val>240)
									{
										clb_set_val = clb_set_val-1;

									}
                                                                        DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BAT_BOOST_HI,HI_BYTE(clb_set_val));
                                                                        DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BAT_BOOST_LO,LO_BYTE(clb_set_val));
									
									AdcData__Conv((unsigned int)clb_set_val);
									LCD_Line(9,2);
										LCD_PutChar(adthrd+48);
										LCD_PutChar(adsnd+48);
										LCD_Puts(".");
										LCD_PutChar(adfst+48);
										LCD_Line(13,1);
										LCD_Puts(" V ");

								  break;
							}
						break;
						case GRID_CHARGING_CUR_LMT:
				LCD_Line(0,1);
				LCD_Puts("GRID_C_C:  ");
				LCD_Line(9,1);
				AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_BCL_Limit*10));
				LCD_PutChar(adthrd+48);
				LCD_PutChar(adsnd+48);
				LCD_Puts(".");
				LCD_PutChar(adfst+48);
				LCD_Line(13,1);
				LCD_Puts(" A ");
				switch(Sub_menu)
				{
				case 0:
					//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

					  clb_set_val = (uint16_t)(Inverter_ParameterSet.gridcharging_BCL_Limit*10);

						LCD_Line(0,2);
						LCD_Puts("SET_VAL: ");
						LCD_Line(9,2);
						AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_BCL_Limit*10));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,2);
						LCD_Puts(" A ");


						Sub_menu = 1;

					break;

					case 1:
						if(key_value==2 && clb_set_val< 200)
						{
							clb_set_val = clb_set_val+1;
						}
						else if(key_value==3 && clb_set_val>50)
						{
							clb_set_val = clb_set_val-1;

						}
						DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_CUR_LIMIT_HI,HI_BYTE(clb_set_val));
                                               DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_CUR_LIMIT_LO,LO_BYTE(clb_set_val));
                                               
						AdcData__Conv((unsigned int)clb_set_val);
						LCD_Line(9,2);
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,1);
							LCD_Puts(" A ");

					  break;
				}
				break;

				case PV_FLOAT_LMT:
				LCD_Line(0,1);
				LCD_Puts("PV_F_V: ");
				LCD_Line(9,1);
				AdcData__Conv((unsigned int)(ParameterSet.Batt_Float_voltage_Limit*10));
				LCD_PutChar(adthrd+48);
				LCD_PutChar(adsnd+48);
				LCD_Puts(".");
				LCD_PutChar(adfst+48);
				LCD_Line(13,1);
				LCD_Puts(" V ");
				switch(Sub_menu)
				{
				case 0:
				//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

				  clb_set_val = (uint16_t)(ParameterSet.Batt_Float_voltage_Limit*10);

					LCD_Line(0,2);
					LCD_Puts("SET_VAL: ");
					LCD_Line(9,2);
					AdcData__Conv((unsigned int)(ParameterSet.Batt_Float_voltage_Limit*10));
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,2);
					LCD_Puts(" V ");


					Sub_menu = 1;

				break;

				case 1:
					if(key_value==2 && clb_set_val< 300)
					{
						clb_set_val = clb_set_val+1;
					}
					else if(key_value==3 && clb_set_val>240)
					{
						clb_set_val = clb_set_val-1;

					}
					DataBase_Update(FACTORY_PAGE,LIMIT_BAT_FLOAT_V_HI,HI_BYTE(clb_set_val));
                                        DataBase_Update(FACTORY_PAGE,LIMIT_BAT_FLOAT_V_LO,LO_BYTE(clb_set_val));
					AdcData__Conv((unsigned int)clb_set_val);
					LCD_Line(9,2);
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,1);
						LCD_Puts(" V ");

				  break;
					}
				break;
				case PV_BOOST_LMT:
					LCD_Line(0,1);
					LCD_Puts("PV_B_V: ");
					LCD_Line(9,1);
					AdcData__Conv((unsigned int)(ParameterSet.Batt_Boost_voltage_Limit*10));
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,1);
					LCD_Puts(" V ");
					switch(Sub_menu)
					{
					case 0:
							//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

							  clb_set_val = (uint16_t)(ParameterSet.Batt_Boost_voltage_Limit*10);

								LCD_Line(0,2);
								LCD_Puts("SET_VAL: ");
								LCD_Line(9,2);
								AdcData__Conv((unsigned int)(ParameterSet.Batt_Boost_voltage_Limit*10));
								LCD_PutChar(adthrd+48);
								LCD_PutChar(adsnd+48);
								LCD_Puts(".");
								LCD_PutChar(adfst+48);
								LCD_Line(13,2);
								LCD_Puts(" V ");


								Sub_menu = 1;

							break;

							case 1:
								if(key_value==2 && clb_set_val< 300)
								{
									clb_set_val = clb_set_val+1;
								}
								else if(key_value==3 &&clb_set_val>240 )// clb_set_val>ParameterSet.Batt_Boost_voltage_Limit*10)
								{
									clb_set_val = clb_set_val-1;

								}
                                                                DataBase_Update(FACTORY_PAGE,LIMIT_BAT_BOOST_V_HI,HI_BYTE(clb_set_val));
                                                                DataBase_Update(FACTORY_PAGE,LIMIT_BAT_BOOST_V_LO,LO_BYTE(clb_set_val));
                                                                
								AdcData__Conv((unsigned int)clb_set_val);
								LCD_Line(9,2);
								LCD_PutChar(adthrd+48);
								LCD_PutChar(adsnd+48);
								LCD_Puts(".");
								LCD_PutChar(adfst+48);
								LCD_Line(13,1);
								LCD_Puts(" V ");

							  break;
						}
					break;
					case PV_CHARGING_LMT:
					LCD_Line(0,1);
					LCD_Puts("PV_CHG:  ");
					LCD_Line(9,1);
					AdcData__Conv((unsigned int)(ParameterSet.Batt_max_current_Limit*10));
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,1);
					LCD_Puts(" A ");
					switch(Sub_menu)
					{
					case 0:
					//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

					  clb_set_val = (uint16_t)(ParameterSet.Batt_max_current_Limit*10);

						LCD_Line(0,2);
						LCD_Puts("SET_VAL: ");
						LCD_Line(9,2);
						AdcData__Conv((unsigned int)(ParameterSet.Batt_max_current_Limit*10));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,2);
						LCD_Puts(" A ");


						Sub_menu = 1;

					break;

					case 1:
						if(key_value==2 && clb_set_val< 300)
						{
							clb_set_val = clb_set_val+1;
						}
						else if(key_value==3 && clb_set_val>50)
						{
							clb_set_val = clb_set_val-1;

						}
                                                DataBase_Update(FACTORY_PAGE,LIMIT_BAT_MAX_CUR_HI,HI_BYTE(clb_set_val));
                                                DataBase_Update(FACTORY_PAGE,LIMIT_BAT_MAX_CUR_LO,LO_BYTE(clb_set_val));
                                                
						AdcData__Conv((unsigned int)clb_set_val);
						LCD_Line(9,2);
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,1);
							LCD_Puts(" A ");

					  break;
						}
					break;
                                        
                                        case INVERTER_OVERLOAD_LMT:
					LCD_Line(0,1);
					LCD_Puts("INV_Curr.");
					LCD_Line(9,1);
					AdcData__Conv((unsigned int)(Inverter_ParameterSet.op_current_max_Limit*10));
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
                                        LCD_PutChar(adfst+48);
					LCD_Line(13,1);
					LCD_Puts(" A ");
					switch(Sub_menu)
					{
					case 0:
					//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

					  clb_set_val = (uint16_t)(Inverter_ParameterSet.op_current_max_Limit*10);

						LCD_Line(0,2);
						LCD_Puts("SET_VAL: ");
						LCD_Line(9,2);
						AdcData__Conv((unsigned int)(Inverter_ParameterSet.op_current_max_Limit*10));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,2);
						LCD_Puts(" A ");


						Sub_menu = 1;

					break;

					case 1:
						if(key_value==2 && clb_set_val< 45)
						{
							clb_set_val = clb_set_val+1;
						}
						else if(key_value==3 && clb_set_val>20)
						{
							clb_set_val = clb_set_val-1;

						}
                                                DataBase_Update(FACTORY_PAGE,LIMIT_INV_OP_CUR_HI,HI_BYTE(clb_set_val));
                                                DataBase_Update(FACTORY_PAGE,LIMIT_INV_OP_CUR_LO,LO_BYTE(clb_set_val));
                                                
                                                DataBase_Update(USER_PAGE,INV_OP_CUR_HI,HI_BYTE(clb_set_val));
                                                DataBase_Update(USER_PAGE,INV_OP_CUR_LO,LO_BYTE(clb_set_val));
                                                
						AdcData__Conv((unsigned int)clb_set_val);
						LCD_Line(9,2);
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,1);
							LCD_Puts(" A ");

					  break;
						}
					break;

					////

					case GRID_RECONNECT_BATTERY_VOL_LMT:
						LCD_Line(0,1);
						LCD_Puts("GRID_RBV:");
						LCD_Line(9,1);
						AdcData__Conv((unsigned int)(ParameterSet.grid_reconnect_battvol_Limit*10));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,1);
						LCD_Puts(" A ");
						switch(Sub_menu)
						{
						case 0:
						//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

						  clb_set_val = (uint16_t)(ParameterSet.grid_reconnect_battvol_Limit*10);

							LCD_Line(0,2);
							LCD_Puts("SET_VAL: ");
							LCD_Line(9,2);
							AdcData__Conv((unsigned int)(ParameterSet.grid_reconnect_battvol_Limit*10));
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,2);
							LCD_Puts(" A ");


							Sub_menu = 1;

						break;

						case 1:
							if(key_value==2 && clb_set_val< 300)
							{
								clb_set_val = clb_set_val+1;
							}
							else if(key_value==3 && clb_set_val>200)
							{
								clb_set_val = clb_set_val-1;

							}
                                                        DataBase_Update(FACTORY_PAGE,LIMIT_BAT_RECON_V_HI,HI_BYTE(clb_set_val));
                                                        DataBase_Update(FACTORY_PAGE,LIMIT_BAT_RECON_V_LO,LO_BYTE(clb_set_val));
                                                        
                                                        DataBase_Update(USER_PAGE,BAT_RECON_V_HI,HI_BYTE(clb_set_val));
                                                        DataBase_Update(USER_PAGE,BAT_RECON_V_LO,LO_BYTE(clb_set_val));
                                                        
							AdcData__Conv((unsigned int)clb_set_val);
							LCD_Line(9,2);
                                                        LCD_PutChar(adthrd+48);
                                                        LCD_PutChar(adsnd+48);
                                                        LCD_Puts(".");
                                                        LCD_PutChar(adfst+48);
                                                        LCD_Line(13,1);
                                                        LCD_Puts(" A ");

						  break;
							}
						break;

						case GRID_DISCONNECT_BATTERY_VOL_LMT:
					LCD_Line(0,1);
					LCD_Puts("GRID_DBV:");
					LCD_Line(9,1);
					AdcData__Conv((unsigned int)(ParameterSet.grid_discon_battvol_Limit*10));
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,1);
					LCD_Puts(" A ");
					switch(Sub_menu)
					{
					case 0:
					//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

					  clb_set_val = (uint16_t)(ParameterSet.grid_discon_battvol_Limit*10);

						LCD_Line(0,2);
						LCD_Puts("SET_VAL: ");
						LCD_Line(9,2);
						AdcData__Conv((unsigned int)(ParameterSet.grid_discon_battvol_Limit*10));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,2);
						LCD_Puts(" A ");


						Sub_menu = 1;

					break;

					case 1:
						if(key_value==2 && clb_set_val< 300)
						{
							clb_set_val = clb_set_val+1;
						}
						else if(key_value==3 && clb_set_val>200)
						{
							clb_set_val = clb_set_val-1;

						}
                                                
                                              DataBase_Update(FACTORY_PAGE,LIMIT_BAT_DIS_V_HI,HI_BYTE(clb_set_val));
                                              DataBase_Update(FACTORY_PAGE,LIMIT_BAT_DIS_V_LO,LO_BYTE(clb_set_val));
                                              
                                              DataBase_Update(USER_PAGE,BAT_DIS_V_HI,HI_BYTE(clb_set_val));
                                              DataBase_Update(USER_PAGE,BAT_DIS_V_LO,LO_BYTE(clb_set_val));
                                                        
						AdcData__Conv((unsigned int)clb_set_val);
						LCD_Line(9,2);
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,1);
							LCD_Puts(" A ");

					  break;
						}
					break;
                                        
                                        
                                        case GRID_CHG_START_BATTERY_VOL_LMT:
					LCD_Line(0,1);
					LCD_Puts("GRID_CSBV:");
					LCD_Line(9,1);
					AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_start_Battvoltage_Limit*10));
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,1);
					LCD_Puts(" A ");
					switch(Sub_menu)
					{
					case 0:
					//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

					  clb_set_val = (uint16_t)(Inverter_ParameterSet.gridcharging_start_Battvoltage_Limit*10);

						LCD_Line(0,2);
						LCD_Puts("SET_VAL: ");
						LCD_Line(9,2);
						AdcData__Conv((unsigned int)(Inverter_ParameterSet.gridcharging_start_Battvoltage_Limit*10));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_Puts(".");
						LCD_PutChar(adfst+48);
						LCD_Line(13,2);
						LCD_Puts(" A ");


						Sub_menu = 1;

					break;

					case 1:
						if(key_value==2 && clb_set_val< 300)
						{
							clb_set_val = clb_set_val+1;
						}
						else if(key_value==3 && clb_set_val>200)
						{
							clb_set_val = clb_set_val-1;

						}
                                                
                                              DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BATT_START_V_HI,HI_BYTE(clb_set_val));
                                              DataBase_Update(FACTORY_PAGE,LIMIT_GRIDCHRG_BATT_START_V_LO,LO_BYTE(clb_set_val));
                                              
                                              DataBase_Update(USER_PAGE,GRIDCHRG_BATT_START_V_HI,HI_BYTE(clb_set_val));
                                              DataBase_Update(USER_PAGE,GRIDCHRG_BATT_START_V_LO,LO_BYTE(clb_set_val));
                                                        
						AdcData__Conv((unsigned int)clb_set_val);
						LCD_Line(9,2);
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,1);
							LCD_Puts(" A ");

					  break;
						}
					break;

					case GRID_DISCONNECT_BATTERY_CUR_LMT:
				LCD_Line(0,1);
				LCD_Puts("GRID_DBC:");
				LCD_Line(9,1);
				AdcData__Conv((unsigned int)(ParameterSet.grid_discon_battCur_Limit*10));
				LCD_PutChar(adthrd+48);
				LCD_PutChar(adsnd+48);
				LCD_Puts(".");
				LCD_PutChar(adfst+48);
				LCD_Line(13,1);
				LCD_Puts(" A ");
				switch(Sub_menu)
				{
				case 0:
				//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

				  clb_set_val = (uint16_t)(ParameterSet.grid_discon_battCur_Limit*10);

					LCD_Line(0,2);
					LCD_Puts("SET_VAL: ");
					LCD_Line(9,2);
					AdcData__Conv((unsigned int)(ParameterSet.grid_discon_battCur_Limit*10));
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,2);
					LCD_Puts(" A ");


					Sub_menu = 1;

				break;

				case 1:
					if(key_value==2 && clb_set_val< 300)
					{
						clb_set_val = clb_set_val+1;
					}
					else if(key_value==3 && clb_set_val>50)
					{
						clb_set_val = clb_set_val-1;

					}
                                        DataBase_Update(FACTORY_PAGE,LIMIT_GRID_DIS_B_C_HI,HI_BYTE(clb_set_val));
                                        DataBase_Update(FACTORY_PAGE,LIMIT_GRID_DIS_B_C_LO,LO_BYTE(clb_set_val));
                                        
                                        DataBase_Update(USER_PAGE,GRID_DIS_B_C_HI,HI_BYTE(clb_set_val));
                                        DataBase_Update(USER_PAGE,GRID_DIS_B_C_LO,LO_BYTE(clb_set_val));
                                        
                                        
					//clb_set_val = ParameterSet.grid_discon_battCur_Limit *10;// no change here
                                        
					AdcData__Conv((unsigned int)clb_set_val);
					LCD_Line(9,2);
					LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_Puts(".");
					LCD_PutChar(adfst+48);
					LCD_Line(13,1);
					LCD_Puts(" A ");

				  break;
					}
				break;

				case GRID_LOW_CUT_LMT:
					LCD_Line(0,1);
					LCD_Puts("GRID_LCV:");
					LCD_Line(9,1);
					AdcData__Conv((unsigned int)(Inverter_ParameterSet.grid_on_voltage_level_Limit));
					 LCD_Puts(" ");
                                        LCD_PutChar(adthrd+48);
					LCD_PutChar(adsnd+48);
					LCD_PutChar(adfst+48);
                                        LCD_Line(13,1);
					LCD_Puts(" V ");
					switch(Sub_menu)
					{
					case 0:
					//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

					  clb_set_val = (uint16_t)(Inverter_ParameterSet.grid_on_voltage_level_Limit);

						LCD_Line(0,2);
						LCD_Puts("SET_VAL: ");
						LCD_Line(9,2);
						AdcData__Conv((unsigned int)(Inverter_ParameterSet.grid_on_voltage_level_Limit));
						
                                                LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_PutChar(adfst+48);
						LCD_Line(13,2);
						LCD_Puts(" V ");


						Sub_menu = 1;

					break;

					case 1:
						if(key_value==2 && clb_set_val< 200)
						{
							clb_set_val = clb_set_val+1;
						}
						else if(key_value==3 && clb_set_val>160)
						{
							clb_set_val = clb_set_val-1;

						}
                                                
                                                DataBase_Update(FACTORY_PAGE,LIMIT_GRID_UNDER_V_HI,HI_BYTE(clb_set_val*10));
                                                DataBase_Update(FACTORY_PAGE,LIMIT_GRID_UNDER_V_LO,LO_BYTE(clb_set_val*10));
                                                
                                                DataBase_Update(USER_PAGE,GRID_UNDER_V_HI,HI_BYTE(clb_set_val*10));
                                                DataBase_Update(USER_PAGE,GRID_UNDER_V_LO,LO_BYTE(clb_set_val*10));
                                                
						AdcData__Conv((unsigned int)clb_set_val);
                                                
						LCD_Line(9,2);
                                                LCD_Puts(" ");
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_PutChar(adfst+48);
						LCD_Line(13,1);
						LCD_Puts(" V ");

					  break;
						}
					break;

					case GRID_HI_CUT_LMT:
						LCD_Line(0,1);
						LCD_Puts("GRID_HCV:");
						LCD_Line(9,1);
						AdcData__Conv((unsigned int)(Inverter_ParameterSet.grid_off_voltage_level_Limit));
						LCD_PutChar(adthrd+48);
						LCD_PutChar(adsnd+48);
						LCD_PutChar(adfst+48);
                                                LCD_Puts(" ");
						LCD_Line(13,1);
						LCD_Puts(" V ");
						switch(Sub_menu)
						{
						case 0:
						//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

						  clb_set_val = (uint16_t)(Inverter_ParameterSet.grid_off_voltage_level_Limit);

							LCD_Line(0,2);
							LCD_Puts("SET_VAL: ");
							LCD_Line(9,2);
							AdcData__Conv((unsigned int)(Inverter_ParameterSet.grid_off_voltage_level_Limit));
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_PutChar(adfst+48);
                                                        LCD_Puts(" ");
							LCD_Line(13,2);
							LCD_Puts(" V ");


							Sub_menu = 1;

						break;

						case 1:
							if(key_value==2 && clb_set_val< 290)
							{
								clb_set_val = clb_set_val+1;
							}
							else if(key_value==3 && clb_set_val>240 )//Inverter_ParameterSet.grid_on_voltage_level_Limit)
							{
								clb_set_val = clb_set_val-1;

							}
                                                        
                                                        DataBase_Update(FACTORY_PAGE,LIMIT_GRID_OVER_V_HI,HI_BYTE(clb_set_val*10));
                                                        DataBase_Update(FACTORY_PAGE,LIMIT_GRID_OVER_V_LO,LO_BYTE(clb_set_val*10));
                                                        
                                                        DataBase_Update(USER_PAGE,GRID_OVER_V_HI,HI_BYTE(clb_set_val*10));
                                                        DataBase_Update(USER_PAGE,GRID_OVER_V_LO,LO_BYTE(clb_set_val*10));
                                                        
							AdcData__Conv((unsigned int)clb_set_val);
							LCD_Line(9,2);
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_PutChar(adfst+48);
							LCD_Line(13,1);
							LCD_Puts(" V ");

						  break;
							}
						break;
                                                
                                                /*
						case BATTERY_CUR_F_B_LMT:
							LCD_Line(0,1);
							LCD_Puts("BATT_FBC:");
							LCD_Line(9,1);
							AdcData__Conv((unsigned int)(ParameterSet.Batt_FB_Cur_Limit*10));
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,1);
							LCD_Puts(" A ");
							switch(Sub_menu)
							{
							case 0:
							//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

							  clb_set_val = (uint16_t)(ParameterSet.Batt_FB_Cur_Limit*10);

								LCD_Line(0,2);
								LCD_Puts("SET_VAL: ");
								LCD_Line(9,2);
								AdcData__Conv((unsigned int)(ParameterSet.Batt_FB_Cur_Limit*10));
								LCD_PutChar(adthrd+48);
								LCD_PutChar(adsnd+48);
								LCD_Puts(".");
								LCD_PutChar(adfst+48);
								LCD_Line(13,2);
								LCD_Puts(" A ");


								Sub_menu = 1;

							break;

							case 1:
								if(key_value==2 && clb_set_val< 50)
								{
									clb_set_val = clb_set_val+1;
								}
								else if(key_value==3 && clb_set_val>10)
								{
									clb_set_val = clb_set_val-1;

								}
								clb_set_val = ParameterSet.Batt_FB_Cur_Limit*10;// no change
								AdcData__Conv((unsigned int)clb_set_val);
								LCD_Line(9,2);
								LCD_PutChar(adthrd+48);
								LCD_PutChar(adsnd+48);
								LCD_Puts(".");
								LCD_PutChar(adfst+48);
								LCD_Line(13,1);
								LCD_Puts(" A ");

							  break;
								}
							break;
                                                       
							case BATTERY_CUR_B_F_LMT:
								LCD_Line(0,1);
								LCD_Puts("BATT_BFC:");
								LCD_Line(9,1);
								AdcData__Conv((unsigned int)(ParameterSet.Batt_BF_Cur_Limit*10));
								LCD_PutChar(adthrd+48);
								LCD_PutChar(adsnd+48);
								LCD_Puts(".");
								LCD_PutChar(adfst+48);
								LCD_Line(13,1);
								LCD_Puts(" A ");
								switch(Sub_menu)
								{
								case 0:
								//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

								  clb_set_val = (uint16_t)(ParameterSet.Batt_BF_Cur_Limit*10);

									LCD_Line(0,2);
									LCD_Puts("SET_VAL: ");
									LCD_Line(9,2);
									AdcData__Conv((unsigned int)(ParameterSet.Batt_BF_Cur_Limit*10));
									LCD_PutChar(adthrd+48);
									LCD_PutChar(adsnd+48);
									LCD_Puts(".");
									LCD_PutChar(adfst+48);
									LCD_Line(13,2);
									LCD_Puts(" A ");


									Sub_menu = 1;

								break;

								case 1:
									if(key_value==2 && clb_set_val< 50)
									{
										clb_set_val = clb_set_val+1;
									}
									else if(key_value==3 && clb_set_val>10)
									{
										clb_set_val = clb_set_val-1;

									}
									clb_set_val = ParameterSet.Batt_BF_Cur_Limit*10;// no change
									AdcData__Conv((unsigned int)clb_set_val);
									LCD_Line(9,2);
									LCD_PutChar(adthrd+48);
									LCD_PutChar(adsnd+48);
									LCD_Puts(".");
									LCD_PutChar(adfst+48);
									LCD_Line(13,1);
									LCD_Puts(" A ");

								  break;
									}
								break;
                                                        */
									case PV_OVERLOAD_WATTS_LMT:
									LCD_Line(0,1);
									LCD_Puts("PV_OVR_W:");
									LCD_Line(9,1);
									AdcData__Conv((unsigned int)(ParameterSet.pv_overload_watts_Limit));
									//LCD_PutChar(adfsixth+48);
                                                                       // LCD_PutChar(adffth+48);
                                                                        LCD_PutChar(adfrth+48);
                                                                        LCD_PutChar(adthrd+48);
									LCD_PutChar(adsnd+48);
									LCD_PutChar(adfst+48);
									//LCD_Line(13,1);
									LCD_Puts(" W ");
									switch(Sub_menu)
									{
									case 0:
									//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

									  clb_set_val = (uint16_t)(ParameterSet.pv_overload_watts_Limit);

										LCD_Line(0,2);
										LCD_Puts("SET_VAL: ");
										LCD_Line(9,2);
										AdcData__Conv((unsigned int)(ParameterSet.pv_overload_watts_Limit));
                                                                                LCD_PutChar(adfrth+48);
										LCD_PutChar(adthrd+48);
										LCD_PutChar(adsnd+48);
										LCD_PutChar(adfst+48);
										LCD_Line(13,2);
										LCD_Puts(" W ");


										Sub_menu = 1;

									break;

									case 1:
										if(key_value==2 && clb_set_val< 1000)
										{
											clb_set_val = clb_set_val+1;
										}
										else if(key_value==3 && clb_set_val>100)
										{
											clb_set_val = clb_set_val-1;

										}
                                                                                
                                                                              DataBase_Update(FACTORY_PAGE,LIMIT_PV_CUR_LIMT_HI,HI_BYTE(clb_set_val*10));
                                                                              DataBase_Update(FACTORY_PAGE,LIMIT_PV_CUR_LIMT_LO,LO_BYTE(clb_set_val*10));   
                                                                              
                                                                              DataBase_Update(USER_PAGE,PV_CUR_LIMT_HI,HI_BYTE(clb_set_val*10));
                                                                              DataBase_Update(USER_PAGE,PV_CUR_LIMT_LO,LO_BYTE(clb_set_val*10));
                                                                             
										AdcData__Conv((unsigned int)clb_set_val);
										LCD_Line(9,2);
                                                                                LCD_PutChar(adfrth+48);
										LCD_PutChar(adthrd+48);
										LCD_PutChar(adsnd+48);
										LCD_PutChar(adfst+48);
										LCD_Line(13,1);
										LCD_Puts(" W ");

									  break;
										}
									break;
									case BATTERY_UNDER_CUT_LIMIT:
									LCD_Line(0,1);
									LCD_Puts("BATT_UV: ");
									LCD_Line(9,1);
									AdcData__Conv((unsigned int)(ParameterSet.Batt_undervoltage_Limit*10));
									LCD_PutChar(adthrd+48);
									LCD_PutChar(adsnd+48);
									LCD_Puts(".");
									LCD_PutChar(adfst+48);
									LCD_Line(13,1);
									LCD_Puts(" V ");
									switch(Sub_menu)
									{
									case 0:
									//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

									  clb_set_val = (uint16_t)(ParameterSet.Batt_undervoltage_Limit*10);

										LCD_Line(0,2);
										LCD_Puts("SET_VAL: ");
										LCD_Line(9,2);
										AdcData__Conv((unsigned int)(ParameterSet.Batt_undervoltage_Limit*10));
										LCD_PutChar(adthrd+48);
										LCD_PutChar(adsnd+48);
										LCD_Puts(".");
										LCD_PutChar(adfst+48);
										LCD_Line(13,2);
										LCD_Puts(" V ");


										Sub_menu = 1;

									break;

									case 1:
										if(key_value==2 && clb_set_val< 240)
										{
											clb_set_val = clb_set_val+1;
										}
										else if(key_value==3 && clb_set_val>200)
										{
											clb_set_val = clb_set_val-1;

										}
                                                                                
                                                                                DataBase_Update(FACTORY_PAGE,LIMIT_BAT_LOWCUT_HI,HI_BYTE(clb_set_val));
                                                                                DataBase_Update(FACTORY_PAGE,LIMIT_BAT_LOWCUT_LO,LO_BYTE(clb_set_val));
                                                                                
                                                                                DataBase_Update(USER_PAGE,BAT_LOWCUT_HI,HI_BYTE(clb_set_val));
                                                                                DataBase_Update(USER_PAGE,BAT_LOWCUT_LO,LO_BYTE(clb_set_val));
                                                                                
										AdcData__Conv((unsigned int)clb_set_val);
										LCD_Line(9,2);
										LCD_PutChar(adthrd+48);
										LCD_PutChar(adsnd+48);
										LCD_Puts(".");
										LCD_PutChar(adfst+48);
										LCD_Line(13,1);
										LCD_Puts(" V ");

									  break;
										}
									break;
									case BATTERY_UNDER_CUT_RECONNECT_LIMIT:
										LCD_Line(0,1);
										LCD_Puts("BATT_UVR:");
										LCD_Line(9,1);
										AdcData__Conv((unsigned int)(ParameterSet.batt_reconnect_vol_Limit*10));
										LCD_PutChar(adthrd+48);
										LCD_PutChar(adsnd+48);
										LCD_Puts(".");
										LCD_PutChar(adfst+48);
										LCD_Line(13,1);
										LCD_Puts(" V ");
										switch(Sub_menu)
										{
										case 0:
										//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

										  clb_set_val = (uint16_t)(ParameterSet.batt_reconnect_vol_Limit*10);

											LCD_Line(0,2);
											LCD_Puts("SET_VAL: ");
											LCD_Line(9,2);
											AdcData__Conv((unsigned int)(ParameterSet.batt_reconnect_vol_Limit*10));
											LCD_PutChar(adthrd+48);
											LCD_PutChar(adsnd+48);
											LCD_Puts(".");
											LCD_PutChar(adfst+48);
											LCD_Line(13,2);
											LCD_Puts(" V ");


											Sub_menu = 1;

										break;

										case 1:
											if(key_value==2 && clb_set_val< 270)
											{
												clb_set_val = clb_set_val+1;
											}
											else if(key_value==3 && clb_set_val>220)
											{
												clb_set_val = clb_set_val-1;

											}
                                                                                        DataBase_Update(FACTORY_PAGE,LIMIT_BAT_LOWCUT_REC_HI,HI_BYTE(clb_set_val));
                                                                                        DataBase_Update(FACTORY_PAGE,LIMIT_BAT_LOWCUT_REC_LO,LO_BYTE(clb_set_val));
                                                                                        
                                                                                        DataBase_Update(USER_PAGE,BAT_LOWCUT_REC_HI,HI_BYTE(clb_set_val));
                                                                                        DataBase_Update(USER_PAGE,BAT_LOWCUT_REC_LO,LO_BYTE(clb_set_val));
                                                                                        
											AdcData__Conv((unsigned int)clb_set_val);
											LCD_Line(9,2);
											LCD_PutChar(adthrd+48);
											LCD_PutChar(adsnd+48);
											LCD_Puts(".");
											LCD_PutChar(adfst+48);
											LCD_Line(13,1);
											LCD_Puts(" V ");

										  break;
											}
										break;
										case BATTERY_OVER_CUT_LIMIT:
											LCD_Line(0,1);
											LCD_Puts("BATT_OV: ");
											LCD_Line(9,1);
											AdcData__Conv((unsigned int)(ParameterSet.Batt_overvoltage_Limit*10));
											LCD_PutChar(adthrd+48);
											LCD_PutChar(adsnd+48);
											LCD_Puts(".");
											LCD_PutChar(adfst+48);
											LCD_Line(13,1);
											LCD_Puts(" V ");
											switch(Sub_menu)
											{
											case 0:
											//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

											  clb_set_val = (uint16_t)(ParameterSet.Batt_overvoltage_Limit*10);

												LCD_Line(0,2);
												LCD_Puts("SET_VAL: ");
												LCD_Line(9,2);
												AdcData__Conv((unsigned int)(ParameterSet.Batt_overvoltage_Limit*10));
												LCD_PutChar(adthrd+48);
												LCD_PutChar(adsnd+48);
												LCD_Puts(".");
												LCD_PutChar(adfst+48);
												LCD_Line(13,2);
												LCD_Puts(" V ");


												Sub_menu = 1;

											break;

											case 1:
												if(key_value==2 && clb_set_val< 350)
												{
													clb_set_val = clb_set_val+1;
												}
												else if(key_value==3 && clb_set_val>280)
												{
													clb_set_val = clb_set_val-1;

												}
                                                                                        DataBase_Update(FACTORY_PAGE,LIMIT_BAT_HICUT_HI,HI_BYTE(clb_set_val));
                                                                                        DataBase_Update(FACTORY_PAGE,LIMIT_BAT_HICUT_LO,LO_BYTE(clb_set_val));
                                                                                        
                                                                                        DataBase_Update(USER_PAGE,BAT_HICUT_HI,HI_BYTE(clb_set_val));
                                                                                        DataBase_Update(USER_PAGE,BAT_HICUT_LO,LO_BYTE(clb_set_val));
                                                                                        
												AdcData__Conv((unsigned int)clb_set_val);
												LCD_Line(9,2);
												LCD_PutChar(adthrd+48);
												LCD_PutChar(adsnd+48);
												LCD_Puts(".");
												LCD_PutChar(adfst+48);
												LCD_Line(13,1);
												LCD_Puts(" V ");

											  break;
												}
											break;

						case BATTERY_OVER_CUT_RECONNECT_LIMIT:
							LCD_Line(0,1);
							LCD_Puts("BATT_OVR:");
							LCD_Line(9,1);
							AdcData__Conv((unsigned int)(ParameterSet.batt_Overreconnect_vol_Limit*10));
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,1);
							LCD_Puts(" V ");
							switch(Sub_menu)
							{
							case 0:
							//  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

							  clb_set_val = (uint16_t)(ParameterSet.batt_Overreconnect_vol_Limit*10);

								LCD_Line(0,2);
								LCD_Puts("SET_VAL : ");
								LCD_Line(9,2);
								AdcData__Conv((unsigned int)(ParameterSet.batt_Overreconnect_vol_Limit*10));
								LCD_PutChar(adthrd+48);
								LCD_PutChar(adsnd+48);
								LCD_Puts(".");
								LCD_PutChar(adfst+48);
								LCD_Line(13,2);
								LCD_Puts(" V ");


								Sub_menu = 1;

							break;

							case 1:
								if(key_value==2 && clb_set_val< 300)
								{
									clb_set_val = clb_set_val+1;
								}
								else if(key_value==3 && clb_set_val>250)
								{
									clb_set_val = clb_set_val-1;

								}
                                                              DataBase_Update(FACTORY_PAGE,LIMIT_BAT_HICUT_REC_HI,HI_BYTE(clb_set_val));
                                                              DataBase_Update(FACTORY_PAGE,LIMIT_BAT_HICUT_REC_LO,LO_BYTE(clb_set_val));
                                                              
                                                              DataBase_Update(USER_PAGE,BAT_HICUT_REC_HI,HI_BYTE(clb_set_val));
                                                              DataBase_Update(USER_PAGE,BAT_HICUT_REC_LO,LO_BYTE(clb_set_val));
                                                              
								AdcData__Conv((unsigned int)clb_set_val);
								LCD_Line(9,2);
								LCD_PutChar(adthrd+48);
								LCD_PutChar(adsnd+48);
								LCD_Puts(".");
								LCD_PutChar(adfst+48);
								LCD_Line(13,1);
								LCD_Puts(" V ");

							  break;
								}
							break;
				}


}








void SubMenu__CalbMode(void)
{
	float calc_v;

	if(time_out++ <800 && key_value!=4)
		{

		Display_Buff_key[0]= Battery.Voltage;
			if(key_pressed==1)
			{
				key_pressed=0;
				time_out=0;
				//back_light_cntrl=1;
				count=0;
			}

			user_pass_flag=1;
			if((key_value==1) && menu_select<TOTAL_PARAM)//set key pressed
			{
				key_value=0;
				Sub_menu =0;
				clb_set_val =0;
				clb_read_val=0;
				menu_select++;
				if(menu_select>TOTAL_PARAM-1)
				menu_select=0;
				time_out=0;
                                 while(SET_KEY==1);
			}
			if(user_pass_flag==0 && menu_select==2)
			 menu_select=3;
		}
		else
		{
			Keypad.menu_mode =0;
			time_out =0;
			key_value=0;
			LCD_Clear();
		}//// cases
	switch(menu_select)
			{
			case BAT_V_CLBR:
                LCD_Line(0,1);
                LCD_Puts("BAT_VOL: ");
                LCD_Line(9,1);
                AdcData__Conv((unsigned int)(Battery.Voltage*10));
                LCD_PutChar(adthrd+48);
                LCD_PutChar(adsnd+48);
                LCD_Puts(".");
                LCD_PutChar(adfst+48);
                LCD_Line(13,1);
                LCD_Puts(" V ");
				switch(Sub_menu)
				{
				case 0:
                          
                          clb_set_val = (uint16_t)(Battery.Voltage*10);

							LCD_Line(0,2);
							LCD_Puts("ACT_VAL: ");
							LCD_Line(9,2);
							AdcData__Conv((unsigned int)(Battery.Voltage*10));
							LCD_PutChar(adthrd+48);
							LCD_PutChar(adsnd+48);
							LCD_Puts(".");
							LCD_PutChar(adfst+48);
							LCD_Line(13,2);
							LCD_Puts(" V ");


							Sub_menu = 1;

						break;

                        case 1:
                        	if(key_value==2 && clb_set_val< (Battery.Voltage*15.0))
                        	{
                        		clb_set_val = clb_set_val+1;
                        	}
                        	else if(key_value==3 && clb_set_val>(Battery.Voltage/0.20))
                        	{
                        		clb_set_val = clb_set_val-1;

                        	}
                                if(Battery.Voltage > 10.0)
                                  Calib_Scalning_Factor[FACTOR_POS_BATT_VOL] = (float)(((float)clb_set_val)/(Battery.Voltage*10.0));
                                //Adc_Multiplier[FACTOR_POS_BATT_VOL] *= Calib_Scalning_Factor[FACTOR_POS_BATT_VOL];
                        	AdcData__Conv((unsigned int)clb_set_val);
                        	LCD_Line(9,2);
                                LCD_PutChar(adthrd+48);
                                LCD_PutChar(adsnd+48);
                                LCD_Puts(".");
                                LCD_PutChar(adfst+48);
                                LCD_Line(13,1);
                                LCD_Puts(" V ");

                          break;
					}
				break;
				case SLR_V_CLBR:
					LCD_Line(0,1);

									switch(Sub_menu)
									{
									case 0:
					                        //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);
										LCD_Puts("SLR_VOL: ");
										LCD_Line(9,1);
										AdcData__Conv((unsigned int)(Solar.Voltage*10));
										LCD_PutChar(adfrth+48);
										LCD_PutChar(adthrd+48);
										LCD_PutChar(adsnd+48);
                                        LCD_Puts(".");
										LCD_PutChar(adfst+48);
										LCD_Line(14,1);
										LCD_Puts(" V");
										clb_read_val = Solar.Voltage*10;
					                          clb_set_val = (uint16_t)(Solar.Voltage*10);

												LCD_Line(0,2);
												LCD_Puts("ACT_VAL: ");
												LCD_Line(9,2);
												AdcData__Conv((unsigned int)(Solar.Voltage*10));
												LCD_PutChar(adfrth+48);
                                                LCD_PutChar(adthrd+48);
												LCD_PutChar(adsnd+48);
												LCD_Puts(".");
												LCD_PutChar(adfst+48);
                                                                                                
												LCD_Line(14,2);
												LCD_Puts(" V");


												Sub_menu = 1;

											break;

					                        case 1:
					                        	if(key_value==2 && clb_set_val< 1700)
					                        	{
					                        		clb_set_val = clb_set_val+1;
					                        	}
					                        	else if(key_value==3 && clb_set_val>500)
					                        	{
					                        		clb_set_val = clb_set_val-1;

					                        	}
                                                                        if(Solar.Voltage > 10.0)
					                        	Calib_Scalning_Factor[FACTOR_POS_SOLOAR_VOL] = (float)(((float)clb_set_val)/(Solar.Voltage*10.0));
                                                                        
					                        	AdcData__Conv((unsigned int)clb_set_val);
					                        	LCD_Line(9,2);
					                        	LCD_PutChar(adfrth+48);
					                                LCD_PutChar(adthrd+48);
					                                LCD_PutChar(adsnd+48);
					                                LCD_Puts(".");
					                                LCD_PutChar(adfst+48);
					                                LCD_Line(14,1);
					                                LCD_Puts(" V ");

					                          break;
										}

					break;
                                        	case INV_VOl_CLBR:
						 LCD_Line(0,1);
						                LCD_Puts("INV_VOL: ");
						                LCD_Line(9,1);
						                AdcData__Conv((unsigned int)(Inverter_OP.voltage));
						                LCD_PutChar(adthrd+48);
						                LCD_PutChar(adsnd+48);
						                LCD_PutChar(adfst+48);
						                LCD_Line(12,1);
						                LCD_Puts(" V ");
										switch(Sub_menu)
										{
										case 0:
						                        //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

						                          clb_set_val = (uint16_t)(Inverter_OP.voltage);

													LCD_Line(0,2);
													LCD_Puts("ACT_VAL: ");
													LCD_Line(9,2);
													AdcData__Conv((unsigned int)(Inverter_OP.voltage));
													LCD_PutChar(adthrd+48);
													LCD_PutChar(adsnd+48);
													LCD_PutChar(adfst+48);
													LCD_Line(12,2);
													LCD_Puts(" V ");


													Sub_menu = 1;

												break;

						                        case 1:
						                        	if(key_value==2 && clb_set_val< 280)
						                        	{
						                        		clb_set_val = clb_set_val+1;
						                        	}
						                        	else if(key_value==3 && clb_set_val>50)
						                        	{
						                        		clb_set_val = clb_set_val-1;

						                        	}
                                                                                 if(Inverter_OP.voltage > 90.0)
						                        	Calib_Scalning_Factor[FACTOR_POS_INVOP_VOL] = (float)(((float)clb_set_val)/(Inverter_OP.voltage));
						                        	AdcData__Conv((unsigned int)clb_set_val);
						                        	LCD_Line(9,2);
						                                LCD_PutChar(adthrd+48);
						                                LCD_PutChar(adsnd+48);
						                                LCD_PutChar(adfst+48);
						                                LCD_Line(12,1);
						                                LCD_Puts(" V ");

						                          break;
											}

						break;
                                                //
                                                case BAT_DIS_I_CLBR:
						 LCD_Line(0,1);
						                LCD_Puts("BAT_CUR:-");
						                LCD_Line(9,1);
						                AdcData__Conv((unsigned int)(Battery.Discharging_current*10));
						                LCD_PutChar(adthrd+48);
						                LCD_PutChar(adsnd+48);
						                LCD_Puts(".");
						                LCD_PutChar(adfst+48);
						                LCD_Line(13,1);
						                LCD_Puts(" A ");
										switch(Sub_menu)
										{
										case 0:
						                        //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

						                          clb_set_val = (uint16_t)(Battery.Discharging_current*10);

													LCD_Line(0,2);
													LCD_Puts("ACT_VAL: ");
													LCD_Line(9,2);
													AdcData__Conv((unsigned int)(Battery.Discharging_current*10));
													LCD_PutChar(adthrd+48);
													LCD_PutChar(adsnd+48);
													LCD_Puts(".");
													LCD_PutChar(adfst+48);
													LCD_Line(13,2);
													LCD_Puts(" A ");


													Sub_menu = 1;

												break;

						                        case 1:
						                        	if(key_value==2 && clb_set_val< 250)
						                        	{
						                        		clb_set_val = clb_set_val+1;
						                        	}
						                        	else if(key_value==3 && clb_set_val>50)
						                        	{
						                        		clb_set_val = clb_set_val-1;

						                        	}
                                                                                if(Battery.Discharging_current > 1.0)
						                        	Calib_Scalning_Factor[FACTOR_POS_BATT_OCUR] = (float)(((float)clb_set_val)/(Battery.Discharging_current*10.0));
						                        	AdcData__Conv((unsigned int)clb_set_val);
						                        	LCD_Line(9,2);
						                                LCD_PutChar(adthrd+48);
						                                LCD_PutChar(adsnd+48);
						                                LCD_Puts(".");
						                                LCD_PutChar(adfst+48);
						                                LCD_Line(13,1);
						                                LCD_Puts(" A ");

						                          break;
											}

						break;
                                                
                                                case TEMP_COMPN_CLBR:
						 LCD_Line(0,1);
						                LCD_Puts("TEM_COM: ");
						                LCD_Line(9,1);
						                AdcData__Conv((unsigned int)(Temperature.compensation*10));
						                LCD_PutChar(adthrd+48);
						                LCD_PutChar(adsnd+48);
						                LCD_Puts(".");
						                LCD_PutChar(adfst+48);
						                LCD_Line(13,1);
						                LCD_Puts(" C ");
										switch(Sub_menu)
										{
										case 0:
						                        //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

						                          clb_set_val = (uint16_t)(Temperature.compensation*10);

													LCD_Line(0,2);
													LCD_Puts("ACT_VAL: ");
													LCD_Line(9,2);
													AdcData__Conv((unsigned int)(Temperature.compensation*10));
													LCD_PutChar(adthrd+48);
													LCD_PutChar(adsnd+48);
													LCD_Puts(".");
													LCD_PutChar(adfst+48);
													LCD_Line(13,2);
													LCD_Puts(" C ");


													Sub_menu = 1;

												break;

						                        case 1:
						                        	if(key_value==2 && clb_set_val< 250)
						                        	{
						                        		clb_set_val = clb_set_val+1;
						                        	}
						                        	else if(key_value==3 && clb_set_val>50)
						                        	{
						                        		clb_set_val = clb_set_val-1;

						                        	}
                                                                                if(Temperature.compensation > 10)
						                        	Calib_Scalning_Factor[FACTOR_POS_TEMP_COMP] = (float)(((float)clb_set_val)/(Temperature.compensation*10.0));
						                        	AdcData__Conv((unsigned int)clb_set_val);
						                        	LCD_Line(9,2);
						                                LCD_PutChar(adthrd+48);
						                                LCD_PutChar(adsnd+48);
						                                LCD_Puts(".");
						                                LCD_PutChar(adfst+48);
						                                LCD_Line(13,1);
						                                LCD_Puts(" C ");

						                          break;
											}

						break;
                                                case MAINS_VOl_CLBR:
						 LCD_Line(0,1);
						                LCD_Puts("MAINS_V: ");
						                LCD_Line(9,1);
						                AdcData__Conv((unsigned int)(Mains.voltage));
						                LCD_PutChar(adthrd+48);
						                LCD_PutChar(adsnd+48);
						                LCD_PutChar(adfst+48);
						                LCD_Line(12,1);
						                LCD_Puts(" V ");
										switch(Sub_menu)
										{
										case 0:
						                        //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

						                          clb_set_val = (uint16_t)(Mains.voltage);

													LCD_Line(0,2);
													LCD_Puts("ACT_VAL: ");
													LCD_Line(9,2);
													AdcData__Conv((unsigned int)(Mains.voltage));
													LCD_PutChar(adthrd+48);
													LCD_PutChar(adsnd+48);
													LCD_PutChar(adfst+48);
													LCD_Line(12,2);
													LCD_Puts(" V ");


													Sub_menu = 1;

												break;

						                        case 1:
						                        	if(key_value==2 && clb_set_val< 290)
						                        	{
						                        		clb_set_val = clb_set_val+1;
						                        	}
						                        	else if(key_value==3 && clb_set_val>50)
						                        	{
						                        		clb_set_val = clb_set_val-1;

						                        	}
                                                                               // if(Mains.voltage > 90)
						                        	//Calib_Scalning_Factor[FACTOR_POS_MAIN_VOL] = (float)(((float)clb_set_val)/(Mains.voltage));
						                        	AdcData__Conv((unsigned int)clb_set_val);
						                        	LCD_Line(9,2);
						                                LCD_PutChar(adthrd+48);
						                                LCD_PutChar(adsnd+48);
						                                LCD_PutChar(adfst+48);
						                                LCD_Line(12,1);
						                                LCD_Puts(" V ");

						                          break;
											}

						break;
                                                
                                                
                                                //
                                                case INV_I_CLBR:
						 LCD_Line(0,1);
						                LCD_Puts("INV_CUR: ");
						                LCD_Line(9,1);
						                AdcData__Conv((unsigned int)(Inverter_OP.current*10));
						                LCD_PutChar(adthrd+48);
						                LCD_PutChar(adsnd+48);
						                LCD_Puts(".");
						                LCD_PutChar(adfst+48);
						                LCD_Line(13,1);
						                LCD_Puts(" A ");
										switch(Sub_menu)
										{
										case 0:
						                        //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

						                          clb_set_val = (uint16_t)(Inverter_OP.current*10);

													LCD_Line(0,2);
													LCD_Puts("ACT_VAL: ");
													LCD_Line(9,2);
													AdcData__Conv((unsigned int)(Inverter_OP.current*10));
													LCD_PutChar(adthrd+48);
													LCD_PutChar(adsnd+48);
													LCD_Puts(".");
													LCD_PutChar(adfst+48);
													LCD_Line(13,2);
													LCD_Puts(" A ");


													Sub_menu = 1;

												break;

						                        case 1:
						                        	if(key_value==2 && clb_set_val< 250)
						                        	{
						                        		clb_set_val = clb_set_val+1;
						                        	}
						                        	else if(key_value==3 && clb_set_val>50)
						                        	{
						                        		clb_set_val = clb_set_val-1;

						                        	}
                                                                                if(Inverter_OP.current > 1.0)
						                        	Calib_Scalning_Factor[FACTOR_POS_INVOP_CUR] = (float)(((float)clb_set_val)/(Inverter_OP.current*10.0));
						                        	AdcData__Conv((unsigned int)clb_set_val);
						                        	LCD_Line(9,2);
						                                LCD_PutChar(adthrd+48);
						                                LCD_PutChar(adsnd+48);
						                                LCD_Puts(".");
						                                LCD_PutChar(adfst+48);
						                                LCD_Line(13,1);
						                                LCD_Puts(" A ");

						                          break;
											}

						break;
					case BAT_I_CLBR:
						 LCD_Line(0,1);
						                LCD_Puts("BAT_CUR: ");
						                LCD_Line(9,1);
						                AdcData__Conv((unsigned int)(Battery.Current*10));
						                LCD_PutChar(adthrd+48);
						                LCD_PutChar(adsnd+48);
						                LCD_Puts(".");
						                LCD_PutChar(adfst+48);
						                LCD_Line(13,1);
						                LCD_Puts(" A ");
										switch(Sub_menu)
										{
										case 0:
						                        //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

						                          clb_set_val = (uint16_t)(Battery.Current*10);

													LCD_Line(0,2);
													LCD_Puts("ACT_VAL: ");
													LCD_Line(9,2);
													AdcData__Conv((unsigned int)(Battery.Current*10));
													LCD_PutChar(adthrd+48);
													LCD_PutChar(adsnd+48);
													LCD_Puts(".");
													LCD_PutChar(adfst+48);
													LCD_Line(13,2);
													LCD_Puts(" A ");


													Sub_menu = 1;

												break;

						                        case 1:
						                        	if(key_value==2 && clb_set_val< 250)
						                        	{
						                        		clb_set_val = clb_set_val+1;
						                        	}
						                        	else if(key_value==3 && clb_set_val>50)
						                        	{
						                        		clb_set_val = clb_set_val-1;

						                        	}
                                                                                if(Battery.Current > 1.0)
						                        	Calib_Scalning_Factor[FACTOR_POS_BATT_ICUR] = (float)(((float)clb_set_val)/(Battery.Current*10.0));
						                        	AdcData__Conv((unsigned int)clb_set_val);
						                        	LCD_Line(9,2);
						                                LCD_PutChar(adthrd+48);
						                                LCD_PutChar(adsnd+48);
						                                LCD_Puts(".");
						                                LCD_PutChar(adfst+48);
						                                LCD_Line(13,1);
						                                LCD_Puts(" A ");

						                          break;
											}

						break;

						case SLR_I_CLBR:
							 LCD_Line(0,1);
							                LCD_Puts("SLR_CUR: ");
							                LCD_Line(9,1);
							                AdcData__Conv((unsigned int)(Solar.Current*10));
							                LCD_PutChar(adthrd+48);
							                LCD_PutChar(adsnd+48);
							                LCD_Puts(".");
							                LCD_PutChar(adfst+48);
							                LCD_Line(13,1);
							                LCD_Puts(" A ");
											switch(Sub_menu)
											{
											case 0:
							                        //  sprintf((char*)float_buff_key,"%s %.2f %s ",parameter_name_key[0],Display_Buff_key[0],unit_name_key[0]);

							                          clb_set_val = (uint16_t)(Solar.Current*10);

														LCD_Line(0,2);
														LCD_Puts("ACT_VAL: ");
														LCD_Line(9,2);
														AdcData__Conv((unsigned int)(Solar.Current*10));
														LCD_PutChar(adthrd+48);
														LCD_PutChar(adsnd+48);
														LCD_Puts(".");
														LCD_PutChar(adfst+48);
														LCD_Line(13,2);
														LCD_Puts(" A ");


														Sub_menu = 1;

													break;

							                        case 1:
							                        	if(key_value==2 && clb_set_val< 600)
							                        	{
							                        		clb_set_val = clb_set_val+1;
							                        	}
							                        	else if(key_value==3 && clb_set_val>50)
							                        	{
							                        		clb_set_val = clb_set_val-1;

							                        	}
                                                                                        if(Solar.Current > 1.0)
							                        	Calib_Scalning_Factor[FACTOR_POS_SOLOAR_CUR] = (float)(((float)clb_set_val)/(Solar.Current*10.0));
							                        	AdcData__Conv((unsigned int)clb_set_val);
							                        	LCD_Line(9,2);
							                                LCD_PutChar(adthrd+48);
							                                LCD_PutChar(adsnd+48);
							                                LCD_Puts(".");
							                                LCD_PutChar(adfst+48);
							                                LCD_Line(13,1);
							                                LCD_Puts(" A ");

							                          break;
												}
							break;

							

			}


}

void ClibPassword__Scan(void)
{

	unsigned int time_out=0;

	LCD_Line(0,1);
	LCD_Puts("Enter Password  ");
	//delay(100);
	time_out=0;
	secure_value[0]=0;
    secure_value[1]=0;
    secure_value[2]=0;
    secure_value[3]=0;
    secure_value[4]=0;
	secure_value[5]=0;
    secure_value[6]=0;
	secure_value[7]=0;
    secure_value[8]=0;
	secure_value[9]=0;

	key_value=0;
	value_position=0;
	value_position=0;
	value_position=0;
	//buzz=0;
	key_value=0;
	LCD_Line(0,2);
		//out(' ',1);
	while(key_value!=4  && time_out<9000)
	{

		//delay(1000);
		time_out++;
		//CLRWDT();
		if(key_pressed==1)
		{
			key_flag=0;
			//TMR1L = 0xEF;       							// LOAD TIMER LOW REGISTOR VALUE
			//TMR1H = 0xD8;
			key_pressed=0;
			count=0;
			//back_light_cntrl=1;
		}

		if(((key_value==1) || (key_value==2) || (key_value==3) ) && (value_position<12))
		{
			value_position++;
			secure_value[value_position]=key_value;
			key_value=0;
			LCD_Puts("X");
		}
	}
        	//      password   =====  >  IISSSIDDS
	if(secure_value[1]==2 && secure_value[2]==2 && secure_value[3]==1 && secure_value[4]==1 && secure_value[5]==1 && secure_value[6]==2&& secure_value[7]==3&& secure_value[8]==3&& secure_value[9]==1)
	{
		pass_flag=1;
	}
	else
	pass_flag=0;
	//pass_flag=1;
}


void Password__Scan(void)
{
	 
	unsigned int time_out=0;
		//out(' ',1);
	if(key_value!=4  && time_out<9000)
	{
		
		time_out++;
		if(key_pressed==1) 
		{
			key_flag=0;

			key_pressed=0;
			count=0;
		}
		
		if(((key_value==1) || (key_value==2) || (key_value==3) ) && (value_position<12))
		{
                  	LCD_Line(value_position++,2);
			
			secure_value[value_position]=key_value;
			key_value=0;
			LCD_Puts("X");
		}
	}    
	else if(time_out>8888)
	{
		Keypad.parameter_setting = 0;
		Keypad.password_scan_mode =0;
		time_out =0;
		key_value=0;
		LCD_Clear();
                time_out=0;
	secure_value[0]=0;
    secure_value[1]=0;
    secure_value[2]=0;
    secure_value[3]=0;
    secure_value[4]=0;
	secure_value[5]=0;
    secure_value[6]=0;
	secure_value[7]=0;
    secure_value[8]=0;
	secure_value[9]=0;
  
	key_value=0;
	value_position=0;
	value_position=0;
	value_position=0;
	//buzz=0;
	key_value=0;
	}
        	//      password   =====  >  I ISSSIDDS
	

}


void AdcData__Conv(unsigned int Data)
{
     adfst	=	Data % 10;
	adsnd	=	(Data/10)%10;
	adthrd	=	(Data/100)%10 ;
	adfrth	=	(Data/1000)%10;
	adffth 	=	(Data/10000) %10;//
}


int CheckAllow__Security(char Data, char * buff)
{

	while(*buff != '\0')
	{
		if(Data == *buff)
		{
			return 1;
		}
                buff++;

	}
	return 0;
}
