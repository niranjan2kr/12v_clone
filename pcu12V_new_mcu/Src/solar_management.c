/* Includes ------------------------------------------------------------------*/
#include "version_def.h"
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim8;
// local variables
float Pre_Bat_Vol,Pre_pv_Vol,Pre_PV_current,Pre_Bat_Current,Pre_PVIP_Power;


// fie details: contain---> charging management, load management, fault management, softstart management
void SoftStartCharging__Management(void)
{
	if(Flags_F.pv_available==SET && Flags_fault.pv_over_voltage == RESET && Flags_fault.batt_over_voltage == RESET  && (Pre_PVIP_Power<ParameterSet.pv_overload_watts))
	{
		if((Flags_fault.NTC_MPPT_Fail == RESET) && (Flags_fault.batt_over_voltage==RESET) && (Flags_F.soft_start_finished == RESET))
		{
                  /////// Batt dead softstsrt check
                        if(Battery.Voltage > ParameterSet.Batt_Boost_voltage-18 /*13.0*/)
			PWM.Soft_Start = (((Battery.Voltage+1.5)/ Solar.Voltage) * PWM.Duty_limt)+PWM.Soft2final_offset;
                           else PWM.Soft_Start = (((12+0.5)/ Solar.Voltage) * PWM.Duty_limt)+PWM.Soft2final_offset;
                        PWM.Final = (int)PWM.Soft_Start;
                          if(PWM.Final> PWM.Duty_limt )
                                PWM.Final = PWM.Duty_limt-50;
                        HAL_TIMEx_PWMN_Start(&htim8,TIM_CHANNEL_1);
                        HAL_TIM_PWM_Start(&htim8,TIM_CHANNEL_1);                //start pwm channel 1 //always after ADC conversion//test1
                        htim8.Instance->CCR1=PWM.Final;//PWM.Final;
                        // set soft start as finished
                        Flags_F.soft_start_finished = SET;// raedy to go in RunStateCharging__Management
                        Flag_ChargingState.Float = RESET;// reinitiate float charging
                        Flag_ChargingState.Boost = RESET;// reinitiate boost charging.

		}
                
	}
        if(Flags_F.pv_available==RESET || Flags_fault.pv_over_voltage == SET || Flags_fault.batt_over_voltage == SET || Flags_fault.batt_over_voltage==SET || Flags_fault.NTC_MPPT_Fail == SET)
        {
          HAL_TIM_PWM_Stop(&htim8,TIM_CHANNEL_1);                //start pwm channel 1 //always after ADC conversion//test1
          HAL_TIMEx_PWMN_Stop(&htim8,TIM_CHANNEL_1);
          PWM.Final =0;
          Flags_F.soft_start_finished = RESET;
          //FAULT_ON;
        }
       if( Flags_fault.NTC_MPPT_Fail == SET)
        {
          //FAULT_OFF
            ;
        }
  
  
}

float Power_Diff=0.0;
float Voltage_Diff=0.0;

void Charging__Management(void)// mppt algo // 10-15 ms second tracking
{
  

Power_Diff = (Solar.Current * Solar.Voltage)-Pre_PVIP_Power;
Voltage_Diff = Solar.Voltage - Pre_Bat_Vol;
Pre_Bat_Vol = Solar.Voltage ;
Pre_PV_current = Solar.Current;
Pre_Bat_Current = Battery.Current;
Pre_PVIP_Power = Solar.Current * Solar.Voltage;
//Power_Diff = 0;// for testing.

//ParameterSet.pv_overload_watts
   if((Flags_F.pv_available == SET ) && ((Pre_PVIP_Power<ParameterSet.pv_overload_watts)) && (Flags_fault.pv_over_voltage == RESET )&& /*(Flags_F.pwm_duty_OverLimit == RESET) && */(Flags_fault.NTC_MPPT_Fail == RESET) && (Flags_fault.batt_over_voltage==RESET) && (Flags_F.soft_start_finished == SET))
   {
           if((Battery.Current < ParameterSet.Batt_min_current && Solar.Current<1.0 ))  
           {
              if(Battery.Voltage < ParameterSet.Batt_Float_voltage-0.25)
              {
                if(Flags_F.pwm_duty_OverLimit == RESET)
                PWM.Final = PWM.Final+2;
                htim8.Instance->CCR1=PWM.Final;
              }
           
             else if((Battery.Voltage > ParameterSet.Batt_Float_voltage+0.25) /* (Battery.Current >0.2) */ || (Battery.Current> ParameterSet.Batt_max_current + 1) )
             {
               if(PWM.Final > PWM.Duty_min)
                PWM.Final = PWM.Final-2;
               htim8.Instance->CCR1=PWM.Final; 
                           
             }
             Flag_ChargingState.Float = SET;
             Flag_ChargingState.Boost = RESET;


          }
       
         
           else if (Flag_State.SMR_INPUT==SET)
           {
             
             if(Battery.Current < ParameterSet.Batt_min_current )
             {
            if(Battery.Voltage < (ParameterSet.Batt_Float_voltage-0.25) && (Battery.Current < ParameterSet.Batt_max_current-1.0)  )
                          {
                               if(PWM.Final < PWM.Duty_limt )
                                  {
                                    PWM.Final = PWM.Final+1;
                                    
                                  }
                           }
                    else if(Battery.Voltage > (ParameterSet.Batt_Float_voltage+0.25) || (Battery.Current > ParameterSet.Batt_max_current+1.0) )
                    {
                      if(PWM.Final > PWM.Duty_min )
                      {
                       PWM.Final = PWM.Final-1;
                      
                      }
                    }
            
             htim8.Instance->CCR1=PWM.Final;
           }
           
         else
             {
            if(Battery.Voltage < (ParameterSet.Batt_Boost_voltage-0.25) && (Battery.Current < ParameterSet.Batt_max_current-1.0)  )
                          {
                               if(PWM.Final < PWM.Duty_limt )
                                  {
                                    PWM.Final = PWM.Final+1;
                                    
                                  }
                           }
                    else if(Battery.Voltage > (ParameterSet.Batt_Boost_voltage+0.25) || (Battery.Current > ParameterSet.Batt_max_current+1.0) )
                    {
                      if(PWM.Final > PWM.Duty_min )
                      {
                       PWM.Final = PWM.Final-1;
                      
                      }
                    }
            
             htim8.Instance->CCR1=PWM.Final;
           }
           
           }
           
           else if(Solar.Current < 1.0 ) //if solar current <1A no tracking
           {
             
             if(Battery.Voltage < (ParameterSet.Batt_Boost_voltage-0.25) && (Battery.Current < ParameterSet.Batt_max_current-1.0))
           {
           
            if(PWM.Final < PWM.Duty_limt )
               {
                   PWM.Final = PWM.Final+1;
               }
           }
                
                 else if(Battery.Voltage > (ParameterSet.Batt_Boost_voltage+0.25) || (Battery.Current > ParameterSet.Batt_max_current+1.0))
           {
           
                    if(PWM.Final > PWM.Duty_min )
               {
                   PWM.Final = PWM.Final-1;
               }
           }
            htim8.Instance->CCR1=PWM.Final;
           
           }
           
           
           
           else /// tracking
           {
            
             
             
             
            if( Battery.Current > ParameterSet.Batt_min_current)
            {
             Flag_ChargingState.Float = RESET;
             Flag_ChargingState.Boost = SET;
             if(Power_Diff >= 1)// if sufficient power available @ PV
                   {
                      if ((Battery.Current < ParameterSet.Batt_max_current) && (Battery.Voltage < ParameterSet.Batt_Boost_voltage-0.2))
                          
                     
                           if(Voltage_Diff <= 0)  
                           {
                             if(Flags_F.pwm_duty_OverLimit == RESET)
                             PWM.Final = PWM.Final+2;
                             htim8.Instance->CCR1=PWM.Final; // duty cycle++
                           }
                           else if(Voltage_Diff > 0 )
                           {
                              if(PWM.Final > PWM.Duty_min)
                             PWM.Final = PWM.Final-2;
                             htim8.Instance->CCR1=PWM.Final; // duty cycle--
                           }
                   }
                   else if(Power_Diff < -1)
                   {
                     if((Battery.Current > ParameterSet.Batt_max_current+1.0) || (Battery.Voltage > ParameterSet.Batt_Boost_voltage+0.5))
                     {
                     if(PWM.Final > PWM.Duty_min)
                     {  
                             PWM.Final = PWM.Final-2;
                             htim8.Instance->CCR1=PWM.Final;
                     }
                     
                     }
                   
                     
                         else
                      {
                        if(Voltage_Diff<0)
                        {
                          if(PWM.Final > PWM.Duty_min )
                          {
                             PWM.Final= PWM.Final-3; 
                         }
                        }
                        else if(Voltage_Diff>0)
                        {
                          if(PWM.Final < PWM.Duty_limt )
                          {
                             PWM.Final= PWM.Final+3; 
                          }                    
                        }
                        htim8.Instance->CCR1=PWM.Final;
                        
                      } 
                   }
           }
           
            else 
              {
            Flag_ChargingState.Float = SET;
             Flag_ChargingState.Boost = RESET;
             if(Power_Diff >= 1)// if sufficient power available @ PV
                   {
                      if ((Battery.Current < ParameterSet.Batt_max_current) && (Battery.Voltage < ParameterSet.Batt_Float_voltage-0.2))
                          
                     
                           if(Voltage_Diff <= 0)  
                           {
                             if(Flags_F.pwm_duty_OverLimit == RESET)
                             PWM.Final = PWM.Final+2;
                             htim8.Instance->CCR1=PWM.Final; // duty cycle++
                           }
                           else if(Voltage_Diff > 0 )
                           {
                              if(PWM.Final > PWM.Duty_min)
                             PWM.Final = PWM.Final-2;
                             htim8.Instance->CCR1=PWM.Final; // duty cycle--
                           }
                   }
                   else if(Power_Diff < -1)
                   {
                     if((Battery.Current > ParameterSet.Batt_max_current+1.0) || (Battery.Voltage > ParameterSet.Batt_Float_voltage+0.5))
                     {
                     if(PWM.Final > PWM.Duty_min)
                     {  
                             PWM.Final = PWM.Final-2;
                             htim8.Instance->CCR1=PWM.Final;
                     }
                     
                     }
                   
                     
                         else
                      {
                        if(Voltage_Diff<0)
                        {
                          if(PWM.Final > PWM.Duty_min )
                          {
                             PWM.Final= PWM.Final-3; 
                         }
                        }
                        else if(Voltage_Diff>0)
                        {
                          if(PWM.Final < PWM.Duty_limt )
                          {
                             PWM.Final= PWM.Final+3; 
                          }                    
                        }
                        htim8.Instance->CCR1=PWM.Final;
                        
                      } 
                   }
           }
            
            
            
               
           
              htim8.Instance->CCR1=PWM.Final;

                   }

   }
   
   else if(Pre_PVIP_Power>ParameterSet.pv_overload_watts+50)
   {
	   
	   if(PWM.Final > PWM.Duty_min )
          {
            PWM.Final= PWM.Final-1; 
          }
	   
	   
	   
   }
   
   
}
 
void Fault__Management(void)// all type of faults, related to pv,batt, load,ble,storage etc.
{
      
  if((Mains.frq < 47.0 /*Mains.Frq_low_limit*/ || Mains.frq > 53.0 /*Mains.Frq_hi_limit*/))                          
  {
    Mains_flags.MAINS_OK = 0;
    Inverter_ParameterSet.PWM_gridChrg = 0;
    MAINS_OFF;
    Mains_flags.available = RESET; 
    LED.grid_charging_on=0;
    if(Mains.charging_disabled ==  RESET)
    {
      Mains.charging_disabled =SET;
      Mains_flags.charging_start = 0;

      HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_2);
      HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_2);
      HAL_TIM_PWM_Stop(&htim1,TIM_CHANNEL_3);  
      HAL_TIMEx_PWMN_Stop(&htim1,TIM_CHANNEL_3);
      htim1.Instance->CCR3 =0;
      htim1.Instance->CCR2=0;
    }
  }
  if( Mains_flags.available ==RESET || Mains_flags.MAINS_OK==RESET || Mains_flags.charging_start==RESET )
  {
    
     LED.grid_charging_on=0;
  }
  
  
  if((Solar.Voltage*Solar.Current) > ParameterSet.pv_overload_watts+1 && Flags_fault.pv_over_load==0)
  {
    if(pv_overWatts_timer++ > 2500)
    {
		/*
      pv_overWatts_timer = 0;
      Flags_fault.pv_over_load = 1;
      HAL_TIM_PWM_Stop(&htim8,TIM_CHANNEL_1);                //start pwm channel 1 //always after ADC conversion//test1
      HAL_TIMEx_PWMN_Stop(&htim8,TIM_CHANNEL_1);
      PWM.Final =0;
      
	  */
	  ;
    }
    
  }
  else pv_overWatts_timer = 0;
 
  if(Battery.Current >= 2.0 /*ParameterSet.minimum_current_led_on */)
  {
    LED.charging_on = SET;

    
  }
  else if(Battery.Current < 1.0 /*ParameterSet.minimum_current_led_on-0.30 */)
  {
    LED.charging_on = RESET;
     Flag_ChargingState.Float = SET;
      Flag_ChargingState.Boost = SET;
    
  }
  if(PWM.Final > PWM.Duty_limt)
  {
    Flags_F.pwm_duty_OverLimit = SET;
  }
  else Flags_F.pwm_duty_OverLimit = RESET;
  
  if((Solar.Voltage > ParameterSet.PV_overvoltage))
  {
    if(pv_over_timer++ > 20)
    {
      pv_over_timer = 0;
      Flags_fault.pv_over_voltage = SET;   // pv Over 
      HAL_TIM_PWM_Stop(&htim8,TIM_CHANNEL_1);                //start pwm channel 1 //always after ADC conversion//test1
      HAL_TIMEx_PWMN_Stop(&htim8,TIM_CHANNEL_1);
      PWM.Final =0;
      Flags_F.soft_start_finished = RESET;
      
       MPPT_CHARGING_OFF;
    }
    
  }
  
  
  else if((Solar.Voltage < (Battery.Voltage+1)) || (Solar.Voltage < ParameterSet.PV_undervoltage))
  {
    
    if(pv_under_timer++ > 6)
    {
      pv_under_timer =0;
       pv_on_timer =0;
      Flags_F.pv_available = RESET;// pv_under
      Flags_fault.pv_under_voltage = SET;
      HAL_TIM_PWM_Stop(&htim8,TIM_CHANNEL_1);                //start pwm channel 1 //always after ADC conversion//test1
      HAL_TIMEx_PWMN_Stop(&htim8,TIM_CHANNEL_1);
      PWM.Final =0;
      Flags_F.soft_start_finished = RESET;
      
     Flags_fault.pv_over_load = 0;
      MPPT_CHARGING_OFF;
      
      if(pv_under_timer > 4)
      {
     if(Solar.Voltage <= ( ParameterSet.PV_undervoltage) && Solar.Voltage >15.0 )
     {
       Flag_State.SMR_INPUT=0;
     }
      }
        
    }
  
    
  //  if(Solar.Voltage <= ( ParameterSet.PV_undervoltage) && Solar.Voltage >15.0 )
   //  {
    //   Flag_State.SMR_INPUT=0;
    // }
    if(Solar.Voltage < 15.0)
     {
       Flag_State.SMR_INPUT=1;
     }
    
  }
  else if((Solar.Voltage >  Battery.Voltage+3.0) && Solar.Voltage > ParameterSet.PV_undervoltage)
  {
     pv_under_timer=0;
     pv_over_timer=0;
    if(pv_on_timer++ > 150) //1500
    {
      pv_on_timer =0;
     Flags_F.pv_available = SET;// pv on
     Flags_fault.pv_over_voltage = RESET;
      Flags_fault.pv_under_voltage = RESET;
      if(LED.charging_on == RESET)
       MPPT_CHARGING_ON;
        
    }
  }
 
    

  
  if((Battery.Voltage < ParameterSet.Batt_undervoltage))
  {
    if(batt_underr_timer++ > 2400)
    {
      batt_underr_timer = 0;
      Flags_fault.batt_under_voltage = SET;   // bat low
      Flag_State.batt_medium_chagre = RESET;
      Flag_State.batt_full_charge = RESET;
    }
       
  }
  else if(Flags_fault.batt_under_voltage)
  {
   
    
    if((Battery.Voltage > ParameterSet.batt_reconnect_vol) || Mains_flags.charging_start == SET)// && Flags_fault.batt_under_voltage==SET)
    {
      
       if(batt_under_reconnect_timer++ > 2400)
       {
         batt_under_reconnect_timer=0;
      Flags_fault.batt_under_voltage = RESET;// bat ok
      Inverter_flags.health_check = RESET;// able to on inveter
       }
    }
    
    else if(Battery.Voltage < ParameterSet.batt_reconnect_vol)
    {
    batt_under_reconnect_timer=0;
    }
    
  }
  else if(Flags_fault.batt_under_voltage == RESET)
  {
    
    if((Battery.Voltage > ParameterSet.Batt_undervoltage))
    {
      batt_underr_timer = 0;
      Flags_fault.batt_under_voltage = RESET;   // bat low
    }
  }
  
  if(Battery.Voltage > ParameterSet.Batt_overvoltage+0.4)
  {
    
     if(batt_over_timer++ > 2400)
    {
    batt_over_timer = 0;
    Flags_fault.batt_over_voltage = SET;
    HAL_TIM_PWM_Stop(&htim8,TIM_CHANNEL_1);                //start pwm channel 1 //always after ADC conversion//test1
    HAL_TIMEx_PWMN_Stop(&htim8,TIM_CHANNEL_1);
    PWM.Final =0;
    Flags_F.soft_start_finished = RESET;
    }
  }
  else if(Flags_fault.batt_over_voltage)
  {
    if(Battery.Voltage < ParameterSet.Batt_overvoltage-3.0)
    {
       Flags_fault.batt_over_voltage = RESET;
        Inverter_flags.health_check = RESET;
      
    }
  }
  else if(!Flags_fault.batt_over_voltage)
  {
    if(Battery.Voltage < (ParameterSet.Batt_overvoltage-1.0))
    {
      batt_over_timer = 0;
      Flags_fault.batt_over_voltage = RESET;
      Inverter_flags.health_check = RESET;
    }
    
  }
  Load__Management();
  Battery__State_Calculation();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// objective: charged 70-100% then both load ON and 50-70% then only emergency load ON and after all LOAD OFF//
///////////////////////////////
void Load__Management(void)
{ 
  if( Flags_fault.NTC_MPPT_Fail==SET ||  Flags_fault.NTC_INV_Fail== SET || Flags_fault.batt_under_voltage == SET || Inverter_flags.overload == SET || Flags_fault.batt_over_voltage == SET )
  {
  ///FAULT_ON;
  
  }
  
  else if( Flags_fault.NTC_MPPT_Fail==RESET &&  Flags_fault.NTC_INV_Fail== RESET && Flags_fault.batt_under_voltage == RESET && Inverter_flags.overload == RESET && Flags_fault.batt_over_voltage == RESET )
  {
  FAULT_OFF;
  
  }
  
  
  
}



void Battery__State_Calculation(void)
{
  float calc;
  if(Battery.Voltage > 12.4 )
  {
    Flag_State.batt_full_charge=SET;  
     Flag_State.batt_medium_chagre=RESET;
  }
  if (Battery.Voltage < 12.3 && Battery.Voltage > ParameterSet.Batt_undervoltage)
  {
    Flag_State.batt_medium_chagre=SET;
    Flag_State.batt_full_charge = RESET;
  }
  if(Battery.Voltage > ParameterSet.Batt_undervoltage)
  {
    Flag_State.Back_up_time= ((Battery.Voltage - 10.5)/2.5)*8.0;
    if(Flag_State.Back_up_time> 8)
    {
     Flag_State.Back_up_time = 8.0;
    }
  }
}

_Bool start_both_blinking=0;
void LEDindication_Management (void)
{
  if(LED.charging_on)
  {
    if((Solar.Voltage > ParameterSet.PV_undervoltage) && (Mains_flags.available == SET && Mains_flags.grid_charging_indication == SET))
    {
      if(start_both_blinking == 0)
      {
        start_both_blinking  = 1;
        GRID_CHARGING_ON;
        MPPT_CHARGING_ON;
      }
      else
      {
         if(LED.chrg_blink_timer++ > 200) //50
        {
            LED.chrg_blink_timer=0;
            MPPT_CHARGING_BLINK;
            GRID_CHARGING_BLINK;
        }
      }
      
    }
    else if(Solar.Voltage > ParameterSet.PV_undervoltage)
    {
      start_both_blinking=0;
    if(LED.chrg_blink_timer++ > 200) //50
    {
      MPPT_CHARGING_BLINK;
      LED.chrg_blink_timer =0;
      
    }
    
    }
    else if(Mains_flags.available == SET && Mains_flags.grid_charging_indication == SET)
     {
       start_both_blinking=0;
        if(LED.grid_chrg_blink_timer++ > 200) //50
          {
            GRID_CHARGING_BLINK;
            LED.grid_chrg_blink_timer =0;          
          }
    }
   
  }
  else start_both_blinking =0;
  
 
  
  
  
  
}
 

