/**
  ******************************************************************************
  * @file            : usb_host.c
  * @version         : v1.0_Cube
  * @brief           : This file implements the USB Host
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2020 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "usb_host.h"
#include "usbh_core.h"
#include "usbh_msc.h"
#include "ff_gen_drv.h"
#include "usbh_diskio.h"
#include "main.h"
/* USB Host Core handle declaration */
FATFS USBDISKFatFs;           /* File system object for USB disk logical drive */
FIL MyFile;                   /* File object */
char USBDISKPath[4];        /* USB Host logical drive path */
USBH_HandleTypeDef hUsbHostFS;
typedef enum {
  APPLICATION_IDLE1 ,  
  APPLICATION_START1,    
  APPLICATION_RUNNING1,
}MSC_ApplicationTypeDef;
 
FRESULT res;                                          /* FatFs function common result code */
  uint32_t byteswritten, bytesread;                     /* File write/read counts */
  
MSC_ApplicationTypeDef Appli_state = APPLICATION_IDLE1;

/**
* -- Insert your variables declaration here --
*/ 
/* USER CODE BEGIN 0 */
static void MSC_Application(void);
/* USER CODE END 0 */

/*
* user callbak declaration
*/ 
static void USBH_UserProcess  (USBH_HandleTypeDef *phost, uint8_t id);

/**
* -- Insert your external function declaration here --
*/ 
/* USER CODE BEGIN 1 */
void USB_INIT (void)
{
  
  /* Init Host Library,Add Supported Class and Start the library*/
  USBH_Init(&hUsbHostFS, USBH_UserProcess, HOST_FS);

  USBH_RegisterClass(&hUsbHostFS, USBH_MSC_CLASS);

  USBH_Start(&hUsbHostFS);
  HAL_UART_Transmit(&huart2,(uint8_t *)"\r\nFatFS---> USB Ready\r\n",25,50);
}
/* USER CODE END 1 */

/* init function */				        
void MX_USB_HOST_Init(void)
{
  //int usb_loop=0;
      /* USB Host Background task */
      USBH_Process(&hUsbHostFS);
      
      /* Mass Storage Application State Machine */
      switch(Appli_state)
      {
      case APPLICATION_START:
        HAL_UART_Transmit(&huart2,(uint8_t *)"\r\nUSB Detected\r\n",20,50);
          MSC_Application();
        Appli_state = APPLICATION_IDLE1;
        break;
        
      case APPLICATION_IDLE:
      default:
        break;      
      }
}
/**
  * @brief  Main routine for Mass Storage Class
  * @param  None
  * @retval None
  */
char filename[10]= {'P','C','U','X'};
static void MSC_Application(void)
{
  BUZZER_ON
  uint8_t wtext[] = "PCU 24V test data EEPROM to pendrive copy testing"; /* File write buffer */
  uint8_t rtext[100];                                   /* File read buffer */
  //usb_timeout_counter = 0;// reset usb time out
  /* Register the file system object to the FatFs module */
  if(f_mount(&USBDISKFatFs, "0:", 1) != FR_OK)
  {
    /* FatFs Initialization Error */
    Error_Handler();
  }
  else
  {
        filecounter++;
      filename[4] = (filecounter/10)+48;
    filename[5] = (filecounter%10)+48;
     filename[6] ='.'; 
      filename[7] ='T'; 
       filename[8] ='X'; 
        filename[9] ='T'; 
      
    /* Create and Open a new text file object with write access */
    if(f_open(&MyFile, filename, FA_CREATE_ALWAYS | FA_WRITE) != FR_OK) 
    {
      /* 'STM32.TXT' file Open for write Error */
      Error_Handler();
    }
    else
    {
      HAL_UART_Transmit(&huart2,(uint8_t *)"\r\nUSB File Created: ",20,50);
      // HAL_UART_Transmit(&huart2,filename,10,50);
       HAL_UART_Transmit(&huart2,(uint8_t *)"\r\n",4,50);
      /* Write data to the text file */
      //res = f_write(&MyFile, usb_log_header, sizeof(usb_log_header), (void *)&byteswritten);
      //Append_mode = 1;
      //Copy_to_USB();
      res = f_write(&MyFile, wtext, sizeof(wtext), (void *)&byteswritten);
     // res = f_write(&MyFile, wtext, Usb_strng_size/*sizeof(wtext)*/, (void *)&byteswritten);
     // res = f_write(&MyFile, wtext, Usb_strng_size/*sizeof(wtext)*/, (void *)&byteswritten);
      //res = f_write(&MyFile, wtext, Usb_strng_size/*sizeof(wtext)*/, (void *)&byteswritten);
      if((res != FR_OK))
      {
        /* 'STM32.TXT' file Write or EOF Error */
        Error_Handler();
      }
      else
      {
              /* Close the open text file */
              f_close(&MyFile);
              HAL_Delay(50);
              BUZZER_OFF
             HAL_UART_Transmit(&huart2,(uint8_t *)"\r\nUSB Copy DONE & Unmounted\r\n",31,50);
              
        }
  
      
      }
    
    }
  /* Unlink the USB disk I/O driver */
  FATFS_UnLinkDriver(USBDISKPath);
}

/*
 * Background task
*/ 
void MX_USB_HOST_Process(void) 
{
  /* USB Host Background task */
    USBH_Process(&hUsbHostFS); 						
}
/*
 * user callback definition
*/

/*
 * user callback definition
*/ 
static void USBH_UserProcess  (USBH_HandleTypeDef *phost, uint8_t id)
{

  switch(id)
  { 
  case HOST_USER_SELECT_CONFIGURATION:
    break;
    
  case HOST_USER_DISCONNECTION:
    Appli_state = APPLICATION_IDLE1;
    f_mount(NULL, (TCHAR const*)"", 0);          
    break;
    
  case HOST_USER_CLASS_ACTIVE:
    Appli_state = APPLICATION_START1;
    break;
    
  default:
    break; 
  }
  /* USER CODE END CALL_BACK_1 */
}

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
