	/////////////////////////////////////
	#include "stm32f4xx_hal.h"//////////
	///////////////////////////////////
	extern uint8_t MPPT_ID[6];
	extern uint8_t R_DATA[15];
	extern uint8_t D_BUFF[200];
	extern uint8_t log_buff[15];
	extern uint8_t prepaid_buff[15];
	extern int i;
	extern uint8_t dummy;

	struct Flags
	{
		unsigned a:1;
		unsigned b:1;
		unsigned c:1;
		unsigned d:1;
		unsigned mppt_data:1;
		
	};

	struct MPPT_RX_flags
	{
		unsigned rx_start:1;
		unsigned rx_start1:1;
		unsigned rx_start2:1;
		unsigned rx_mppt_end:1;
		unsigned tx_mppt:1;
		unsigned log_end1:1;
		unsigned log_end2:1;	
	};

	struct mppt_cmd
	{
		unsigned device_status:1;
		unsigned device_log:1;
		unsigned pre_metering:1;
		unsigned dg_mode:1;
		unsigned mppt_id:1;
		
	};


	struct user_cmd
	{
		unsigned device_status:1;
		unsigned device_log:1;
		unsigned pre_metering:1;
		unsigned dg_mode:1;
		unsigned Load_on:1;
                unsigned Load_off:1;
		
	};
	
	struct mppt_data
	{
		unsigned status_rdy_to_sent_apps:1;
		unsigned log_redy_to_sent_apps:1;
		unsigned log_end_to_sent_apps:1;
	        unsigned prepaid_to_sent_apps:1;
		unsigned mppt_id_set:1;

		
	};