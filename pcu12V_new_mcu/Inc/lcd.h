#include "stm32f4xx_hal.h"
#include "main.h"


//defines
#define Line_1          0x80
#define Line_2          0xC0
#define Line_3          0x94
#define Line_4          0xD4
#define Cursor          0x0E
#define Clear_LCD       0x01
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////+

//prototypes
void LCD_Send(unsigned char DATA);
void LCD_SendCommand(unsigned char command);
void LCD_Enable (void);
void LCD_Line(unsigned char x, unsigned char y);
void LCD_PutChar(unsigned char DATA);
void LCD_INI(void);
uint32_t getUs(void);
void LCD_Clear(void);
void LCD_Puts(char *ptr);
uint32_t getUs(void);
void delayUs(uint32_t micros);
void DWT_Init(void);