#include "stm32f4xx_hal.h"
#include "main.h"
#include "stm32f4xx_hal.h"
//pending
/*Defines for eeprom addresses*/
#define EEprom1_ADD1                    0x50                //slave address for first eeprom
#define EEprom1_ADD2                    0x51                //slave address for second eeprom
#define EEprom1_ADD1_write              0xA0                //write address for eeprom 1
#define EEprom1_ADD2_write              0xA2                //write address for eeprom 2
#define EEprom1_ADD1_read               0xA1                //read address for eeprom 1
#define EEprom1_ADD2_read               0xA3                //read address for eeprom 2
/*END of Defines for eeprom addresses*/
/*Defines for storage values addresses*/
#define Start_Address 0x08040000
#define End_Address   0x0805ffff
#define log_length    12
#define ID_CAl_ADD    0x08060000;
/*END of Defines for storage values addresses*/
extern uint8_t DataBase[6][100];
extern uint8_t DataBaseMirror[6][100];
/*Protypes of storage Functions*/
void Clear_DataBase(void);
void Write_DataFlash(uint8_t *buffer, uint16_t length);//function to write a single character at an address provides end address as return
uint32_t Read_Data(uint32_t Address);//function to read a single character at an address
void DataBase_Update(uint8_t pg,uint8_t pg_adrs, uint8_t Data);
void Read__DataBase(void);
void Initialization_DataBase(void);
void UpdateSystem__FromFlash(uint8_t page);
void update_to_flash(void);
void read_data(void);
void Write_data_to_Flash(void);
/*End of Protypes of storage Functions*/
