
//display Settings are here
/*LED PINS and PORTS
MPPT_CHARGING_Pin               
MPPT_CHARGING_GPIO_Port         
PV_ON_Pin                       
PV_ON_GPIO_Port                 
MAINS_CHARGING_Pin              
MAINS_CHARGING_GPIO_Port        
LOAD_ON_Pin                     
LOAD_ON_GPIO_Port               
LOAD_CUTOFF_Pin                 
LOAD_CUTOFF_GPIO_Port           
USB_COPY_STATUS_Pin            
USB_COPY_STATUS_GPIO_Port   
LCD_BACKLIGHT_Pin 
LCD_BACKLIGHT_GPIO_Port 
USB_COPY_BUTTON_Pin 
USB_COPY_BUTTON_GPIO_Port//
*/ 

#include "lcd.h"
#include "main.h"
#include "stm32f4xx_hal.h"

extern float Battery_V;
extern float Solar_V;
extern float Solar_I;
extern float Battery_I;
 extern float Inv_OPV;
extern float Mains_I;
extern float Battery_temp;
extern float Load_I;
extern uint8_t display;
extern char PCUX_ID[6];
extern uint8_t aShowTime[13];            //Time Buffer for RTC
extern uint8_t aShowDate[13];
extern char parameter_name[20][20];
extern char unit_name[20][20];
extern float Display_Buff[20];
extern char float_buff[20];
extern uint8_t display;


/*Declaration of function prototypes*/
void Welcome_Show(void);
void Fault_led(void);
void Parameters_Display(void);
/*Declaration of function prototypes*/
extern UART_HandleTypeDef huart2;
