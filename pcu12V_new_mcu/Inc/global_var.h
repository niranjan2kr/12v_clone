#include "stm32f4xx_hal.h"
extern struct Flags_fault Flags_fault;
extern struct Flags_F Flags_F;
extern struct Battery Battery;
extern struct Solar  Solar;
extern struct Flag_ChargingState Flag_ChargingState;
extern struct PWM PWM;
extern struct ParameterSet ParameterSet;
extern struct System System;
extern struct  Load Load;
extern struct Flag_State Flag_State;
extern struct LED LED;
extern struct TIMERt TIMERt;
extern struct Temperature Temperature;
extern struct Mains Mains;
extern struct Services Services;
extern struct Energy Energy;
extern struct Inverter_OP Inverter_OP;
extern struct Mains_flags Mains_flags;
extern  struct Inverter_flags Inverter_flags;
extern struct Inverter_timers Inverter_timers;
extern struct Inverter_ParameterSet Inverter_ParameterSet;
extern struct Keypad Keypad;
extern struct Inverter_Mode Inverter_Mode;

/* Private variables ---------------------------------------------------------*/

extern uint8_t aShowTime[13];            //Time Buffer for RTC
extern uint8_t aShowDate[13];            //Date Buffer for RTC
extern _Bool blink_state;
extern _Bool Stsrt_apps;
//extern float Temperature;
extern _Bool inc_key_dis_pressed;
extern _Bool dec_key_dis_pressed;
 
extern  uint16_t pv_under_timer;
extern uint16_t pv_over_timer;
extern uint16_t pv_overWatts_timer;
extern uint16_t batt_underr_timer;
extern uint16_t batt_under_reconnect_timer;
extern uint16_t batt_over_timer;
extern uint16_t pv_on_timer;

/////////////////////////////
 /* Solar management */ 
/////////////////////////////////
struct  __attribute__((__packed__)) Flags_fault 
{
	

  /* solar faults flags*/
  uint8_t pv_under_voltage: 1;
  uint8_t pv_over_voltage : 1;
  uint8_t pv_over_load : 1;
  
  
  /* btt faults flags */
  uint8_t batt_under_voltage : 1;
  uint8_t batt_over_voltage : 1;
  uint8_t batt_disconnected:1;
  
  
  /* load faults */
  uint8_t load_overload:1;
  
  
  
  
  /* Ble comm faults */
  
  
  
  /* storage or logs fault */
  uint8_t eeprom_error:1;

  /* Temp Fault */
  uint8_t NTC_INV_Fail:1;
  uint8_t NTC_MPPT_Fail:1;
  uint8_t save_to_flash:1;
  uint8_t factory_reset_todo:1;
  uint8_t save_to_flash_user_fac:1;
  uint8_t save_to_flash_clb:1;
  uint8_t Short_ckt:1;
    
};

struct  __attribute__((__packed__)) Flags_F
{
  /* solar */
  uint8_t pv_available:1;
  
  /* charging */
  uint8_t batt_charging_start:1;
  uint8_t boost_charg_start:1;
  uint8_t float_chrg_start:1;
  uint8_t soft_start_finished:1;
  uint8_t soft_start_enable:1;
  uint16_t solar_tracking_time;// in milli second only
  
  /* load */
  uint8_t load_on:1;
  uint8_t load_disabled:1;
  
  /* Bluetooth */
  uint8_t bluetooth_enable:1;
  
  
  /* logs or eeprom */
  uint8_t storage_log_enable:1;
  uint8_t eeprom_init:1;
  
  /* LED control flags */
  uint8_t charging_led_on:1;
  uint8_t overload_led_on:1;
  uint8_t LowBatt_led_on:1;
  uint8_t Mains_chrgLED_on:1;
  uint8_t pv_available_LEDon:1;
  
  
 /* PWM control flag */
   uint8_t pwm_enable:1; // i.e use for driver shutdown and pulse disable
   uint8_t pwm_duty_OverLimit:1;
   uint8_t track_start:1;
   uint8_t mppt_track_start:1;
   uint8_t grid_chrg_timer:1;
   
   uint8_t dc_overload_set:1;
   uint8_t mute:1;

   uint8_t inv_sw_state:1;

   ////
   uint8_t mains_fails_detected_start:1;
   uint8_t mains_fails_detected_start_1:1;

};

struct __attribute__((__packed__)) Battery
{
	float Voltage;
	float Current;
	float Discharging_current;
	float Charge_power;
	float Discharge_power;


};

struct __attribute__((__packed__)) Inverter_Mode
{
uint8_t Mode_data;
uint8_t S_B_G:1;
uint8_t S_G_B:1;
uint8_t G_S_B:1;
uint8_t Peak_Demand:1;



};

struct __attribute__((__packed__)) Solar
{
	float Voltage;
	float Current;
	float Instantaneous_power;
        float output_current;
};

struct __attribute__((__packed__)) Load
{
  
  float Voltage;
  float Current;
  
  
};

struct __attribute__((__packed__)) PWM
{
	uint16_t Final;
	float Soft_Start;
	float Duty_limt; // read from EEPROM/Flash
	uint8_t Soft2final_offset;// read from EEPROM/flash
        float Duty_min;

};

struct __attribute__((__packed__)) Flag_ChargingState
{
	uint8_t Float:1;
	uint8_t Boost:1;
        


};

struct __attribute__((__packed__)) Flag_State
{
  uint8_t charging:1;
  uint8_t batt_helath:1;
  uint8_t power:1;
  float Back_up_time;
  uint8_t batt_full_charge:1;
  uint8_t batt_medium_chagre:1;
  uint8_t batt_charged:1;
  uint8_t SMR_INPUT:1;
  
};
// limits and parameters read from EEPROM or Flash
struct __attribute__((__packed__)) ParameterSet
{
	float sht_ckt_watts;
	float overload_watts;


	float PV_undervoltage;
	float pv_overload_watts;// use in solar over load
	float PV_overvoltage;

	float Batt_min_current;
	float Batt_BF_Cur;
	float Batt_FB_Cur;
	float Batt_max_current;
	float Batt_Boost_voltage;
	float Batt_Float_voltage;
	float Batt_undervoltage;
	float Batt_overvoltage;
	float batt_Overreconnect_vol;
	float batt_reconnect_vol;
	float grid_discon_battvol;
	float grid_reconnect_battvol;
	float grid_discon_battCur;
	float grid_reconnect_battCur;

	////
	float PV_undervoltage_Limit;
	float pv_overload_watts_Limit;// use in solar over load
	float PV_overvoltage_Limit;

	float Batt_min_current_Limit;
	float Batt_BF_Cur_Limit;
	float Batt_FB_Cur_Limit;
	float Batt_max_current_Limit;
	float Batt_Boost_voltage_Limit;
	float Batt_Float_voltage_Limit;
	float Batt_undervoltage_Limit;
	float Batt_overvoltage_Limit;
	float batt_reconnect_vol_Limit;
	float batt_Overreconnect_vol_Limit;
	float grid_discon_battvol_Limit;
	float grid_reconnect_battvol_Limit;
	float grid_discon_battCur_Limit;
	float grid_reconnect_battCur_Limit;

	///

	float Emergency_loadCutWatts;
	float Emergency_cut_vol;

	float minimum_current_led_on;

	float dusk2dawm_voltage_on;
	float dusk2dawm_voltage_off;

	uint8_t cell_number;
	uint8_t cell_number_limit;
	uint8_t batt_type;
	uint16_t batt_capacity;
	uint16_t cell_capacity_limit;

	uint16_t Mains_Fails_Detect_time;
        uint16_t Mains_Fails_Detect_time_1;
        uint16_t test;
        uint8_t mode_sys;
        

};

struct __attribute__((__packed__)) System
{
  uint8_t Reset:1;
  uint8_t Ready_Run:1;
  
  
  
};
struct __attribute__((__packed__)) TIMERt
{
  uint16_t oerrload;
  uint16_t Mains_HealthCHeckTimer;
  uint16_t Manis_UV_Timer;
  uint16_t Manis_OV_Timer;
  uint16_t dc_overload_timer;
  uint16_t grid_chrg_counter;
  uint16_t Debounce_Counter;
  uint16_t key_scan;
  uint16_t zcd_counter;
  uint16_t handle_scan;
  uint16_t NTC_Fan_ON;
  uint16_t NTC_Fan_OFF;
  uint16_t NTC_ShutDown;
  uint16_t NTC_ShutDownRec;
    uint16_t NTC_ShutDownI;
  uint16_t NTC_ShutDownRecI;
  uint16_t GridDisconnect_delay;
  uint16_t GridDisconnect_delay_2;
   uint16_t GridConnect_delay;
  
  
};

struct __attribute__((__packed__)) LED
{
  uint8_t charging_on:1;
  uint8_t overload_on:1;
  uint8_t low_batt_on:1;
  uint16_t chrg_blink_timer;
  uint16_t grid_chrg_blink_timer; 
  uint8_t grid_charging_on:1;
  
  
};

struct __attribute__((__packed__)) Temperature
{
  float MPPT_NTC;
  float INV_NTC;
  float compensation;
  
};


struct __attribute__((__packed__)) Services
{
  uint8_t dusk2dawn_control:1;
  
};

struct __attribute__((__packed__)) Energy
{
  uint16_t timer;
  float consumed;
   
};



///////////////////////////////////////
//// Inverter management flags  //////
/////////////////////////////////////

/// inveter sections
struct __attribute__((__packed__)) Inverter_ParameterSet
{
  float output_under_voltage;
  float output_over_voltage;
  float op_current_max;
  float op_current_110;
  float op_current_125;
  float op_current_150;
  float op_current_200;
  float op_current_250;
  float op_current_300;
  
  float grid_on_voltage_level;
  float grid_off_voltage_level;
  float gridcharging_stop_Battvoltage;
  float gridcharging_start_Battvoltage;
  float gridcharging_start_voltage;
  float gridcharging_stop_voltage;
  float gridcharging_float_voltage;
  float gridcharging_boost_voltage;
  float gridcharging_BCL;// battery current limit
  float gridcharging_min_cur;
  float output_voltage_max;
  float output_voltage_min;
  uint16_t PWM_gridChrg;
  uint16_t PWM_grid_limt;
  uint16_t PWM_grid_min;
  uint16_t PWM_GridStep_inc;
  uint16_t PWM_GridStep_dec;
  ///
  float output_under_voltage_Limit;
	float output_over_voltage_Limit;
        float op_current_max_Limit;
	float grid_on_voltage_level_Limit;
	float grid_off_voltage_level_Limit;
	float gridcharging_stop_Battvoltage_Limit;
	float gridcharging_start_Battvoltage_Limit;
	float gridcharging_start_voltage_Limit;
	float gridcharging_stop_voltage_Limit;
	float gridcharging_float_voltage_Limit;
	float gridcharging_boost_voltage_Limit;
	float gridcharging_BCL_Limit;// battery current limit
	float gridcharging_min_cur_Limit;
	float output_voltage_max_Limit;
	float output_voltage_min_Limit;
	uint16_t PWM_gridChrg_Limit;
	uint16_t PWM_grid_limt_Limit;
	uint16_t PWM_grid_min_Limit;
	uint16_t PWM_GridStep_inc_Limit;
	uint16_t PWM_GridStep_dec_Limit;
};


struct __attribute__((__packed__)) Inverter_timers
{
  uint16_t manins_on;
  uint16_t manins_off;
  uint16_t output_on;
  uint16_t output_off;
  uint16_t ZCD_counter_Hi;
  uint16_t ZCD_counter_Lo;
  uint16_t op_under;
  uint16_t op_over;
  uint16_t Grid_chrg_Stop_boost2float;
  uint16_t Grid_chrg_Stop_float;
  uint16_t float2boost_counter;
  uint16_t boost2float_counter;
  uint16_t Ms_counter;
  uint16_t Inverter_overload_timer;
  uint16_t Inverter_overload_110_timer;
  uint16_t Inverter_overload_125_timer;
  uint16_t Inverter_overload_150_timer;
  uint16_t Inverter_overload_200_timer;
  uint16_t Inverter_overload_250_timer;
  uint16_t Inverter_overload_300_timer;
  uint16_t Inverter_overload_330_timer;
  
  
  
  
  
};
struct __attribute__((__packed__)) Inverter_OP
{
  float voltage;
  float current;
  float ct_signal;
  float frq;
  
  float watts;
  
  
};

struct __attribute__((__packed__)) Inverter_flags
{
  
  uint8_t output_on:1;
  uint8_t output_under_vol:1;
  uint8_t output_over_vol:1;
  uint8_t output_switched_off:1;
   uint8_t outputPWM_INV_Started:1;
  uint8_t health_check:1;
  uint8_t overload:1;
  uint16_t soft_start_Timer;
  uint8_t inv_soft_short_ckt_delay:1;
  uint8_t inv_short_ckt_check:1;
  
  
};

struct __attribute__((__packed__)) Mains
{
  float voltage;
  float curent;
  float frq;
  uint8_t Check_ZCD:1;
  uint8_t ZCD_Pre_state_hi:1;
  uint8_t ZCD_Pre_state_lo:1;
  uint8_t ZCD_final_state:1;
  uint8_t start_rd_vol:1;
  uint8_t cal_ok:1;
  uint16_t Fail_Timer;
  uint16_t Fail_Timer_1;
  float Frq_low_limit;
  float Frq_hi_limit;
  uint8_t charging_disabled:1;
  uint16_t ZCD_reset_Timer;
  uint16_t mains_Fail_Timer;
  uint16_t mains_absent_Timer;
  uint16_t mains_ok_Timer;
  
  
  
};

struct __attribute__((__packed__)) Mains_flags
{
  uint8_t available:1;
  uint8_t charging_start:1;
  uint8_t charging_stop:1;
  uint8_t charging_softstart:1;
  uint8_t health_check:1;
  uint8_t under_voltage:1;
  uint8_t over_voltage:1;
  uint8_t soft_start:1;
  uint8_t grd_switch_off:1;
  uint8_t chargigng_boost_start:1;
  uint8_t chargigng_float_start:1;
  uint8_t MAINS_OK:1;
  uint8_t Grid_connect_recheck:1;
  uint8_t charging_on:1;
  uint8_t  mains_bypass:1;
  uint8_t  grid_charging_indication:1;
  
  
};


struct __attribute__((__packed__)) Keypad
{
	uint8_t menu_mode:1;
	uint8_t parameter_setting:1;
	uint8_t user_setting:1;
        uint8_t factory_reset:1;
        uint8_t password_scan_factory_reset:1;
	uint8_t sub_menu_clb_mode:1;
	uint8_t scan_do:1;
	uint8_t handling:1;
	uint8_t password_scan_mode:1;
	uint8_t clb_password_scan_mode:1;
        uint8_t version_dis:1;
        uint8_t set_date_time:1;



};
