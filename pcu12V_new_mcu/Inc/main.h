/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2020 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__
#include "stdint.h"
/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#define BLE_BYPASS
#define USB_BYPASS
    //inverter sine
#define PI 					3.14159
#define MEANPERIOD        	500
#define MAX_AMPLITUDE       720//730
#define SINEBUFFERLENGTH    50//60//100
/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define BLE_CSN_Pin GPIO_PIN_13
#define BLE_CSN_GPIO_Port GPIOC
#define PV_CUR_Pin GPIO_PIN_0
#define PV_CUR_GPIO_Port GPIOC
#define PV_VOL_Pin GPIO_PIN_1
#define PV_VOL_GPIO_Port GPIOC
#define BATT_VOL_Pin GPIO_PIN_2
#define BATT_VOL_GPIO_Port GPIOC
#define MAINS_CUR_Pin GPIO_PIN_3
#define MAINS_CUR_GPIO_Port GPIOC
#define BLE_IRQ_Pin GPIO_PIN_0
#define BLE_IRQ_GPIO_Port GPIOA
#define BLE_IRQ_EXTI_IRQn EXTI0_IRQn
#define INV_OP_VOL_Pin GPIO_PIN_1
#define INV_OP_VOL_GPIO_Port GPIOA
#define BATT_CUR_Pin GPIO_PIN_4
#define BATT_CUR_GPIO_Port GPIOA
#define MPPT_PWM_L_Pin GPIO_PIN_5
#define MPPT_PWM_L_GPIO_Port GPIOA
#define INV_OT_Pin GPIO_PIN_6
#define INV_OT_GPIO_Port GPIOA
#define MPPT_OT_Pin GPIO_PIN_7
#define MPPT_OT_GPIO_Port GPIOA
#define BATT_DIS_CUR_Pin GPIO_PIN_4
#define BATT_DIS_CUR_GPIO_Port GPIOC
#define BLE_RESET_Pin GPIO_PIN_5
#define BLE_RESET_GPIO_Port GPIOC
#define MAINS_VOL_Pin GPIO_PIN_0
#define MAINS_VOL_GPIO_Port GPIOB
#define TEMP_COMP_Pin GPIO_PIN_1
#define TEMP_COMP_GPIO_Port GPIOB
#define INV_PWM1_L_Pin GPIO_PIN_10
#define INV_PWM1_L_GPIO_Port GPIOE
#define INV_PWM1_H_Pin GPIO_PIN_11
#define INV_PWM1_H_GPIO_Port GPIOE
#define INV_PWM2_L_Pin GPIO_PIN_12
#define INV_PWM2_L_GPIO_Port GPIOE
#define INV_PWM2_H_Pin GPIO_PIN_13
#define INV_PWM2_H_GPIO_Port GPIOE
#define SDN_DRIVER_INV_Pin GPIO_PIN_12
#define SDN_DRIVER_INV_GPIO_Port GPIOB
#define FAN_CONTROL_Pin GPIO_PIN_14
#define FAN_CONTROL_GPIO_Port GPIOB
//#define ENT_Pin GPIO_PIN_8
//#define ENT_GPIO_Port GPIOD
//#define DEC_Pin GPIO_PIN_9
//#define DEC_GPIO_Port GPIOD
//#define INC_Pin GPIO_PIN_10
//#define INC_GPIO_Port GPIOD
//#define SET_Pin GPIO_PIN_11
//#define SET_GPIO_Port GPIOD

#define SET_Pin GPIO_PIN_8
#define SET_GPIO_Port GPIOD
#define INC_Pin GPIO_PIN_9
#define INC_GPIO_Port GPIOD
#define DEC_Pin GPIO_PIN_10
#define DEC_GPIO_Port GPIOD
#define ENT_Pin GPIO_PIN_11
#define ENT_GPIO_Port GPIOD
#define D4_Pin GPIO_PIN_12
#define D4_GPIO_Port GPIOD
#define D5_Pin GPIO_PIN_13
#define D5_GPIO_Port GPIOD
#define D6_Pin GPIO_PIN_14
#define D6_GPIO_Port GPIOD
#define D7_Pin GPIO_PIN_15
#define D7_GPIO_Port GPIOD
#define MPPT_PWM_H_Pin GPIO_PIN_6
#define MPPT_PWM_H_GPIO_Port GPIOC
#define ZC_Pin GPIO_PIN_7
#define ZC_GPIO_Port GPIOC
#define MAINS_RELAY_Pin GPIO_PIN_8
#define MAINS_RELAY_GPIO_Port GPIOC
#define LCD_BACKLIGHT_Pin GPIO_PIN_10
#define LCD_BACKLIGHT_GPIO_Port GPIOA
#define LED_INV_STATUS_Pin GPIO_PIN_15
#define LED_INV_STATUS_GPIO_Port GPIOA
#define LED_MPPT_CHRG_Pin GPIO_PIN_10
#define LED_MPPT_CHRG_GPIO_Port GPIOC
#define LED_MAINS_GRID_Pin GPIO_PIN_11
#define LED_MAINS_GRID_GPIO_Port GPIOC
#define LED_FAULTS_Pin GPIO_PIN_12
#define LED_FAULTS_GPIO_Port GPIOC
#define RS_Pin GPIO_PIN_0
#define RS_GPIO_Port GPIOD
#define EN_Pin GPIO_PIN_1
#define EN_GPIO_Port GPIOD
#define BUZZER_CONTROL_Pin GPIO_PIN_2
#define BUZZER_CONTROL_GPIO_Port GPIOD
#define INV_ON_OFF_SW_Pin GPIO_PIN_7
#define INV_ON_OFF_SW_GPIO_Port GPIOD
#define BLE_CLK_Pin GPIO_PIN_3
#define BLE_CLK_GPIO_Port GPIOB
#define BLE_MISO_Pin GPIO_PIN_4
#define BLE_MISO_GPIO_Port GPIOB
#define BLE_MOSI_Pin GPIO_PIN_5
#define BLE_MOSI_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */
#define ZCD_Read                         HAL_GPIO_ReadPin(ZC_GPIO_Port,ZC_Pin)
#define SHORT_CKT                        HAL_GPIO_ReadPin(SDN_DRIVER_INV_GPIO_Port,SDN_DRIVER_INV_Pin)
#define BUZZER_ON                        HAL_GPIO_WritePin(BUZZER_CONTROL_GPIO_Port,BUZZER_CONTROL_Pin,GPIO_PIN_SET);
#define BUZZER_OFF                       HAL_GPIO_WritePin(BUZZER_CONTROL_GPIO_Port,BUZZER_CONTROL_Pin,GPIO_PIN_RESET);
#define LED_toggle                       HAL_GPIO_TogglePin( LED_MPPT_CHRG_GPIO_Port, LED_MPPT_CHRG_Pin);
//#define LED_TEST(x)                    HAL_GPIO_WritePin( LED_MPPT_CHRG_GPIO_Port, LED_MPPT_CHRG_Pin,x);
#define MAINS_ON                         HAL_GPIO_WritePin(MAINS_RELAY_GPIO_Port,MAINS_RELAY_Pin,GPIO_PIN_SET);
#define MAINS_OFF                        HAL_GPIO_WritePin(MAINS_RELAY_GPIO_Port,MAINS_RELAY_Pin,GPIO_PIN_RESET);
//#define MAINS_OK                       HAL_GPIO_ReadPin(MAINS_OK_GPIO_Port,MAINS_OK_Pin)
/* USER CODE END Private defines */
#define INVERTER_ON                      HAL_GPIO_WritePin(LED_INV_STATUS_GPIO_Port,LED_INV_STATUS_Pin,GPIO_PIN_SET);
#define INVERTER_OFF                     HAL_GPIO_WritePin(LED_INV_STATUS_GPIO_Port,LED_INV_STATUS_Pin,GPIO_PIN_RESET);
#define GRID_CHARGING_ON                 HAL_GPIO_WritePin(LED_MAINS_GRID_GPIO_Port,LED_MAINS_GRID_Pin,GPIO_PIN_SET);
#define GRID_CHARGING_OFF                HAL_GPIO_WritePin(LED_MAINS_GRID_GPIO_Port,LED_MAINS_GRID_Pin,GPIO_PIN_RESET);
#define MPPT_CHARGING_ON                 HAL_GPIO_WritePin(LED_MPPT_CHRG_GPIO_Port,LED_MPPT_CHRG_Pin,GPIO_PIN_SET);
#define MPPT_CHARGING_OFF                HAL_GPIO_WritePin(LED_MPPT_CHRG_GPIO_Port,LED_MPPT_CHRG_Pin,GPIO_PIN_RESET);
//#define MPPT_CHARGING_ON               HAL_GPIO_WritePin(LED_MAINS_GRID_GPIO_Port,LED_MAINS_GRID_Pin,GPIO_PIN_SET);
//#define MPPT_CHARGING_OFF              HAL_GPIO_WritePin(LED_MAINS_GRID_GPIO_Port,LED_MAINS_GRID_Pin,GPIO_PIN_RESET);
#define MPPT_CHARGING_BLINK              HAL_GPIO_TogglePin( LED_MPPT_CHRG_GPIO_Port, LED_MPPT_CHRG_Pin);
#define GRID_CHARGING_BLINK              HAL_GPIO_TogglePin( LED_MAINS_GRID_GPIO_Port,LED_MAINS_GRID_Pin);
#define LED_PV_TOGGLE                    HAL_GPIO_TogglePin(LED_MPPT_CHRG_GPIO_Port,LED_MPPT_CHRG_Pin)

#define FAN_ON	                         HAL_GPIO_WritePin(FAN_CONTROL_GPIO_Port,FAN_CONTROL_Pin,GPIO_PIN_SET);
#define FAN_OFF	                         HAL_GPIO_WritePin(FAN_CONTROL_GPIO_Port,FAN_CONTROL_Pin,GPIO_PIN_RESET);
//#define PV_ON                          HAL_GPIO_WritePin(PV_ON_LED_GPIO_Port,PV_ON_LED_Pin,GPIO_PIN_RESET);
//#define PV_OFF                         HAL_GPIO_WritePin(PV_ON_LED_GPIO_Port,PV_ON_LED_Pin,GPIO_PIN_SET);
#define FAULT_ON                         HAL_GPIO_WritePin(LED_FAULTS_GPIO_Port,LED_FAULTS_Pin,GPIO_PIN_SET);
#define FAULT_OFF                        HAL_GPIO_WritePin(LED_FAULTS_GPIO_Port,LED_FAULTS_Pin,GPIO_PIN_RESET);
//#define USB_ON                         HAL_GPIO_WritePin(BATT_UV_LED_GPIO_Port,BATT_UV_LED_Pin,GPIO_PIN_RESET);
//#define USB_OFF                        HAL_GPIO_WritePin(BATT_UV_LED_GPIO_Port,BATT_UV_LED_Pin,GPIO_PIN_SET);
#define USER_INV_SWITCH                  HAL_GPIO_ReadPin(INV_ON_OFF_SW_GPIO_Port,INV_ON_OFF_SW_Pin)
//#define LOAD_ON                        HAL_GPIO_WritePin( NORMAL_LOAD_CNTRL_GPIO_Port,NORMAL_LOAD_CNTRL_Pin,GPIO_PIN_SET);
//#define LOAD_OFF                       HAL_GPIO_WritePin( NORMAL_LOAD_CNTRL_GPIO_Port,NORMAL_LOAD_CNTRL_Pin,GPIO_PIN_RESET);
 
void Adc_Avg(void); 
void Set__Date(uint8_t date,uint8_t month , uint16_t year );
void Set__Time(uint8_t hr,uint8_t mm , uint16_t sec );
void RTC_Get_Time_Date(void);
extern unsigned int display_time; 
extern uint16_t ADC_DMA_Buff[11];
extern unsigned int ADC_DMA_Buff_mains;
extern unsigned int ADC_DMA_Buff_mains1;
extern unsigned int ADC_DMA_Buff_mains2;
extern unsigned int ADC_DMA_Buff_mains3;
extern unsigned int ADC_DMA_Buff_mains4;
extern unsigned int ADC_DMA_Buff_mains5;
extern unsigned int ADC_DMA_Buff_mains6;
extern unsigned int ADC_DMA_Buff_mains7;
extern unsigned int timer1;
extern unsigned int timer2;
extern unsigned int timer3;
extern unsigned int timer4;
extern unsigned int timer5;
extern unsigned int timer6;
extern unsigned int timer7;
extern _Bool display2lcd;

#define FACTOR_POS_BATT_VOL  2
#define FACTOR_POS_BATT_ICUR  5
#define FACTOR_POS_BATT_OCUR  6
#define FACTOR_POS_INVOP_VOL  4
#define FACTOR_POS_INVOP_CUR  3
#define FACTOR_POS_SOLOAR_VOL  1
#define FACTOR_POS_SOLOAR_CUR  0
#define FACTOR_POS_MAIN_VOL  7
#define FACTOR_POS_MAIN_CUR  9
#define FACTOR_POS_TEMP_COMP  8




#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
